clear all
close all

load('action_classes.mat')
networks = {'Ours', 'LSTM3LR', 'ERD'};
% actions = {'walking', 'eating', 'smoking', 'discussion'};
actions = {'walking', 'eating'};

all_actions = {'directions', 'discussion', 'eating', 'greeting', 'phoning', ...
               'posing', 'purchases', 'sitting', 'sittingdown', 'smoking', ...
               'takingphoto', 'waiting', 'walking', 'walkingdog', ...
               'walkingtogether', 'Unnatural'};
action_index = 1:16;
action_vs_indx = containers.Map(all_actions, action_index);


num_actions = length(actions);
num_networks = length(networks);

figure;

line_styles = {'-','--', ':'};
set(groot,'defaultAxesLineStyleOrder', line_styles)
co = [0 0 1; % blue
      1 0 0; % red
      0 1 0; % green
      0.5 0.5 0.5; % grey
      1 0 1; % magenta
      0 1 1]; % cyan
set(groot,'defaultAxesColorOrder',co)
font_size = 17;

num_colors = size(co, 1);
num_line_styles = length(line_styles);

for action_idx = 1:num_actions
    subplot(num_actions, 1, action_idx)
    count_plot = 0;
    for network_idx = 1:num_networks          
        current_key = [networks{network_idx}, '_',...
                       actions{action_idx}];
        action_prob = action_classes(current_key);
        action_prob = action_prob(:, action_vs_indx(actions{action_idx}));
        plot(action_prob, 'LineWidth', 2.5, ...
            'MarkerSize', 6, 'Color', co(mod(count_plot, num_colors) + 1,:), ...
            'LineStyle', line_styles{mod(floor(count_plot / num_colors), ...
                                     num_line_styles) + 1})
        if count_plot == 0
            hold on
            ylabel('Class Probability \rightarrow', 'FontSize', font_size)
            xlabel('Frame Index \rightarrow', 'FontSize', font_size)
            set(gca,'FontSize',font_size)
        end
        count_plot = count_plot + 1;
    end
    l = legend(networks);
    set(l, 'FontSize', 18)
end

ax1 = axes('Position',[0 0 1 1],'Visible','off');
axes(ax1) % sets ax1 to current axes
text(.04, 0.7, 'Walking', 'FontSize', 25, 'Rotation', 90)
text(.04, 0.2, 'Eating', 'FontSize', 25, 'Rotation',90)