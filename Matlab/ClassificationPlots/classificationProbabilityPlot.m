% plots Classification probability plot for an activity
addpath('../')
close all
clear all

% -------------- Settings --------------%
path_to_seqs = '../../python_models/Classifications/';
seq_numbers = {'9', '25', '37', '53'};
is_expmap_data = true;
% network_name = 'LSTM3LR';
network_name = 'Ours';
% network_name = 'ERD';

person = 1;
R0 = eye(3);
T0 = [0 0 0];
% -------------- Settings --------------%

action_classes_name = {'directions', 'discussion', 'eating', 'greeting', ...
       'phoning', 'posing', 'purchases', 'sitting', 'sittingdown', ...
       'smoking', 'takingphoto', 'waiting', 'walking', 'walkingdog', ...
       'walkingtogether', 'Unnatural'};
actin_class_index = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16};
action_classes_map = containers.Map(action_classes_name, actin_class_index);

for seq_number = 1:length(seq_numbers)
    file_name_with_path = [path_to_seqs, 'categorical_', seq_numbers{seq_number}, '.txt'];
    [original_file_name, ~, ~, ~, ~] = parseHeader(file_name_with_path);

    [original_file_name, seq_start_row, ...
     seq_length, dwon_sampling_rate, FPS] = parseHeader(original_file_name);

    original_file_componnents = strsplit(original_file_name, '/');
    original_action_class = strsplit(original_file_componnents{end}, '.');
    original_action_class = original_action_class{1}(1:end-2);

    class_probabilities = dlmread(file_name_with_path, ',', 9);
    
    figure
    set(groot,'defaultAxesLineStyleOrder', {'-','.','--', 's', 'x'})
    co = round([12  156 232;
                241 0   255;
                0   0   0;
                117 255 6]/255);
    set(groot,'defaultAxesColorOrder',co)
    
    plot(class_probabilities, 'LineWidth', 2, 'MarkerSize', 6)
    hold on 
    legend(action_classes_name)
    % plot(class_probabilities(:, action_classes_map(original_action_class)),...
    %     'LineWidth', 3)
    title(original_action_class)
    drawnow

    if exist('action_classes.mat', 'file') == 2
        load('action_classes.mat')
    else
        action_classes = containers.Map();
    end

    current_key = [network_name, '_', original_action_class];

%     if isKey(action_classes, current_key)
%         x = input(...
%             'Results for this network is already in record over write(y/n)?');
%         if x~= 'y' && x~= 'Y'
%             clear all
%             return
%         end
%     end

    action_classes(current_key) = class_probabilities;

    save('action_classes.mat', 'action_classes')
end

