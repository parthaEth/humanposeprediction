clear all
close all

load('action_classes.mat')
networks = {'LSTM3LR', 'ERD', 'Ours'};
actions = {'walking', 'eating', 'smoking', 'discussion'};


num_actions = length(actions);
num_networks = length(networks);

figure;

set(groot,'defaultAxesLineStyleOrder', {'-','--', ':'})
co = [0 0 1; % blue
      1 0 0; % red
      0 1 0; % green
      0.5 0.5 0.5; % grey
      1 0 1; % magenta
      0 1 1]; % cyan
set(groot,'defaultAxesColorOrder',co)
font_size = 13;

for action_idx = 1:num_actions
    for network_idx = 1:num_networks
        subplot(num_actions, num_networks, ...
                (action_idx-1) * num_networks + network_idx)
            
        current_key = [networks{network_idx}, '_',...
                       actions{action_idx}];
        plot(action_classes(current_key), 'LineWidth', 2.5, ...
            'MarkerSize', 6)
        ylabel('Class Probability \rightarrow', 'FontSize', font_size)
        xlabel('Frame Index \rightarrow', 'FontSize', font_size)
        set(gca,'FontSize',font_size)
    end
end

l = legend('directions', 'discussion', 'eating', 'greeting', 'phoning', ...
       'posing', 'purchases', 'sitting', 'sittingdown', 'smoking', ...
       'takingphoto', 'waiting', 'walking', 'walkingdog', ...
       'walkingtogether', 'Unnatural');
set(l, 'FontSize', 18)

ax1 = axes('Position',[0 0 1 1],'Visible','off');
axes(ax1) % sets ax1 to current axes
text(.24, 0.97, 'LSTM3LR', 'FontSize', 25)
text(.50, 0.97, 'ERD', 'FontSize', 25)
text(.71, 0.97, 'Ours', 'FontSize', 25)

y_space = 0.22;
offset = +0.01;
for i =1:4
    h = text(.05, 1 - (offset + y_space*i), actions{i}, ...
        'FontSize', 25, 'Rotation',90);
%     set(h,'Rotate',90)
end