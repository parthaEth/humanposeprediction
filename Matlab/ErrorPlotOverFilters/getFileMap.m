clc
clear all
close all

addpath('../ploting')
addpath('../')
addpath('../../providedCode/RNNexp/structural_rnn/CRFProblems/H3.6m/dataParser/Utils/')
addpath('../../providedCode/RNNexp/structural_rnn/CRFProblems/H3.6m/mhmublv/Motion')

base_path = '../../python_models/GeneratedSequences/';
for file_idx=1:60
    full_path = [base_path, num2str(file_idx), '.csv'];
    [original_file, ~, ~, ~, ~] = parseHeader(full_path);
    
    disp([num2str(file_idx), original_file])
end