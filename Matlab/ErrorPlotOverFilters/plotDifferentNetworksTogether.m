clc
clear all
close all

load('error_across_models.mat')

action = 'all';

netwrok_name = {['AutoencoderLSTM_', action, '_0.0_0.0_DCL6'], ...
                ['AutoencoderLSTM_', action, '_0.0_0.0_CL'], ...
                ['AutoencoderLSTM_', action, '_0.0_0.0_NDNC'],...
                ['ERD_', action, '_0.0_0.0_CL'],...
                ['LSTM3LR_', action, '_0.0_0.0_CL'],...
                ['ours_', action, '_0.0_0.0_CL']};

for net_idx = 1:length(netwrok_name)

    current_key = netwrok_name{net_idx};

    current_errors = error_across_models(current_key);
    if length(current_errors) == 400
        rmse(:, net_idx) = current_errors(51:end);
    else
        rmse(:, net_idx) = current_errors;
    end
end

plot(rmse, 'LineWidth', 3)
legend('DropOut AutoEncode',...
       'Curriculum Learning', 'NoCurrNoDropOut', 'ERD', 'LSTM3LR', 'ours')

xlabel('Frame Index \rightarrow')
ylabel('L2 error \rightarrow')
