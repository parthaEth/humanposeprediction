clear all
addpath('../ploting')
addpath('../../providedCode/RNNexp/structural_rnn/CRFProblems/H3.6m/dataParser/Utils/')
addpath('../../providedCode/RNNexp/structural_rnn/CRFProblems/H3.6m/mhmublv/Motion/')

global H36m_root;
H36m_root = '../';

file_name_with_path = 'corrupted_recovered_pose.txt';
generated_data = dlmread(file_name_with_path, ',', 0);
% generated_data = dlmread('1.csv', ',', 9);

load('../skeletons.mat')
skeleton = skeletons(1).angSkel;
c = zeros(1,78);
[ skel_expmap,~ ] = getExpmapFromSkeleton( skeleton, c );
R0 = eye(3);
T0 = [0 0 0];
data1 = getPosFromExpmap(skel_expmap, generated_data(1, :));
data2 = getPosFromExpmap(skel_expmap, generated_data(2, :));
data3 = getPosFromExpmap(skel_expmap, generated_data(3, :));

data = [data1; data2; data3];

[rows, cols] = size(data);
data = reshape(data', 1, rows*cols);

cols_actual = length(skeleton.tree) * 3;
rows_original = rows*cols / cols_actual;

data = reshape(data, cols_actual, rows_original)';

skeleton.type = 'MyOwn';

for i = 1:3
    subplot(1, 3, i)
    initSkeletonPlot(data(i, :), skeleton, gca);
end