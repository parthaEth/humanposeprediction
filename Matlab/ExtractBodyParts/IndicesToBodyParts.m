function IndicesToBodyParts()
addpath('../../providedCode/RNNexp/structural_rnn/CRFProblems/H3.6m/dataParser/Utils/')
addpath('../../providedCode/RNNexp/structural_rnn/CRFProblems/H3.6m/mhmublv/Motion/')
load('../skeletons.mat')
skeleton = skeletons(1).angSkel;
c = zeros(1,78);
[ skel_expmap,~ ] = getExpmapFromSkeleton( skeleton, c );

skeleton_tree = skel_expmap.tree;

% Parse the list of deleted columns
file_name_with_path = '../../python_models/column_wise_std.text';

fid = fopen(file_name_with_path);
header = textscan(fid, '%s %c %f %c', 1, 'delimiter',' ');
threshold_std = header{1,3}(1);
fclose(fid);
std_vec = dlmread(file_name_with_path, ',', 2);
deleted_cols = (std_vec < threshold_std);

spine = {'Hips', 'Spine', 'Spine1', 'Neck', 'Head'};

right_leg = {'RightUpLeg', 'RightLeg', 'RightFoot', 'RightToeBase'};
left_leg = {'LeftUpLeg', 'LeftLeg', 'LeftFoot', 'LeftToeBase'};

left_arm = {'LeftShoulder', 'LeftArm', 'LeftForeArm', 'LeftHand',...
            'LeftHandThumb', 'L_Wrist_End'};
right_arm = {'RightShoulder', 'RightArm', 'RightForeArm', 'RightHand',...
            'RightHandThumb', 'R_Wrist_End'};
        
disp('Spine indices are ')
indices = ['[0 1 2]'];
for i = 1:length(spine)
    indices = [indices, '[', num2str(findPartByName(spine{i}, skeleton_tree,...
                                                                       deleted_cols)), ']'];
end
indices

        
disp('right_leg indices are ')
indices = [];
for i = 1:length(right_leg)
   indices = [indices, '[', num2str(findPartByName(right_leg{i}, skeleton_tree,...
                                                   deleted_cols)), ']'];
end
indices

disp('left_leg indices are ')
indices = [];
for i = 1:length(left_leg)
   indices = [indices, '[', num2str(findPartByName(left_leg{i}, skeleton_tree, ...
                                                   deleted_cols)), ']'];
end
indices

disp('left_arm indices are ')
indices = [];
for i = 1:length(left_arm)
   indices = [indices, '[', num2str(findPartByName(left_arm{i}, skeleton_tree, ...
                                                   deleted_cols)), ']'];
end
indices

disp('right_arm indices are ')
indices = [];
for i = 1:length(right_arm)
   indices = [indices, '[', num2str(findPartByName(right_arm{i}, skeleton_tree, ...
                                      deleted_cols)), ']'];
end
indices

end

function indices = findPartByName(part_name, skeleton_tree, deleted_cols)
indices = [];
for i = 1:length(skeleton_tree)
    if strcmp(part_name, skeleton_tree(i).name)
        for j= 1:length(skeleton_tree(i).expmapInd)
            if deleted_cols(skeleton_tree(i).expmapInd(j))  ~=  1
                indx_reduction = ...
                    sum(deleted_cols(1:(skeleton_tree(i).expmapInd(j)-1)));
                indices = ...
                    [indices, skeleton_tree(i).expmapInd(j) - indx_reduction];
            end
        end
        
%         for j=1:length(skeleton_tree(i).posInd)
%             indx_reduction = ...
%                 sum(deleted_cols(1:(skeleton_tree(i).posInd(j)-1)));
%             indices = ...
%                 [indices, skeleton_tree(i).posInd(j) - indx_reduction];
%         end
    end
end
indices = indices - 1; % making python index
end
 