% Understanding the Holden Dataset
clear all
% file_name_with_path = '../../python_models/GeneratedSequences/1.csv';
file_name_with_path = '1.csv';
generated_data = dlmread(file_name_with_path, ',', 11);

connection_order = ...
    [-1,0,1,2,3,4,1,6,7,8,1,10,11,12,12,14,15,16,12,18,19,20];

figure
for i=1:length(connection_order)
%     plot3(generated_data(1, (i-1)*3 + 1), generated_data(1, (i-1)*3 + 3),...
%         generated_data(1, (i-1)*3 + 2), '*')
text(generated_data(1, (i-1)*3 + 1), generated_data(1, (i-1)*3 + 3),...
        generated_data(1, (i-1)*3 + 2), num2str(i))
    hold on
end
axis equal

for i=3:length(connection_order)
    line([generated_data(1, (i-1) * 3 + 1), ...
        generated_data(1, connection_order(i) * 3 + 1)], ...
        [generated_data(1, (i-1) * 3 + 3), ...
        generated_data(1, connection_order(i) * 3 + 3)],...
        [generated_data(1, (i-1) * 3 + 2), ...
        generated_data(1, connection_order(i) * 3 + 2)])
end
