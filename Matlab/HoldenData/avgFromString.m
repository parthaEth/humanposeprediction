% average numbers from string
% 3LRLstm
nums = ['2.32', '2.73', '3.46', '3.85';
        '2.25', '2.49', '4.40', '4.86';
        '2.30', '2.71', '2.34', '2.78'];
% AutoEncoder
% nums = ['3.07', '3.19', '5.31', '6.03';
%         '2.43', '3.57', '4.29', '6.81';
%         '2.62', '3.20', '3.93', '4.84'];
average = zeros(1, 4);
for i= 1:size(nums, 1)
    for j = 1:4:size(nums, 2)-1
        average(round(j/4)+1) = average(round(j/4)+1) + str2double(nums(i, j:j+3));
    end
end
format bank
avg = average/size(nums, 1)