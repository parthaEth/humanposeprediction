function handles = initSkeletonPlot(frame, connection_order, axis_id)
%INITSKELETONPLOT Takes joint positions and connection map to generate
%skeletonplot
%   initialize skeleton plot for holden dataset primarily.

if nargin < 2
    figure;
else
    axes(axis_id)
end

handles = zeros(length(connection_order) - 1, 1);

for i=3:length(connection_order)
    handles(i-1) = line([frame((i-1) * 3 + 1), ...
        frame(connection_order(i) * 3 + 1)], ...
        [frame((i-1) * 3 + 3), ...
        frame(connection_order(i) * 3 + 3)],...
        [frame((i-1) * 3 + 2), ...
        frame(connection_order(i) * 3 + 2)]);
    drawnow
    if i==1
        hold on
    end
end
axis equal
end

