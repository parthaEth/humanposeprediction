clear all
file_name_with_path = 'corrupted_recovered_pose.txt';
generated_data = dlmread(file_name_with_path, ',', 0);

% file_name_with_path = '1.csv';
% generated_data = dlmread(file_name_with_path, ',', 11);

connection_order = ...
    [-1, 0,1,2,3,4,1,6,7,8,1,10,11,12,12,14,15,16,12,18,19,20];

for i = 1:3
    subplot(1, 3, i)
    initSkeletonPlot([0, 0, 0, generated_data(i, :)], ...
        connection_order, gca);
    axis tight
end