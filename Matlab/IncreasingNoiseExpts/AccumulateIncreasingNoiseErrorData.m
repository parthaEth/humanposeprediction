% Accumulate results for increasing noise and increasing dropout results
clear all
close all
addpath('../')
% -------------- Settings --------------%
action_category = '*';
my_own_data = false;
is_expmap_data = true;
person = 1; % Whose skeleton to use. Data has been normalized with s1
R0 = eye(3);
T0 = [0 0 0];
netwrok_name = 'AutoencoderOnly';
drop_out_rate = '0.1'; % '0.0, 0.01, 0.05, 0.1';
learning_strategies = '_NDNC_WG';
input_noise_sigma = '0.0'; 
% -------------- Settings --------------%

actions = {'walking', 'eating', 'smoking', 'discussion', 'all'};
indices = {[47, 48, 17, 18], [11, 12, 43, 44], [13, 14, 59, 60], ...
           [5, 5, 41, 42], 1:60};
% indices = {[1,6], 8, [24, 4], [10, 12], 1:30};
actions_to_seq = containers.Map(actions, indices);

mean_over_timesteps = 0;
count = 0;
for action_idx = 1:length(actions)
    if strcmp(action_category, '*')
        current_action_category = actions{action_idx};
    end
    
    for pose_seq = actions_to_seq(current_action_category)
        file_name_with_path = ...
            ['../../python_models/GeneratedSequences/', num2str(pose_seq),...
            '.csv'];

        [original_file_name, seq_star_row, seq_length, dwon_sampling_rate, ...
            FPS] = parseHeader(file_name_with_path);

        disp(['Reporting erros for',original_file_name])

        % python index to matlab conversion
        seq_star_row = seq_star_row + 1;

        generated_data = dlmread(file_name_with_path, ',', 9);
        [rows_gen_data, cols_gen_data] = size(generated_data);
        generated_data = repelem(generated_data, dwon_sampling_rate, 1);
        [generated_data_r, ~, ~]= ...
            revertCoordinateSpace(generated_data, R0, T0);
        generated_data = [generated_data(1:dwon_sampling_rate:end, 1:6),...
            generated_data_r(1:dwon_sampling_rate:end, 7:end)];
        % generated_data(:, 6:end) = generated_data_reconstructed(:, 6:end);

        original_data = load(original_file_name);
        [rows_org_data, cols_org_data] = size(original_data);
        [original_data_r, ~, ~]= revertCoordinateSpace(original_data, R0, T0);
        original_data(:, 7:end) = original_data_r(:, 7:end);

        comparable_length = min(rows_gen_data,...
            floor((rows_org_data - seq_star_row + 1)/dwon_sampling_rate));
        time_vec = 0 : 1/FPS : comparable_length/FPS;

        mse = ...
            sum((original_data(...
            seq_star_row : dwon_sampling_rate : dwon_sampling_rate ...
            * (comparable_length -1) + seq_star_row, 7:end)...
            - generated_data(1:comparable_length, 7:end)).^2, 2);
        rmse = sqrt(mse);

        mean_over_timesteps = mean_over_timesteps + mean(rmse);
        count = count + 1;

    end

    mean_over_seqs = mean_over_timesteps/count

    if exist('increasing_noise.mat', 'file') == 2
        load('increasing_noise.mat')
    else
        increasing_noise = containers.Map();
    end

    current_key = [netwrok_name, '_', current_action_category, '_', ...
                   drop_out_rate, '_', input_noise_sigma, ...
                   learning_strategies];

    if isKey(increasing_noise, current_key)
        x = input(...
            'Results for this network is already in record over write(y/n)?');
        if x~= 'y' && x~= 'Y'
            clear all
            return
        end
    end

    increasing_noise(current_key) = mean_over_seqs;
    save('increasing_noise.mat', 'increasing_noise')
    
    if ~strcmp(action_category, '*')
        break
    end
end

