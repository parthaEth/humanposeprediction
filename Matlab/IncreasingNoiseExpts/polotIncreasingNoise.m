load('increasing_noise.mat')

drop_outs = {'0.0', '0.01', '0.05', '0.1'};
netwrok_name = 'AutoencoderOnly';
input_noise_sigma = '0.0';
learning_strategies = {'NDNC_WG', 'CL_WG', 'DCL6_WG'};
current_action_category = 'all';

for drop_out_idx = 1:length(drop_outs)
    for learnin_str_idx = 1:length(learning_strategies)
        current_key = [netwrok_name, '_', current_action_category, '_', ...
                       drop_outs{drop_out_idx}, '_', input_noise_sigma, ...
                       '_', learning_strategies{learnin_str_idx}];
        all_measurements(drop_out_idx, learnin_str_idx) = ...
            increasing_noise(current_key);
    end
end

for drop_out_idx = 1:length(drop_outs)
    drop_nums(drop_out_idx, 1) = str2double(drop_outs{drop_out_idx});
end

plot(repmat(drop_nums, 1, size(all_measurements, 2)), all_measurements,...
    'LineWidth', 3)
legend(learning_strategies, 'Interpreter', 'none')


% load('increasing_noise.mat')
% actions = {'walking', 'eating', 'smoking', 'discussion', 'all'};
% noise_levels = {'0.0', '0.01', '0.05', '0.1'};
% 
% for i=1:length(actions)
%     for j=1:length(noise_levels)
% 
%         current_key = ['AutoencoderOnly_', actions{i}, '_0.0_',...
%                        noise_levels{j}, '_NDNC_WG'];
% 
%         temp = increasing_noise(current_key);
%         remove(increasing_noise, current_key);
%         current_key = ['AutoencoderOnly_', actions{i}, '_',...
%                        noise_levels{j}, '_0.0_NDNC_WG'];
%         increasing_noise(current_key) = temp;
%     end
% end
% 
% save('increasing_noise.mat', 'increasing_noise')
