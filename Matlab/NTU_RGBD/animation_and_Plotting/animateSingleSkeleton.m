function animateSingleSkeleton(file_number, base_dir_path, ...
    frame_rate, video_file_name)
%ANIMATESINGLESKELETON Animates the content ofa *.skeleton file and
%optionally records it to a .avi file

files = dir(base_dir_path);
filename = [files(file_number+2).folder, '/', files(file_number+2).name];
bodyinfo = read_skeleton_file(filename);

handles = initSkeletonPlot(bodyinfo(1).bodies(1).joints);

if(nargin ==  4)
    set(gcf, 'Position', get(0,'Screensize')-[0, 0, 500, 500]);
    v = VideoWriter(video_file_name);
    v.FrameRate = frame_rate;
    open(v);
end

for frame_idx = 2:length(bodyinfo)
    tic
    updateSkeletonPlot(bodyinfo(frame_idx).bodies(1).joints, handles, gca);
    elapsed_time = toc;
    if(elapsed_time < 1/frame_rate)
        pause((1/frame_rate) - elapsed_time);
    else
        warning(['FPS requirement too high cannot perform everything'...
            ,' in time.']);
    end
    
    if(nargin ==  4)
        F = getframe(gcf);
        writeVideo(v, F);
    end
    
end

if(nargin ==  4)
    close(v);
end

end

