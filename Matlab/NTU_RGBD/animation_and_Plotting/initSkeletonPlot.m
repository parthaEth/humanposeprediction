function handles = initSkeletonPlot(joint_positions, axis_handle)
%INITSKELETONPLOTINITSKELETONPLOT initializes the plot in a given window
%and returns the body link handles.
link_mat = getSkeletonConnectionmat();

handles = zeros(size(link_mat, 1));

if(nargin < 2)
    figure;
else
    axes(axis_handle)
end

for i = 1:size(link_mat, 1)
    handles(i) = line([joint_positions(link_mat(i, 1)).x, ...
                      joint_positions(link_mat(i, 2)).x], ...
                      [joint_positions(link_mat(i, 1)).y, ...
                      joint_positions(link_mat(i, 2)).y], ...
                      [joint_positions(link_mat(i, 1)).z, ...
                      joint_positions(link_mat(i, 2)).z], ...
                      'LineWidth', 3);
                  
end
xlim([-1 + joint_positions(1).z, 1 + joint_positions(1).z]);
ylim([-1 + joint_positions(1).x, 1 + joint_positions(1).x])
zlim([-1 + joint_positions(1).y, 1 + joint_positions(1).y])
axis equal
grid on
end

