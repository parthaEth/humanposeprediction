function updateSkeletonPlot(joint_positions, link_handles, axis_handle)
%UPDATESKELETONPLOT given line handles updates the skeleton plot

link_mat = getSkeletonConnectionmat();

for i = 1:length(link_handles)
    set(link_handles(i), 'XData', ...
        [joint_positions(link_mat(i, 1)).x, ...
         joint_positions(link_mat(i, 2)).x], ...
        'YData', [joint_positions(link_mat(i, 1)).y, ...
                  joint_positions(link_mat(i, 2)).y],...
        'ZData', [joint_positions(link_mat(i, 1)).z, ...
                  joint_positions(link_mat(i, 2)).z])
end

axis_handle.ZLim = [-1 + joint_positions(1).z, 1 + joint_positions(1).z];
axis_handle.XLim = [-1 + joint_positions(1).x, 1 + joint_positions(1).x];
axis_handle.YLim = [-1 + joint_positions(1).y, 1 + joint_positions(1).y];

drawnow
end

