clc
clear all
close all
% This function animates groundtruth and predicted sequence side by side or
% on top of each other for visual comparison

addpath('ploting')
addpath('../providedCode/RNNexp/structural_rnn/CRFProblems/H3.6m/dataParser/Utils/')
addpath('../providedCode/RNNexp/structural_rnn/CRFProblems/H3.6m/mhmublv/Motion/')

% -------------- Settings --------------%
fps = 25; % This is just display fps
pose_seq = '17';
express_error_in_normalized_scale = true;
write_to_video_file = true; %set to true if an avi is desired
my_own_data = false;
is_expmap_data = true;
skip_first_n_frames = 1;
person = 1; % Whose skeleton to use. Data has been normalized with s1
R0 = eye(3);
T0 = [0 0 0];
% -------------- Settings --------------%

file_name_with_path = ...
    ['../python_models/GeneratedSequences/', pose_seq, '.csv'];

[original_file_name, seq_start_row, ...
 seq_length, dwon_sampling_rate, FPS] = parseHeader(file_name_with_path);
path_components = strsplit(original_file_name, '/');
seq_name = strsplit(path_components{end}, '.'); seq_name = seq_name{1};

% python index to matlab conversion 
seq_start_row = seq_start_row * dwon_sampling_rate + 1;

generated_data = dlmread(file_name_with_path, ',', 9);
generated_data(:, [4, 6]) = 0;
% generated_data(:, 5) = 0.02;
[rows_gen_data, cols_gen_data] = size(generated_data);

original_data = load(original_file_name);
[rows_org_data, cols_org_data] = size(original_data);
[original_data, ~, ~]= revertCoordinateSpace(original_data, R0, T0);

comparable_length = min(rows_gen_data,...
    floor((rows_org_data - seq_start_row + 1)/dwon_sampling_rate));
time_vec = 0 : 1/FPS : comparable_length/FPS;

drawable_original = original_data(...
     seq_start_row : dwon_sampling_rate : dwon_sampling_rate ...
        * (comparable_length -1) + seq_start_row,:);

drawable_generated = generated_data(1:comparable_length, :);
    
load('skeletons.mat')

if is_expmap_data
    skeleton = skeletons(person).angSkel;
    c = zeros(1,78);
    [ skel_expmap,~ ] = getExpmapFromSkeleton( skeleton, c );
    
    drawable_generated = repelem(drawable_generated, dwon_sampling_rate, 1);
    [drawable_generated, ~, ~]= revertCoordinateSpace(drawable_generated, R0, T0);
    drawable_generated = drawable_generated(1:dwon_sampling_rate:end, :);
    
    drawable_original = getPosFromExpmap(skel_expmap, drawable_original);
    [rows, cols] = size(drawable_original);
    drawable_original = reshape(drawable_original', 1, rows*cols);
    
    drawable_generated = getPosFromExpmap(skel_expmap, drawable_generated);
    drawable_generated = reshape(drawable_generated', 1, rows*cols);
    
    skeleton = skeletons(person).posSkel;
    cols_actual = length(skeleton.tree) * 3;
    rows_original = rows*cols / cols_actual;
    
    drawable_original = reshape(drawable_original, cols_actual, rows_original)';
    drawable_generated = reshape(drawable_generated, cols_actual, rows_original)';
    
else
    skeleton = skeletons(person).posSkel;
end

if my_own_data || is_expmap_data
    skeleton.type = 'MyOwn';
end


% expPlayData(skel_expmap, drawable_original, 1/FPS)


f = figure;
set(f, 'Position', get(0,'Screensize')-[0, -499, 500, 500]);
t = suptitle(['Current time = 0 sec.; FPS = ',  num2str(FPS)]);

subplot(1, 2, 1)
title([seq_name, ' Original'], 'Interpreter', 'none')
axis_handle_original = gca;
handles_orginal = ...
    initSkeletonPlot(drawable_original(skip_first_n_frames, :), ...
                     skeleton, axis_handle_original);
colorSkel(handles_orginal, false); % Showing original in grey always

subplot(1, 2, 2)
title([seq_name, ' Predicted'], 'Interpreter', 'none')
axis_handle_generated = gca;
% plot3(drawable_generated(:, 1), drawable_generated(:, 2),...
%       drawable_generated(:, 3))
handles_generated = ...
    initSkeletonPlot(drawable_generated(skip_first_n_frames, :),...
                     skeleton, axis_handle_generated);
colorSkel(handles_generated, false);

if(write_to_video_file)
    set(gcf, 'Position', get(0,'Screensize')-[0, -499, 500, 500]);
    v = VideoWriter('OriginalAndPredictedSidebyside.avi');
    v.FrameRate = 16;
    open(v);
end

for i = (skip_first_n_frames + 1):size(drawable_original, 1)
    tic
    %     angles(i, 4:6) = [0, 1.7208, 1.4708];
    if uint64(i) >= uint64(seq_length + 1)
        colorSkel(handles_generated, true);
    end
    
    updateSkeletonPlot(handles_orginal, drawable_original(i, :),...
                       skeleton, true, axis_handle_original);
    updateSkeletonPlot(handles_generated, drawable_generated(i, :),...
                       skeleton, true, axis_handle_generated);
    
    t.String = ['Current time = ', num2str((i-2)/FPS), ...
                'sec. FPS = ', num2str(FPS)];
    if(write_to_video_file)
        F = getframe(gcf);
        writeVideo(v, F);
    end
    
    elapsed_time = toc;
    if(elapsed_time < 1/fps)
        pause((1/fps) - elapsed_time);
    else
        warning(['FPS requirement too high cannot perform everything'...
            ,' in time.']);
    end
end
if(write_to_video_file)
    close(v);
end

