clc
% clear all
close all

global H36m_root;
H36m_root = [pwd, '/'];

addpath('ploting')
addpath('../providedCode/RNNexp/structural_rnn/CRFProblems/H3.6m/dataParser/Utils/')
addpath('../providedCode/RNNexp/structural_rnn/CRFProblems/H3.6m/mhmublv/Motion/')

load('skeletons.mat')
%------------------------<Settings>------------------------%
fps = 25; % frames per second
is_expmap_data = true; % Determines skeleton type
pose_seq = '11'; %  Animates this file number
person = 1; % 1 => S1
show_original_data = false;
show_srnn_data = false;
write_to_video_file = false; %set to true if an avi is desired
my_own_data = false;
%------------------------<\Settings>------------------------%

if show_original_data && show_srnn_data
    error('Both  cant be true at the same time');
end

seq_length = 0;
FPS = 50; % FPS at which data was recorded

if show_srnn_data
%     data = load(['/home/partha/ait-ra-ship/srnnResults/h3.6m/',...
%         'checkpoints_dra_T_150_bs_100_tg_10_ls_10_fc_10_demo/', ...
%         'forecast_iteration_14750_N_', pose_seq]);
data = load(['/home/partha/ait-ra-ship/srnnResults/h3.6m/',...
        'checkpoints_dra_T_150_bs_100_tg_10_ls_10_fc_10_demo/', ...
        'ground_truth_forecast_N_', pose_seq]);
else
    if show_original_data
        if is_expmap_data
            data = load(['../Data/S',num2str(person),...
                '/MyPoseFeatures/D3_Angles/expMap/waiting_1.txt']);
        else
            data = load(['../Data/S',num2str(person),...
                '/MyPoseFeatures/norm3Dpos/Directions.csv']);
        end
    else
        file_name_with_path = ...
            ['../python_models/GeneratedSequences/', pose_seq, '.csv'];
        [original_file_name, seq_star_row, ...
            seq_length, dwon_sampling_rate, FPS] = ...
            parseHeader(file_name_with_path);
        path_components = strsplit(original_file_name, '/');
        seq_name = strsplit(path_components{end}, '.'); seq_name = seq_name{1};
        data = dlmread(file_name_with_path, ',', 9);
    end
end

% if is_angular_data
%     skeleton = skeletons(person).angSkel;
if is_expmap_data
    skeleton = skeletons(person).angSkel;
    c = zeros(1,78);
    [ skel_expmap,~ ] = getExpmapFromSkeleton( skeleton, c );
    R0 = eye(3);
    T0 = [0 0 0];
    [data, ~, ~]= revertCoordinateSpace(data, R0, T0);
    data = getPosFromExpmap(skel_expmap, data);
    [rows, cols] = size(data);
    data = reshape(data', 1, rows*cols);
    
    skeleton = skeletons(person).posSkel;
    cols_actual = length(skeleton.tree) * 3;
    rows_original = rows*cols / cols_actual;
    
    data = reshape(data, cols_actual, rows_original)';
else
    skeleton = skeletons(person).posSkel;
end

if my_own_data || is_expmap_data
    skeleton.type = 'MyOwn';
end

handles = initSkeletonPlot(data(2, :), skeleton);
t = title([seq_name, ' Current time = 0 sec.; FPS = ',  num2str(FPS)]);
colorSkel(handles, show_original_data);
axis_handle = gca;

if(write_to_video_file)
    set(gcf, 'Position', get(0,'Screensize')-[0, 0, 500, 500]);
    v = VideoWriter('newfile.avi');
    v.FrameRate = 16;
    open(v);
end
for i = 2:size(data, 1)
    tic
    %     angles(i, 4:6) = [0, 1.7208, 1.4708];
    if uint64(i) == uint64(seq_length + 1)
        colorSkel(handles, true);
    end
    updateSkeletonPlot(handles, data(i, :), skeleton, true, axis_handle);
    t.String = [seq_name, ' Current time = ', num2str((i-2)/FPS), ...
                'sec. FPS = ', num2str(FPS)];
    if(write_to_video_file)
        F = getframe(gcf);
        writeVideo(v, F);
    end
    
    elapsed_time = toc;
    if(elapsed_time < 1/fps)
        pause((1/fps) - elapsed_time);
    else
        warning(['FPS requirement too high cannot perform everything'...
            ,' in time.']);
    end
end
if(write_to_video_file)
    close(v);
end