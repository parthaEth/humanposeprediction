function copyNegativeSeqs()

%%%%%%%%%%%%%%%%%%% Settigns make sure they are correct%%%%%%%%%%%%%%%%%%%%
num_files_to_copy = 60;
base_dir = '../python_models/GeneratedSequences';
destination_dir = '../negativeSamples';
source_file_type = '.csv';
% NNtype_gamma_noiseDim_noiseType_std
dest_file_name_stub = 'CVAE_latentSpaceSmoothness_lessTraining';
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

cpyNgtvSqs(num_files_to_copy, base_dir,...
           destination_dir, source_file_type, ...
           dest_file_name_stub)
disp('<----------------All Done-------------------->')
end

function cpyNgtvSqs(num_files_to_copy, base_dir,...
                          destination_dir, source_file_type, ...
                          dest_file_name_stub)
    if base_dir(end) ~= '/'
        base_dir = [base_dir, '/'];
    end
    
    if destination_dir(end) ~= '/'
        destination_dir = [destination_dir, '/'];
    end
    
    file_attribs = dir([base_dir, '*', source_file_type]);
    
    count_files_done = 1;
    for i = 1:length(file_attribs)
        file_name_with_path = ...
            [base_dir, file_attribs(count_files_done).name];
        [~, ~, seq_length,...
         dwon_sampling_rate, ~] = parseHeader(file_name_with_path);
     
        if dwon_sampling_rate ~=2
            error(['This file has an FPS other than 25 so might be ',...
                   'incompatable with others'])
        end
        
        dest_file_name = ...
            [dest_file_name_stub, num2str(count_files_done), '.txt'];
        dest_file_name_and_path = [destination_dir, dest_file_name];
        if exist(dest_file_name_and_path, 'file')
            error('Files from this setting is already available. Try others')
        end
        
        generated_data = dlmread(file_name_with_path, ',', 9 + seq_length);
        
        if size(generated_data, 2) < 30
            continue
        end
        
        dlmwrite(dest_file_name_and_path, generated_data, 'delimiter', ',')
        if count_files_done >= num_files_to_copy
            break
        end
        count_files_done = count_files_done + 1;
    end
    if count_files_done < num_files_to_copy
        error(['Could not find asked number of files copied ',...
            num2str(count_files_done), 'instead']);
    end
end