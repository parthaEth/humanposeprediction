clc
% clear all
close all

global H36m_root;
H36m_root = [pwd, '/'];

addpath('ploting')

pose_seq = '12';
person = 1;

load('skeletons.mat')

angular_data = false;

if angular_data
    base_dir = '../Data/S1/MyPoseFeatures/D3_Angles/expMap';
    skel = skeletons(person).angSkel;
else
    base_dir = '../Data/S1/MyPoseFeatures/norm3Dpos';
    skel = skeletons(person).angSkel;
    skel.type = 'MyOwn';
end

files = dir([base_dir,'/', '*.csv']);

num_skels = 1;
% roll = zeros(num_skels, 1);
% pitch = zeros(num_skels, 1);
% yaw = zeros(num_skels, 1);
for i=1:num_skels
    files(i)
    data = load([base_dir,'/', files(i).name]);
    handles = initSkeletonPlot(data(1, :), skel, 1);
%     roll(i) = data(1, 1);
%     pitch(i) = data(1, 2);
%     yaw(i) = data(1, 3);
end
figure
plot(data(:, 1))
hold on
plot(data(:, 2))
plot(data(:, 10))
% plot(roll, 'r')
% hold on
% plot(pitch, 'g')
% plot(yaw, 'b')