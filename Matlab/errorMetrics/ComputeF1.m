clear all
load('dependency_matrices.mat')

actions = {'walking_1', 'eating_1', 'smoking_1', 'discussion_1'};
num_actions = length(actions);

networks = {'Groundtruth', 'LSTM3LR', 'AutoEncoderLSTM', 'ERD'};
num_nets = length(networks);

seq_lengths = [50, 100, 150, 200];
num_seqs = length(seq_lengths);


all_scores = zeros(num_nets, num_actions, num_seqs);

for action_idx = 1:num_actions
    for net_idx = 2:num_nets
        for seq_idx = 1:num_seqs
            current_key = [networks{net_idx}, '_', ...
                       actions{action_idx}, '_',...
                       num2str(seq_lengths(seq_idx))];
                   
            groundtruth_key = ['Groundtruth', '_', ...
                       actions{action_idx}, '_',...
                       num2str(seq_lengths(seq_idx))];
            
            current_dep_matrix = dependency_matrices(current_key);   
            current_dep_matrix = binarizeDepMats(current_dep_matrix);
            
            true_dep_matrix = dependency_matrices(groundtruth_key);
            true_dep_matrix = binarizeDepMats(true_dep_matrix);
            
            f1 = computeF1forDependencyMatrices(true_dep_matrix, ...
                current_dep_matrix);
            
            all_scores(net_idx, action_idx, seq_idx) = f1;
            
        end
    end
end

save('all_scores.mat', 'all_scores')