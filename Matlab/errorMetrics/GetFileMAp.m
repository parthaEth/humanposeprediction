clc
clear all
close all

addpath('../ploting')
addpath('../')
addpath('../../providedCode/RNNexp/structural_rnn/CRFProblems/H3.6m/dataParser/Utils/')
addpath('../../providedCode/RNNexp/structural_rnn/CRFProblems/H3.6m/mhmublv/Motion')

base_path = '../../python_models/Classifications/';

for file_idx = 1:60
    full_path = [base_path, 'categorical_', num2str(file_idx),'.txt'];
    [original_file_name, ~, seq_length, ~, ~] = parseHeader(full_path);
    [original_file_name, ~, seq_length, ~, ~] = ...
        parseHeader(original_file_name);
    
    disp([num2str(file_idx), original_file_name])
end