clc
clear all
close all
% This file computes Pearson correlation between different major bodyparts
% such as the legs, the hands and the spine

addpath('../ploting')
addpath('../')
addpath('../../providedCode/RNNexp/structural_rnn/CRFProblems/H3.6m/dataParser/Utils/')
addpath('../../providedCode/RNNexp/structural_rnn/CRFProblems/H3.6m/mhmublv/Motion')


actions = {'walking', 'eating', 'smoking', 'discussion', 'all'};
indices = {[47, 48, 17, 18], [11, 12, 43, 44], [13, 14, 59, 60], ...
           [5, 41, 42, 6], 1:60};
% indices = {[1,6], 8, [24, 4], [10, 12], 1:30};
actions_to_seq = containers.Map(actions, indices);
for ac_idx=1:4
    for time_steps_to_consider = [50, 100, 200, 300]
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% Settings %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        retrain_model = false;
        generated_data = true;
        subject = '1';
        generated_data_file_type = '.csv';
        action = actions{ac_idx};
        org_data_file_type = '.txt';
        threshold_band = 0.5;
        offset = 20;
        important_parts = {'RightLeg', 'LeftLeg', 'Spine', 'LeftArm', 'RightArm'};
        seed_seq_length = 200;

%         network_name = 'LSTM3LR';
%         network_name = 'AutoEncoderLSTM';
        network_name = 'ERD';
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        generated_files = actions_to_seq(action); % files to consider
        if ~generated_data
            network_name = 'Groundtruth';
        end

        if generated_data
            base_path = '../../python_models/GeneratedSequences/';
        else
            base_path = ...
                ['../../Data/S', subject, ...
                 '/MyPoseFeatures/D3_Angles/expMap/'];
        end

        % Load originl data

        load('../skeletons.mat')
        skeleton = skeletons(str2double(subject)).angSkel;

        R0 = eye(3);
        T0 = [0 0 0];
        c = zeros(1,78);
        [ skel_expmap,~ ] = getExpmapFromSkeleton( skeleton, c );

        if ~generated_data
            dir_atrib = dir([base_path, '*', org_data_file_type]);
            file_indices = 1:length(dir_atrib);
        else
            file_indices = generated_files;
        end

        %Reading the files and storing all time sequence data
        pearson_correlation = zeros(length(important_parts));
        for file_idx = file_indices
            if generated_data
                full_path = [base_path, num2str(file_idx),...
                             generated_data_file_type];
                if ~(exist(full_path, 'file') == 2)
                    continue
                end
%                 [~, ~, seq_length, ~, ~] = parseHeader(full_path);
                seq_data_G = dlmread(full_path, ',', 9 + offset);
                seq_data_G = seq_data_G(1:time_steps_to_consider, :);
            else
                current_file = dir_atrib(file_idx).name;
                if ~strcmp(current_file(1:end-6), action) && ~strcmp(action, 'all')
                    continue
                end
                full_path = [base_path, current_file];
                seq_data_G = load(full_path);
                seq_data_G = ...
                    seq_data_G(1:2:time_steps_to_consider * 2, :);
            end

            [seq_data_G, ~, ~] = ...
                revertCoordinateSpace(seq_data_G, R0, T0);
            seq_data_G = getPosFromExpmap(skel_expmap, seq_data_G);

            [rows, cols] = size(seq_data_G);
            cols_actual = length(skeleton.tree) * 3;
            rows_original = rows*cols / cols_actual;
            seq_data_G = reshape(seq_data_G', 1, rows*cols);
            seq_data_G = reshape(seq_data_G, cols_actual, rows_original)';
            %     seq_data_G(2:end, :) = seq_data_G(2:end, :) - seq_data_G(1:end-1, :);

            % Get only the important joint data. Each important part corresponds to a
            % column
            time_steps = size(seq_data_G, 1);
            cropped_data_mat = zeros(time_steps, length(important_parts) * 3);

            part_count = 0;
            for i = 1:length(skeletons(str2double(subject)).posSkel.tree)
        %         skeletons(str2double(subject)).posSkel.tree(i).name
                if strcmp(skeletons(str2double(subject)).posSkel.tree(i).name,...
                        important_parts{part_count + 1})
                    cropped_data_mat(:, part_count*3+1 : part_count*3+3) = ...
                        seq_data_G(:, skeletons(str2double(subject)).posSkel.tree(i).posInd);
                    if part_count == length(important_parts)-1
                        break
                    else
                        part_count = part_count + 1;
                    end
                end
            end

            if part_count < length(important_parts)-1
                error('you messaed with the part ordering.')
            end

            % Computing perarson correlation coefficient for each column
            cropped_data_mat = normc(cropped_data_mat);
        %     cov_mat = corr(cropped_data_mat, 'type', 'Spearman');
            dependency_strength_mat = colDependency(cropped_data_mat);
        %     std_each_col = std(cropped_data_mat);
        %     var_prod = std_each_col' * std_each_col;
            pearson_correlation = pearson_correlation + dependency_strength_mat;%./var_prod;

            if generated_data
                disp(['Done with ', num2str(file_idx), generated_data_file_type])
            else
                disp(['Done with ', dir_atrib(file_idx).name])
            end
        end
        pearson_correlation_avg = abs(pearson_correlation)./file_indices(end);

        % part_names = cell(1, 3*(part_count + 1));
        % for i = 1:3*(part_count + 1)
        %     part_names{i} = '     ';
        % end
        % for i = 1:part_count + 1
        %     part_names{(i -1)*3 + 1} = important_parts{i};
        % end
        % pearson_correlation = abs(pearson_correlation - mean(mean(pearson_correlation)));
        % pearson_correlation(pearson_correlation < threshold_band) = 0;

        % for i=1:(part_count + 1)
        %     pearson_correlation_col_avg(:, i) = ...
        %         mean(pearson_correlation(:, (3*i-2):3*i), 2);
        % end
        % 
        % for i=1:(part_count + 1)
        %     pearson_correlation_avg(i, :) = ...
        %         mean(pearson_correlation_col_avg((3*i-2):3*i, :));
        % end
        % Rescale in 0 to 1
        minval = min(min(pearson_correlation_avg));
        pearson_correlation_avg = pearson_correlation_avg - minval;
        max_ater_min_sub = max(max(pearson_correlation_avg));
        pearson_correlation_avg = pearson_correlation_avg./max_ater_min_sub;


%         HeatMap(pearson_correlation_avg, ...
%             'Colormap', redbluecmap(), 'ColumnLabels', important_parts,...
%             'RowLabels', important_parts)

        humanStructureGivenCorr(pearson_correlation_avg)

        if exist('dependency_matrices.mat', 'file') == 2
            load('dependency_matrices.mat')
        else
            dependency_matrices = containers.Map();
        end

        current_key = [network_name, '_', action, '_',...
            num2str(time_steps_to_consider)];
%         if isKey(dependency_matrices, current_key)
%             x = input(...
%                 'Results for this network is already in record over write(y/n)?');
%             if x~= 'y' && x~= 'Y'
%                 clear all
%                 return
%             end
%         end

        dependency_matrices(current_key) = pearson_correlation_avg;

        save('dependency_matrices.mat', 'dependency_matrices')
    end
end

