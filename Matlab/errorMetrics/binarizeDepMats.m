function bin_dep_matrix = binarizeDepMats(true_dep_matrix)

bin_dep_matrix = zeros(size(true_dep_matrix));

for i = 1:length(true_dep_matrix)
    thres = median(true_dep_matrix(:, i));
    for j = 1:length(true_dep_matrix)
        if true_dep_matrix(j, i) <= thres
            bin_dep_matrix(j, i) = 1;
        end
    end
end
end