function dep_mat = colDependency(data)
[~, cols] = size(data);
dep_mat = zeros(cols/3);
dep_mat_row = 1;
for i = 1:3:cols
    dep_mat_col = dep_mat_row + 1;
    for col = i+3:3:cols
        rmse = 0;
        for j = 0:2
            mdl = NonLinearModel.fit(data(:, i:i+2), data(:, col + j),...
                                     @nonLinFunctionFormToFit, ...
                                     0.1*ones(1, 27));
            rmse = rmse + mdl.RMSE;
        end
        dep_mat(dep_mat_row, dep_mat_col) = rmse/3;
        dep_mat(dep_mat_col, dep_mat_row) = rmse/3;
        dep_mat_col = dep_mat_col + 1;
    end
    dep_mat_row = dep_mat_row + 1;
end
end

function predicted_pose = nonLinFunctionFormToFit(params, x)
% 2nd_order dense model
predicted_pose = params(1) * x(:, 1) + ...
    params(2) * x(:, 2) + params(3) * x(:, 3) + ...
    params(4) * x(:, 1).^2 + params(5) * x(:, 2).^2 + ...
    params(6) * x(:, 3).^2 + params(7) * x(:, 1) .* x(:, 2) + ...
    params(8) * x(:, 1) .* x(:, 3) + ...
    params(9) * x(:, 2) .* x(:, 3);
end