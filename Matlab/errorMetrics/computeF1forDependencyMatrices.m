function f1 = computeF1forDependencyMatrices(true_dep_matrix,...
    current_dep_matrix)

true_positive = 0.00000000001;
flase_positive = 0.00000000001;
flase_negative = 0.00000000001;

for i = 1:length(true_dep_matrix)
    for j = 1:length(true_dep_matrix)
        if true_dep_matrix(i, j) == 1 && current_dep_matrix(i, j) == 1
            true_positive = true_positive + 1;
        elseif current_dep_matrix(i, j) == 1 && true_dep_matrix(i, j) == 0
            flase_positive = flase_positive + 1;
        elseif current_dep_matrix(i, j) == 0 && true_dep_matrix(i, j) == 1
            flase_negative = flase_negative + 1;
        end
    end
end

precision = true_positive/(true_positive + flase_positive);
recall = true_positive/(true_positive + flase_negative);

f1 = 2 * (precision * recall) / (precision + recall);
end