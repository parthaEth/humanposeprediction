function humanStructureGivenCorr( pearson_correlation, axis_h )
%HUMANSTRUCTUREGIVENCORR Draws the st-graph given pearson_correlation
%matrix of limbs
addpath('../ploting')

if nargin < 2
    figure
else
    axes(axis_h)
end

raddi = 0.25;
parts = {'RightLeg', 'LeftLeg', 'Spine', 'LeftArm', 'RightArm'};
centres = [1.5, -5;
          -1.5, -5;
             0,  0;
            -2,  -1;
             2,  -1];
el_centres = [1.5, -5 - raddi;
             -1.5, -5 - raddi;
                0,  0 + raddi;
               -2 - raddi,  -1;
                2 + raddi,  -1];
phi = [0, 0, 0, pi/2, pi/2];

viscircles(centres(3, :), raddi);
hold on
viscircles(centres(2, :), raddi, 'LineWidth', 3, 'Color', 'r');
viscircles(centres(1, :), raddi, 'LineWidth', 3, 'Color', 'r');
viscircles(centres(4, :), raddi, 'LineWidth', 3, 'Color', 'r');
viscircles(centres(5, :), raddi, 'LineWidth', 3, 'Color', 'r');
axis equal

for i = 1:length(parts)
    thres = median(pearson_correlation(:, i));
    for j = 1:length(parts)
        if pearson_correlation(j, i) <= thres
            if i==j
                el = drawEllipse(2*raddi, raddi, phi(i), el_centres(i, 1),...
                    el_centres(j, 2), 'b');
                el.LineWidth = 3;
            else
                line(centres([i,j], 1), centres([i,j], 2), 'LineWidth', 3, 'color', 'b');
            end
        end
    end
end

axis off

end

