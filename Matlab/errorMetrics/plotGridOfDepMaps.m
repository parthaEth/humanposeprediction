clear all
load('dependency_matrices.mat')

actions = {'walking_1', 'eating_1', 'smoking_1', 'discussion_1'};
networks = {'Groundtruth', 'LSTM3LR', 'ERD', 'AutoEncoderLSTM'};
seq_lengths = [50, 100, 150, 200, 250];
time_idx = 3;
FPS = 25;

for time_idx = 1:length(seq_lengths)
    figure
    suptitle(['window_width = ', num2str(seq_lengths(time_idx))])
    for network_idx = 1:length(networks)
        for action_idx = 1:length(actions)
            current_key = [networks{network_idx}, '_', ...
                           actions{action_idx}, '_',...
                           num2str(seq_lengths(time_idx))];

            subplot(length(seq_lengths), length(networks), ...
                (action_idx-1)*length(networks) + network_idx)
            if isKey(dependency_matrices, current_key)
                dependency_matrices(current_key) = ...
                    dependency_matrices(current_key) + 1e5*eye(size(dependency_matrices(current_key)));
                humanStructureGivenCorr(-dependency_matrices(current_key), gca)
            else
                continue;
            end

            if network_idx == 1
                ylabel([actions(action_idx)], 'FontSize', 20, 'Interpreter', 'none')
                set(get(gca,'YLabel'),'visible','on')
            end

            if action_idx == 1
                title(networks{network_idx}, 'FontSize', 20)
            end

        end
    end
end
