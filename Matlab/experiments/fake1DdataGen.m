% Generates fake sinosoidal data

amplitude = 5;
freq = 1;
time_len_insec = 10;
samplingrate = 100;
num_seqs = 200;
base_dir = '~/ait-ra-ship/Data/S1/MyPoseFeatures/fakeTestData/';

time_vec = 0:1/samplingrate:time_len_insec;

for i=1:num_seqs
    freq = rand(1,1);
    phase = (rand(1,1) - 0.5)*pi;
    current_seq = sin(freq * 2*pi*time_vec + phase);
    filename = [base_dir, num2str(i), '.csv'];
    csvwrite(filename, current_seq')
end