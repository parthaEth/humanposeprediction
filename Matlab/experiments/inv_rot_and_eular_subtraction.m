% Euler angle subtraction and rotation inverse
clc

addpath('../rotationMath')

order = 'xyz';
euler_random = rand(1, 3) - 0.5;
euler_random2 = euler_random + 0.001 * (rand(1, 3) - 0.5);

q_init = angle2quat(euler_random(1), ...
                    euler_random(2), ...
                    euler_random(3), ...
                    order);
q_fin = angle2quat(euler_random2(1), ...
                   euler_random2(2), ...
                   euler_random2(3), ...
                   order);

q_rot = quatmultiply(q_fin, quatinv(q_init));
[euler_rot(1), euler_rot(2), euler_rot(3)]= quat2angle(q_rot, order);
euler_rot

euler_rot_cmp = euler_random2 - euler_random