clc
close all
%Add utility paths
addpath('../providedCode/Release-v1.1/H36M');
addpath('../providedCode/Release-v1.1/H36MBL');
addpath('../providedCode/Release-v1.1/H36MGUI');
addpath(genpath('../providedCode/Release-v1.1/utils'));
addpath(genpath('../providedCode/Release-v1.1/external_utils'));

fno = 1;
for i = 1:11
    Sequence = H36MSequence(i,13,2,1,fno);
    Subject = Sequence.getSubject();
    skeletons(i).posSkel = Subject.getPosSkel();
    skeletons(i).angSkel = Subject.getAnglesSkel();
end

save('skeletons.mat', 'skeletons');