% This script makes the subplots with skeletons from different time steps

addpath('../ploting')
addpath('../')
addpath('../../providedCode/RNNexp/structural_rnn/CRFProblems/H3.6m/dataParser/Utils/')
addpath('../../providedCode/RNNexp/structural_rnn/CRFProblems/H3.6m/mhmublv/Motion')


% -------------- Settings --------------%
path_to_seqs = '../../python_models/GeneratedSequences/';
seq_number = '17';
is_expmap_data = true;
% network_name = 'LSTM3LR';
% network_name = 'DropoutEncoder';
network_name = 'ERD';
% network_name = 'GroundTruth';


person = 1;
R0 = eye(3);
T0 = [0 0 0];
% -------------- Settings --------------%

if exist('timed_skeletons.mat', 'file') == 2
    load('timed_skeletons.mat')
else
    timed_skeletons = containers.Map();
    load_grund_truth = true;
end

if isKey(timed_skeletons, network_name)
    x = input(...
        'Results for this network is already in record over write(y/n)?');
    if x~= 'y' && x~= 'Y'
        clear all
        return
    end
end

file_name_with_path = [path_to_seqs, seq_number, '.csv'];

[original_file_name, seq_start_row, ...
 seq_length, dwon_sampling_rate, FPS] = parseHeader(file_name_with_path);
path_components = strsplit(original_file_name, '/');
seq_name = strsplit(path_components{end}, '.'); seq_name = seq_name{1};

if strcmp(network_name, 'GroundTruth')
    generated_data = load(original_file_name);
else
    generated_data = dlmread(file_name_with_path, ',', 9);
end

load('../skeletons.mat')
skeleton = skeletons(person).angSkel;
c = zeros(1,78);
[ skel_expmap,~ ] = getExpmapFromSkeleton( skeleton, c );

if ~strcmp(network_name, 'GroundTruth')
    generated_data = repelem(generated_data, dwon_sampling_rate, 1);
end
[generated_data, ~, ~]= revertCoordinateSpace(generated_data, R0, T0);
generated_data = generated_data(1:dwon_sampling_rate:end, :);

generated_data = getPosFromExpmap(skel_expmap, generated_data);
[rows, cols] = size(generated_data);
generated_data = reshape(generated_data', 1, rows*cols);

skeleton = skeletons(person).posSkel;
cols_actual = length(skeleton.tree) * 3;
rows_original = rows*cols / cols_actual;
    
generated_data = ...
    reshape(generated_data, cols_actual, rows_original)';

skeleton.type = 'MyOwn';

time_stamps_to_pick_in_ms = ...
    [-1, 100, 200, 300, 400, 500, 600, 700, 800, 1000, 1200];
indices_to_pick = seq_length + ...
    round(FPS * time_stamps_to_pick_in_ms / 1000);

timed_skeletons(network_name) = generated_data(indices_to_pick, :);
save('timed_skeletons.mat', 'timed_skeletons')


for i = indices_to_pick
    handles_generated = ...
        initSkeletonPlot(generated_data(i, :),...
                         skeleton);
end

