% Loads and plots the time stamped skeletons of different networks
clear all

load('timed_skeletons.mat')

addpath('../ploting')
addpath('../')
addpath('../../providedCode/RNNexp/structural_rnn/CRFProblems/H3.6m/dataParser/Utils/')
addpath('../../providedCode/RNNexp/structural_rnn/CRFProblems/H3.6m/mhmublv/Motion')

load('../skeletons.mat')
skeleton = skeletons(1).posSkel;
skeleton.type = 'MyOwn';

all_keys = {'GroundTruth', 'LSTM3LR', 'ERD', 'DropoutEncoder'};
cols = length(all_keys);
rows = 10;

col_idx = 1;
for key = all_keys
    poses = timed_skeletons(key{1});
    for i = 0:rows-1
        subplot(rows, cols, i * cols + col_idx)
        if i == 0
            temp_poses = timed_skeletons('GroundTruth');
            handles_generated = initSkeletonPlot(temp_poses(i+1, :),...
                                                 skeleton, gca);
        else
            handles_generated = initSkeletonPlot(poses(i+1, :),...
                                                 skeleton, gca);
        end
%         margin = 1200;

        set(gca,'xticklabel',[])
        set(gca,'xlabel',[])
%         xlim([-margin + poses(1, 1), margin + poses(1, 1)])
        
        set(gca,'yticklabel',[])
        set(gca,'ylabel',[])
%         ylim([-margin + poses(1, 2), margin + poses(1, 2)])
      
        set(gca,'zticklabel',[])
        set(gca,'zlabel',[])
        view([0, 0.3, -1]);
%         zlim([-margin + poses(1, 3), margin + poses(1, 3)])
        grid on
        
        for j = 2:length(handles_generated)
            handles_generated(j).LineWidth = 0.25;
            if i == 0 || strcmp(key{1}, 'GroundTruth')
%                 handles_generated(j).Color = 'g';
                  colorSkel(handles_generated, false);
                if i == 0
                    title(key{1})
                end
%             else
%                 handles_generated(j).Color = 'b';
            end
        end
%         hold on
%         if i == 0 || strcmp(key{1}, 'GroundTruth')
%             plot3(poses(i+1, 1:3:end), poses(i+1, 2:3:end),...
%                   poses(i+1, 3:3:end), '.', 'markersize', 10,...
%                   'Color', 'g')
%         else
%             plot3(poses(i+1, 1:3:end), poses(i+1, 2:3:end),...
%                   poses(i+1, 3:3:end), '.', 'markersize', 10,...
%                   'Color', 'b')
%         end
    end
    col_idx = col_idx + 1;
end
print('-painters', '-depsc', 'qualitative_longTerm.eps', '-r1200' )