function [original_file_name, seq_star_row,...
          seq_length, dwon_sampling_rate, FPS] = ...
    parseHeader(file_name_with_path)
    fid = fopen(file_name_with_path);
    
    original_file_name = textscan(fid, '%s', 1, 'delimiter','\n');
    original_file_name_start_index = strfind(original_file_name{1}, '=');
    original_file_name = ...
        original_file_name{1}{1}(original_file_name_start_index{1} + 2:end);
    
    rest_of_header = textscan(fid, '%s %c %f %c', 4, 'delimiter',' ');
    seq_star_row = rest_of_header{1,3}(1);

    seq_length = rest_of_header{1,3}(2);

    dwon_sampling_rate = rest_of_header{1,3}(4);
     
    FPS = rest_of_header{1,3}(3);
    
    fclose(fid);
end