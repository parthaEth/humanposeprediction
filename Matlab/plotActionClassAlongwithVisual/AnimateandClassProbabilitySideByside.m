clc
clear all
close all
% This function animates groundtruth and predicted sequence side by side or
% on top of each other for visual comparison

addpath('../')
addpath('../ploting')
addpath('../../providedCode/RNNexp/structural_rnn/CRFProblems/H3.6m/dataParser/Utils/')
addpath('../../providedCode/RNNexp/structural_rnn/CRFProblems/H3.6m/mhmublv/Motion/')

action_classes = {'directions', 'discussion', 'eating', 'greeting', ...
       'phoning', 'posing', 'purchases', 'sitting', 'sittingdown', ...
       'smoking', 'takingphoto', 'waiting', 'walking', 'walkingdog', ...
       'walkingtogether', 'unnatural'};
actin_class_index = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16};
action_classes_map = containers.Map(action_classes, actin_class_index);


%% -------------- Settings --------------%%
path_to_seqs = '../../python_models/Classifications/';
fps = 25; % This is just display fps

% Walking - [9, 17, 54, 60]
% Eating - [19, 25, 34, 41]
% Smoking - [37, 52, 11, 12]
% Discussion - [15, 29, 53, 33]
seq_number = '19';
express_error_in_normalized_scale = true;
write_to_video_file = true; %set to true if an avi is desired
my_own_data = false;
is_expmap_data = true;
skip_first_n_frames = 1;
person = 1; % Whose skeleton to use. Data has been normalized with s1
R0 = eye(3);
T0 = [0 0 0];

% -------------- Settings --------------%%

%% -------------- Reading the Classification file --------------------- %%

file_name_with_path = [path_to_seqs, 'categorical_', seq_number, '.txt'];
[corresponding_generated_file, ~, ~, ~, ~] = ...
    parseHeader(file_name_with_path);

[original_file_name, seq_start_row, seq_length, dwon_sampling_rate,...
    FPS] = parseHeader(corresponding_generated_file);

generated_file_componnents = strsplit(original_file_name, '/');
generated_action_class = strsplit(generated_file_componnents{end}, '.');
generated_action_class = generated_action_class{1}(1:end-2);

class_probabilities = dlmread(file_name_with_path, ',', 9);

% plot(class_probabilities, 'LineWidth', 2, 'MarkerSize', 6)
% hold on 
% legend('directions', 'discussion', 'eating', 'greeting', 'phoning', ...
%        'posing', 'purchases', 'sitting', 'sittingdown', 'smoking', ...
%        'takingphoto', 'waiting', 'walking', 'walkingdog', ...
%        'walkingtogether')

% -------------- Reading the Classification file Done ----------------- %%

%% --------------- Reading the Generated file ------------------------- %%

path_components = strsplit(original_file_name, '/');
seq_name = strsplit(path_components{end}, '.'); seq_name = seq_name{1};

% python index to matlab conversion 
seq_start_row = seq_start_row * dwon_sampling_rate + 1;

generated_data = dlmread(corresponding_generated_file, ',', 9);
generated_data(:, [4, 6]) = 0;
    
load('skeletons.mat')

if is_expmap_data
    skeleton = skeletons(person).angSkel;
    c = zeros(1,78);
    [ skel_expmap,~ ] = getExpmapFromSkeleton( skeleton, c );
    
    generated_data = repelem(generated_data, dwon_sampling_rate, 1);
    [generated_data, ~, ~]= revertCoordinateSpace(generated_data, R0, T0);
    generated_data = generated_data(1:dwon_sampling_rate:end, :);
    
    generated_data = getPosFromExpmap(skel_expmap, generated_data);
    [rows, cols] = size(generated_data);
    generated_data = reshape(generated_data', 1, rows*cols);
    
    skeleton = skeletons(person).posSkel;
    cols_actual = length(skeleton.tree) * 3;
    rows_original = rows*cols / cols_actual;

    generated_data = reshape(generated_data, cols_actual, rows_original)';
    
else
    skeleton = skeletons(person).posSkel;
end

if my_own_data || is_expmap_data
    skeleton.type = 'MyOwn';
end


% expPlayData(skel_expmap, drawable_original, 1/FPS)


f = figure;
set(f, 'Position', get(0,'Screensize')-[0, -499, 500, 500]);
t = suptitle([generated_action_class, ', ', 'Current time = 0 sec.; FPS = ',  num2str(FPS)]);

subplot(1, 2, 1)
% title(generated_action_class, 'Interpreter', 'none')
axis_handle_class_proba = gca;
probability_handle = ...
    initProbabilityPlot(class_probabilities(skip_first_n_frames, :),...
                        axis_handle_class_proba, ...
                        action_classes_map(generated_action_class));


subplot(1, 2, 2)
% title(generated_action_class, 'Interpreter', 'none')
axis_handle_generated = gca;
% plot3(drawable_generated(:, 1), drawable_generated(:, 2),...
%       drawable_generated(:, 3))
handles_generated = ...
    initSkeletonPlot(generated_data(skip_first_n_frames, :),...
                     skeleton, axis_handle_generated);
colorSkel(handles_generated, false);

if(write_to_video_file)
    set(gcf, 'Position', get(0,'Screensize')-[0, -499, 500, 500]);
    v = VideoWriter(['OriginalAndPredictedSidebyside', generated_action_class, '.avi']);
    v.FrameRate = 16;
    open(v);
end
for i = (skip_first_n_frames + 1):size(generated_data, 1)
    tic
    %     angles(i, 4:6) = [0, 1.7208, 1.4708];
    if uint64(i) >= uint64(seq_length + 1)
        colorSkel(handles_generated, true);
    end
    updateProbabilityPlot(class_probabilities(skip_first_n_frames + 1:i, :),...
                          axis_handle_class_proba, ...
                          probability_handle, ...
                          action_classes_map(generated_action_class))
    updateSkeletonPlot(handles_generated, generated_data(i, :),...
                       skeleton, true, axis_handle_generated);
    
    t.String = [generated_action_class, ', ', ...
                'Current time = ', num2str((i-2)/FPS), ...
                'sec. FPS = ', num2str(FPS)];
    if(write_to_video_file)
        F = getframe(gcf);
        writeVideo(v, F);
    end
    
    elapsed_time = toc;
    if(elapsed_time < 1/fps)
        pause((1/fps) - elapsed_time);

    else
        warning(['FPS requirement too high cannot perform everything'...
            ,' in time.']);
    end
end
if(write_to_video_file)
    close(v);
end

