clc
% clear all
close all

global H36m_root;
H36m_root = [pwd, '/'];

addpath('ploting')

pose_seq = 12;
person = 1;

load('skeletons.mat')

angles = cdfread('../Data/S1/MyPoseFeatures/D3_Angles/Directions.cdf');
angles = angles{1};
showPose(angles(pose_seq, :), skeletons(person).angSkel)
angles(pose_seq, 4) = 0;
showPose(angles(pose_seq, :), skeletons(person).angSkel)
