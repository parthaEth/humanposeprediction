clc
clear all
close all

train_hist = load('../python_models/training_history.csv');

plot(train_hist(:, 1), 'r')
hold on
plot(train_hist(:, 2), 'g')
legend('Train\_loss', 'Validation\_loss')

xlabel('Epochs\rightarrow')
