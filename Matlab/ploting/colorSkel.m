function colorSkel( handles, color_ful )
%COLORSKEL colors the skeleton grey if color_grey flag is true
%   handles = Line segment handles of a skeleton
%   color_grey = color flag

if color_ful
    c = 'bggggbrrrrbbbbbbrrrrrrrbggggggg';
    
    for i = 1:length(handles) - 1
        set(handles(i+1), 'Color', c(i));
    end
else
    for i = 1:length(handles) -1
        set(handles(i+1), 'Color', [0.25, 0.25, 0.25]);
    end
end

end

