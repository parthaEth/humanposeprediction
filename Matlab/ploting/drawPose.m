function [ handles ] = drawPose( channels, skel, transparency )
%DRAWPOSE Draws pose given skeleton and data
if nargin < 3
    transparency = 1;
end
if ~strcmp(skel.type, 'MyOwn')
    error('Cant use thiskeleton');
end

connect = skelConnectionMatrix(skel);
vals = reshape(channels, 3, length(skel.tree))';
[I, J] = find(connect);
% handle(1) = plot3(vals(:, 1), vals(:, 2), vals(:, 3), '.');
%axis ij % make sure the left is on the left.
% set(handle(1), 'markersize', 10);
hold on
grid on
for i = 1:length(I)
    % modify with show part (3d geometrical thing)
    if strncmp(skel.tree(I(i)).name,'L',1)
        c = 'r';
    elseif strncmp(skel.tree(I(i)).name,'R',1)
        c = 'g';
    else
        c = 'b';
    end
    
    % FIXME
    handles(i+1) = plot3([vals(I(i), 1) vals(J(i), 1)], ...
        [vals(I(i), 2) vals(J(i), 2)], ...
        [vals(I(i), 3) vals(J(i), 3)],'Color',c);
    set(handles(i+1), 'linewidth', 3);
    handles(i+1).Color(4) = transparency;
%     text(vals(I(i), 1), vals(I(i), 2), vals(I(i), 3), skel.tree(I(i)).name)
    xlabel('x')
    ylabel('z')
    zlabel('y')
    axis on
    axis equal
    
end
end

