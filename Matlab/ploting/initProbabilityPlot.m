function [ handles ] = initProbabilityPlot( current_proba, axis_hdl, class_index )
%INITPROBABILITYPLOT initializes class probability plot and returns marker
%handle


AxesLineStyleOrder = {'-', ':', '-.'};
AxesColorOrder = [0 0 1; % blue
                  1 0 0; % red
                  0 1 1; % cyan
                  1 0 1; % magenta
                  0 0 0]; % Black

if nargin < 2
    figure
else
    axes(axis_hdl)
end
color_count = mod(class_index, size(AxesColorOrder, 1)) + 1;
marker_handle = plot(current_proba(class_index), 'o', 'MarkerSize', 20,...
                     'color', AxesColorOrder(color_count, :), ...
                     'LineWidth', 8);
handles = cell(1, size(current_proba, 2));

for i = 1:size(current_proba, 2)
    color_count = mod(i, size(AxesColorOrder, 1)) + 1;
    style_count = mod(floor(i/ size(AxesColorOrder, 1)), ...
                      length(AxesLineStyleOrder)) + 1;
    handles{i} = patchline(0, current_proba(i), 'Color', ...
                      AxesColorOrder(color_count, :), 'LineStyle', ...
                      AxesLineStyleOrder{style_count}, 'LineWidth', 3, 'edgealpha',0.2);
    if i == class_index
        handles{i} = {handles{i}, marker_handle}; 
    end
end

l = legend([handles{1:class_index-1}, handles{class_index}{1}, handles{class_index+1 : end}], ...
           {'directions', 'discussion', 'eating', 'greeting', 'phoning', ...
       'posing', 'purchases', 'sitting', 'sittingdown', 'smoking', ...
       'takingphoto', 'waiting', 'walking', 'walkingdog', ...
       'walkingtogether'});
% 
% set(l, 'FontSize', 18)

xlim([0, 350])
ylim([0, 1])
end

