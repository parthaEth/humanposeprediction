function [ handles ] = initSkeletonPlot( channels, skel, axis_handle, transparency)
%INITSKELETONPLOT initializes plot of a skeleton. Returns line handles
%which can be updated later for annimation. Channels are assumed to be
%3d-angles in exponential map format
global H36m_root;
addpath([H36m_root, '/rotationMath'])
addpath(genpath([H36m_root, '../providedCode']));

if nargin < 4
    transparency = 1;
end

if(nargin < 3)
    figure;
else
    axes(axis_handle)
end

if ~strcmp(skel.type, 'MyOwn')
    if ~isempty(skel.tree(1).rotInd) % Otherwise it is a position skeleton
%         channels(4:6) = channels(4:6) * 180/pi;
        
        for i = 1:length(skel.tree)
            if(~isempty(skel.tree(i).rotInd))
                xangle = channels(skel.tree(i).rotInd(1));
                yangle = channels(skel.tree(i).rotInd(2));
                zangle = channels(skel.tree(i).rotInd(3));
                rotation_quat = expMap2Quat([yangle, zangle, xangle]);
                [r1, r2, r3] = quat2angle(rotation_quat, skel.tree(i).order);
                starting_index = min(skel.tree(i).rotInd);
                channels(starting_index : starting_index+2) = ...
                    [r1, r2, r3]*180/pi;
            end
        end
    end
    
    handles = showPose(channels, skel);
else
    handles = drawPose(channels, skel, transparency);
end
xlabel('x \rightarrow');
ylabel('y \rightarrow');
zlabel('z \rightarrow');

% The following part is subject to change
% zlim([-1000 + channels(1, 3), 1000 + channels(1, 3)])
% xlim([-1000 + channels(1, 1), 1000 + channels(1, 1)])
% ylim([-1000 + channels(1, 2), 1000 + channels(1, 2)])
end

