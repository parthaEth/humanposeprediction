close all
subject = 6;
conf_file_id = fopen('../H36M.conf', 'r');
data_path = fscanf(conf_file_id,'%s');
if(data_path(end) ~= '/' && data_path(end) ~='\')
    data_path = [data_path, '/'];
end
if(isreal(subject) && subject > 0 && subject <= 11)
    subject = ['S',num2str(subject)];
end
data_path = [data_path, subject, '/MyPoseFeatures/D3_Angles'];

files = dir([data_path,'/*.cdf']);

col = 4;
for file_idx = 1:size(files, 1)
    angles = cdfread([data_path, '/', files(file_idx).name]);
    angles = angles{1};
    plot(angles(:, col), 'b')
    hold on
    plot(angles(:, col+1), 'r')
    plot(angles(:, col+2), 'g')
    break
end

figure
clear angles
angles = load(['../../Data/',subject,...
    '/MyPoseFeatures/D3_Angles/expMap/Directions 1.csv']);

col = 4;

plot(angles(:, col), 'b')
hold on
plot(angles(:, col+1), 'r')
plot(angles(:, col+2), 'g')
