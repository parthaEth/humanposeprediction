function updateDrawnPose(handles, channels, skel)
if ~strcmp(skel.type, 'MyOwn')
    error('Cant use thiskeleton');
end
connect = skelConnectionMatrix(skel);
vals = reshape(channels, 3, length(skel.tree))';
[I, J] = find(connect);
for i = 1:length(I)
    set(handles(i+1),...
        'XData', [vals(I(i), 1) vals(J(i), 1)], ...
        'YData', [vals(I(i), 2) vals(J(i), 2)], ...
        'ZData', [vals(I(i), 3) vals(J(i), 3)]);
end
end