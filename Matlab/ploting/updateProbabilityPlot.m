function updateProbabilityPlot( current_proba, axis_hdl, handles, class_index)
%UPDATEPROBABILITYPLOT updates the probability plot

persistent old_proba frame_number

if isempty(old_proba)
    old_proba = current_proba;
    frame_number = 2;
    return
end

axes(axis_hdl)

for i = 1:size(current_proba, 2)
    if i == class_index
        handles{i}{2}.XData = frame_number;
        handles{i}{2}.YData = current_proba(end, class_index);
        handles{i}{1}.XData = 1:frame_number;
        handles{i}{1}.YData = current_proba(:, i);
    else
        handles{i}.XData = 1:frame_number;
        handles{i}.YData = current_proba(:, i);
    end
end
old_proba = current_proba;
frame_number = frame_number + 1;

drawnow

end

