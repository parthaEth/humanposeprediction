function updateSkeletonPlot(handles, channels, skel, ...
                            incremental_yaw_absolute_roll_pitch,...
                            axis_handle)
%UPDATESKELETONPLOT given the current pose angles given in channels 
%variable and skeleton it updates the skeleton plot. handles are the lin
%handles
persistent z_ang_accum

if isempty(z_ang_accum)
    z_ang_accum = 0;
end

if ~strcmp(skel.type, 'MyOwn')
channels(4:6) = channels(4:6) * 180/pi;
    if ~isempty(skel.tree(1).rotInd) % Otherwise it is a position skeleton
        for i = 2:length(skel.tree)
            if(~isempty(skel.tree(i).rotInd))
                xangle = channels(skel.tree(i).rotInd(1));
                yangle = channels(skel.tree(i).rotInd(2));
                zangle = channels(skel.tree(i).rotInd(3));
                rotation_quat = expMap2Quat([xangle, yangle, zangle]);
                [z_ang, r2, r3] = quat2angle(rotation_quat, skel.tree(i).order);
                if incremental_yaw_absolute_roll_pitch && i == 1
                    z_ang_accum = z_ang_accum + z_ang;
                    r1 = z_ang_accum;
                else
                    r1 = z_ang;
                end
                starting_index = min(skel.tree(i).rotInd);
                channels(starting_index : starting_index+2) = ...
                    [r1, r2, r3]*180/pi;
            end
        end
    end
    updatePose(handles, channels, skel);
else
    updateDrawnPose(handles, channels, skel);
end
axis_handle.ZLim = [-1000 + channels(1, 3), 1000 + channels(1, 3)];
axis_handle.XLim = [-1000 + channels(1, 1), 1000 + channels(1, 1)];
axis_handle.YLim = [-1000 + channels(1, 2), 1000 + channels(1, 2)];
end

