clc
% clear all
close all
% This function animates groundtruth and predicted sequence side by side or
% on top of each other for visual comparison

addpath('ploting')
addpath('../providedCode/RNNexp/structural_rnn/CRFProblems/H3.6m/dataParser/Utils/')
addpath('../providedCode/RNNexp/structural_rnn/CRFProblems/H3.6m/mhmublv/Motion')

% -------------- Settings --------------%
fps = 10; % This is just display fps
pose_seq = '7';
express_error_in_normalized_scale = true;
write_to_video_file = true; %set to true if an avi is desired
my_own_data = false;
is_expmap_data = true;
skip_first_n_frames = 2;
person = 1; % Whose skeleton to use. Data has been normalized with s1
R0 = eye(3);
T0 = [0 0 0];
% -------------- Settings --------------%

file_name_with_path = ...
    ['../python_models/GeneratedSequences/', pose_seq, '.csv'];

[original_file_name, seq_start_row, ...
 seq_length, dwon_sampling_rate, FPS] = parseHeader(file_name_with_path);

% python index to matlab conversion 
seq_start_row = seq_start_row * dwon_sampling_rate + 1;

generated_data = dlmread(file_name_with_path, ',', 9);
[rows_gen_data, cols_gen_data] = size(generated_data);

original_data = load(original_file_name);
[rows_org_data, cols_org_data] = size(original_data);
[original_data_r, ~, ~]= revertCoordinateSpace(original_data, R0, T0);

comparable_length = min(rows_gen_data,...
    floor((rows_org_data - seq_start_row + 1)/dwon_sampling_rate));
time_vec = 0 : 1/FPS : comparable_length/FPS;

drawable_original = original_data_r(...
     seq_start_row : dwon_sampling_rate : dwon_sampling_rate ...
        * (comparable_length -1) + seq_start_row,:);

drawable_generated = generated_data(1:comparable_length, :);
drawable_generated = repelem(drawable_generated, dwon_sampling_rate, 1);
[drawable_generated, ~, ~]= revertCoordinateSpace(drawable_generated, R0, T0);
drawable_generated = drawable_generated(1:dwon_sampling_rate:end, :);


plot3(drawable_original(:,1), drawable_original(:,2),...
      drawable_original(:,3), 'g')
hold on

plot3(drawable_generated(:,1), drawable_generated(:,2),...
      drawable_generated(:,3), 'r')
xlabel('x \rightarrow')
ylabel('y \rightarrow')
zlabel('z \rightarrow')