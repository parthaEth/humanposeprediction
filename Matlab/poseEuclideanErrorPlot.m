clear all
% -------------- Settings --------------%
pose_seq = '11';
express_error_in_normalized_scale = false;
R0 = eye(3);
T0 = [0 0 0];
% -------------- Settings --------------%

file_name_with_path = ...
    ['../python_models/GeneratedSequences/', pose_seq, '.csv'];

[original_file_name, seq_star_row, ...
 seq_length, dwon_sampling_rate, FPS] = parseHeader(file_name_with_path);

disp(['Reporting erros for',original_file_name])

% python index to matlab conversion 
seq_star_row = (seq_star_row) * dwon_sampling_rate + 1;

generated_data = dlmread(file_name_with_path, ',', 9);
[rows_gen_data, cols_gen_data] = size(generated_data);
generated_data = repelem(generated_data, dwon_sampling_rate, 1);
[generated_data_r, ~, ~]= revertCoordinateSpace(generated_data, R0, T0);
generated_data = [generated_data(1:dwon_sampling_rate:end, 1:6),...
                  generated_data_r(1:dwon_sampling_rate:end, 7:end)];
% generated_data(:, 6:end) = generated_data_reconstructed(:, 6:end);

original_data = load(original_file_name);
[rows_org_data, cols_org_data] = size(original_data);
[original_data_r, ~, ~]= revertCoordinateSpace(original_data, R0, T0);
original_data(:, 7:end) = original_data_r(:, 7:end);

comparable_length = min(rows_gen_data,...
    floor((rows_org_data - seq_star_row + 1)/dwon_sampling_rate));
time_vec = 0 : 1/FPS : comparable_length/FPS;

if express_error_in_normalized_scale
%     In scale_and_shift first row is shift, second row is scale
    scale_and_shift = load('../python_models/shift_and_scale.text');
    
    column_wise_std_file = '../python_models/column_wise_std.text';
    fid = fopen(column_wise_std_file);
    header_data = textscan(fid, '%s %c %f %c', 1, 'delimiter',' ');
    std_threshold = header_data{1, 3}(1);
    per_col_std = dlmread(column_wise_std_file, ',', 2);
    remaining_cols = per_col_std > std_threshold;
    
    original_num_cols = length(per_col_std);
    scale_and_shift_original_dim = [zeros(1, original_num_cols);...
                                    ones(1, original_num_cols)];
    scale_and_shift_original_dim(:, remaining_cols) = scale_and_shift;
    
    original_data = original_data - ...
        repmat(scale_and_shift_original_dim(1, :), rows_org_data, 1);
    original_data = original_data ./ ...
        repmat(scale_and_shift_original_dim(2, :), rows_org_data, 1);
    
    generated_data = generated_data - ...
        repmat(scale_and_shift_original_dim(1, :), rows_gen_data, 1);
    generated_data = generated_data ./ ...
        repmat(scale_and_shift_original_dim(2, :), rows_gen_data, 1);
end

% original_data = original_data - ...
%         repmat(original_data(1, :), rows_org_data, 1);
% generated_data = generated_data - ...
%     repmat(generated_data(1, :), rows_gen_data, 1);

mse = ...
   sum((original_data(...
     seq_star_row : dwon_sampling_rate : dwon_sampling_rate ...
        * (comparable_length -1) + seq_star_row,7:end)...
       - generated_data(1:comparable_length, 7:end)).^2, 2);
   
% Units are mm if express_error_in_normalized_scale = true
rmse = sqrt(mse); 

rmse(seq_length+[2, 4, 8, 14, 25])

plot(time_vec(1:length(mse)), rmse)
folders_and_file = strsplit(original_file_name, '/');
title([folders_and_file{end}, 'seq ', pose_seq])
xlabel('time in second \rightarrow')
if express_error_in_normalized_scale
    ylabel('RMSE error normalized each dim ranges from 0-1 \rightarrow')
else
    ylabel('RMSE error in mm \rightarrow')
end
ax1 = gca;
ax1_pos = ax1.Position; % position of first axes
axis tight
grid on
ax2 = axes('Position',ax1_pos,...
    'XAxisLocation','top',...
    'YAxisLocation','right',...
    'XLim', [1, length(rmse)], ...
    'YLim', ax1.YLim, ...
    'Color','none');
xlabel('sequence index \rightarrow')
axes(ax1)