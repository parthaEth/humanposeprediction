load angles_to_smooth.mat

plot(angles_to_smooth, 'r')
hold on
smoothed_ones = angles_to_smooth;
for i = 2:length(angles_to_smooth)
    del = angles_to_smooth(i) - angles_to_smooth(i-1);
    if(abs(del) > 300)
        del = del - sign(del)*360;
    end
    smoothed_ones(i) = smoothed_ones(i -1) + del;
end
plot(smoothed_ones, 'b')