clc
% clear all
close all

global H36m_root;
H36m_root = [pwd, '/../'];

addpath('../ploting')

pose_seq = '1';
person = 1;

load('../skeletons.mat')
angles = load(['../../Data/S',num2str(person),...
                '/MyPoseFeatures/D3_Angles/expMap/Directions 1.csv']);

% angles(1, 4:6) = [0, pi/2, pi/2];
            
angles(1, 4:6) = [0, pi/2 + 0.15, pi/2-0.1];
handles = initSkeletonPlot(angles(1, :),...
   skeletons(person).angSkel, 1);
% del_ang = 1 * pi/180; 
% for i = 1:2000
% %     angles(1, 5) = angles(1, 5) - del_ang;
%     updateSkeletonPlot(handles, angles(1, :), ...
%         skeletons(person).angSkel, false);
%     pause(0.1)
%     angles_we_track = angles(1, 4:6)
% end