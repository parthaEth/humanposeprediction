function convertToExpMp( subject, out_format, out_path, tol )
%CONVERTTOEXPMP converts the pose data for given subject to exponencial map
%angles and stores them in a file in the optionally specified path
%subject = [1, 2, etc...] for S1, S2 etc.
%out_format = [.csv, .mat, .cdf, etc...]
%out_path = path where output file will be stored
%tol = if |V| is close to pi then v is replaced by (1-2*pi/|v|)v
%tol is measure of close-ness

load('../skeletons.mat')
skel = skeletons(subject).angSkel;

conf_file_id = fopen('../H36M.conf', 'r');
data_path = fscanf(conf_file_id,'%s');
if(data_path(end) ~= '/' && data_path(end) ~='\')
    data_path = [data_path, '/'];
end
if(isreal(subject) && subject > 0 && subject <= 11)
    subject = ['S',num2str(subject)];
end
data_path = [data_path, subject, '/MyPoseFeatures/D3_Angles'];

if(nargin < 2)
    out_format = '.csv';
    out_path = [data_path, '/expMap/'];
    tol = 0.3;
elseif(nargin < 3)
    out_path = [data_path, '/expMap/'];
    tol = 0.3;
elseif(nargin < 4)
    tol = 0.3;
end
if(exist(out_path, 'dir') == 0)
    present_working_dir = pwd;
    cd(data_path)
    mkdir('expMap')
    cd(present_working_dir)
end

files = dir([data_path,'/*.cdf']);

for file_idx = 1:size(files, 1)
    angles = cdfread([data_path, '/', files(file_idx).name]);
    angles = angles{1};

    prev_orientation = [angles(1, 4), angles(1, 5), angles(1, 6)] * pi /180;
    
    for row = 1:size(angles, 1)
        channels = angles(row, :);
        
%         xangle_root = (pi/180) * (channels(skel.tree(1).rotInd(1)));
%         yangle_root = (pi/180) * (channels(skel.tree(1).rotInd(2)));
%         zangle_prev = (pi/180) * (channels(skel.tree(1).rotInd(3)));
%         
%         zero_orientation_quat = ...
%             angle2quat(zangle_prev, xangle_root, yangle_root, 'ZXY');
        for i = 1:length(skel.tree)
            if(~isempty(skel.tree(i).rotInd))
                xangle = (pi/180) * (channels(skel.tree(i).rotInd(1)));
                yangle = (pi/180) * (channels(skel.tree(i).rotInd(2)));
                zangle = (pi/180) * (channels(skel.tree(i).rotInd(3)));
                
                if(i == 1)
                    
%                     rotation_quat = angle2quat(zangle - zangle_prev,...
%                                                xangle - xangle_root,...
%                                                yangle - yangle_root, 'ZXY');
%                     rotation_quat = ...
%                       quatmultiply(rotation_quat, ...
%                                    quatinv(zero_orientation_quat));
%                     zangle_prev = zangle;
                    del = [zangle, xangle, yangle] - prev_orientation;
                    
                    for j = 1 : 3
                        if(abs(del(j)) > 5.2360) % 300 degree
                            del(j) = del(j) - sign(del(j)) * 2 * pi;
                        end
                    end
                    
                    prev_orientation = [zangle, xangle, yangle];
%                     if(row == 1)
%                         angles(1, 4) = 0;
%                         angles(1, 4:6) = mod(angles(1, 4:6), 360) * pi/180;
%                     else
%                         angles(row, 4:6) = angles(row - 1, 4:6) + del;
%                     end
                    angles(row, 1:6) = [0, 0, 0, 0, 1.4, 1.4];
                    continue
                else
                    rotation_quat = angle2quat(zangle, xangle,...
                                               yangle, 'ZXY');
                end

                exp_map = quat2expMap(rotation_quat);
                norm_exp_map = norm(exp_map);
                if abs(norm_exp_map - pi) < tol
                    exp_map = (1-2*pi/norm_exp_map)*exp_map;
                end
                angles(row, skel.tree(i).rotInd(1)) = exp_map(1);
                angles(row, skel.tree(i).rotInd(2)) = exp_map(2);
                angles(row, skel.tree(i).rotInd(3)) = exp_map(3);
            end
        end
    end
    csvwrite([out_path, files(file_idx).name(1:end-4), out_format],...
             angles)
    files(file_idx).name
end
end

