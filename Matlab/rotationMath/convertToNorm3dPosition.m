function convertToNorm3dPosition( subject, out_format, out_path )
%CONVERTTONORM3DPOSITION converts the pose data for given subject to
%normalized 3D joint positions and stores them in a file in the optionally
%specified path
%   subject = [1, 2, etc...] for S1, S2 etc.
%   out_format = [.csv, .mat, .cdf, etc...]
%   out_path = path where output file will be stored

load('../skeletons.mat')
skel = skeletons(subject).angSkel;

conf_file_id = fopen('../H36M.conf', 'r');
data_path = fscanf(conf_file_id,'%s');
if(data_path(end) ~= '/' && data_path(end) ~='\')
    data_path = [data_path, '/'];
end
if(isreal(subject) && subject > 0 && subject <= 11)
    subject = ['S',num2str(subject)];
end
data_path = [data_path, subject, '/MyPoseFeatures/D3_Angles'];

if(nargin < 2)
    out_format = '.csv';
    out_path = [data_path(1:end-10), '/norm3Dpos/'];
elseif(nargin < 3)
    out_path = [data_path(1:end-10), '/norm3Dpos/'];
end

if(exist(out_path, 'dir') == 0)
    present_working_dir = pwd;
    cd(data_path)
    cd('..')
    mkdir('norm3Dpos')
    cd(present_working_dir)
end

files = dir([data_path,'/*.cdf']);

for file_idx = 1:size(files, 1)
    angles = cdfread([data_path, '/', files(file_idx).name]);
    angles = angles{1};
    
    time_steps = size(angles, 1);
    cols = 3 * length(skel.tree);
    joint_3d_pos = zeros(time_steps, cols);
    
    % Represent everything is first body frame %
    xangle = (pi/180) * (angles(1, skel.tree(1).rotInd(1)));
    yangle = (pi/180) * (angles(1, skel.tree(1).rotInd(2)));
    zangle = (pi/180) * (angles(1, skel.tree(1).rotInd(3)));
    % R_G_B = unit vectors of B in G - global frame. Left multiplication
    % converts vectors in B to vectors in G
    R_B_G = rotationMatrix(0, 0, zangle, skel.tree(1).order);

    % translation of B in G
    t_G_B = [angles(1, skel.tree(1).posInd(1)); ...
             angles(1, skel.tree(1).posInd(2)); ...
             angles(1, skel.tree(1).posInd(3))];
    %------------------------------------------%
    
    for row = 1:size(angles, 1)
        %  t_G_j = translation of joint in G expressed in G
        t_G_j = skel2xyz(skel, angles(row, :));
        
        %  t_B_j = translation of joint in B expressed in B
        t_B_j = R_B_G * (t_G_j' - repmat(t_G_B, 1, length(skel.tree)));
        joint_3d_pos(row, :) = reshape(t_B_j, 1, cols);
    end
    
    csvwrite([out_path, files(file_idx).name(1:end-4), out_format],...
        joint_3d_pos)
    files(file_idx).name
end

end