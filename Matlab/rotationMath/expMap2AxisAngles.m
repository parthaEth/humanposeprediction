function [ axis_angles ] = expMap2AxisAngles( exp_map, ...
                                              input_order, ... 
                                              output_order)
%EXPMAP2AXISANGLES converts exponential maps to axis angles
%   Converts exponential maps of given order into axis angles of given
%   order

if nargin < 2
    input_order = 'xyz';
    output_order = 'xyz';
else
    if ~strcmap(lower(input_order), 'xyz')
        error('Other orders have not been implementd yet')
    end
end

q = expMap2Quat(exp_map);
axis_angles = zeros(1, 3);
[axis_angles(1), axis_angles(2), axis_angles(3)] = ...
    quat2angle(q, output_order);
end

