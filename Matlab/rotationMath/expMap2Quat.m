function [ quat ] = expMap2Quat( exp_map )
%EXPMAP2QUAT converts an exponencial map to a quaternon of the kind
% q = [W, x, y, z]. look at 
% https://www.cs.cmu.edu/~spiff/moedit99/expmap.pdf for def. of expMaps
% the expMap values are considered to be in radians

[row, col] = size(exp_map);
if(col ~= 3)
    error(['Provided array of exponential map must be of dimention nX3' ...
    'representing n exp maps. Got [', num2str(row), 'X'...
    num2str(col), ']']);
end

theta = sqrt(sum(exp_map.^2, 2));
quat = zeros(row, 4);
for r = 1:row
    if(theta(r) < 1e-2) % for numerical stability
        quat(r, :) = [cos(theta(r)/2), ...
                      (0.5 + (theta(r)^2)/48).*exp_map(r, :)];
    else
        quat(r, :) = [cos(theta(r)/2), ...
                      (sin(theta(r)/2)./theta(r)).*exp_map(r, :)];
    end
end

end

