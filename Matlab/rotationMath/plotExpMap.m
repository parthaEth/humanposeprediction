clc
clear all
close all

base_dir = '../../Data/S5/MyPoseFeatures/D3_Angles/expMap';
files = dir(base_dir);

for i=3:100
    angles = load([base_dir,'/', files(i).name]);
    R = quat2rotm(expMap2Quat(angles(1, 4:6)));
    quiver3(0, 0, 0, R(1,1), R(2,1), R(3, 1), 'r')
    hold on
    quiver3(0, 0, 0, R(1,2), R(2,2), R(3, 2), 'g')
    quiver3(0, 0, 0, R(1,3), R(2,3), R(3, 3), 'b')
end