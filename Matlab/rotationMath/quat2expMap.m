function [ exp_map ] = quat2expMap( quat )
%QUAT2EXPMAP converts an quaternon map to a exponencial. 
% The quaternion must be of the kind q = [W, x, y, z]. look at 
% https://www.cs.cmu.edu/~spiff/moedit99/expmap.pdf for def. of expMaps
% the expMap values will be in radians
[row, col] = size(quat);
if(col ~= 4)
    error(['Provided array of quaternions map must be of dimention nX4' ...
    'representing n quaternions. Got [', num2str(row), 'X'...
    num2str(col), ']']);
end

half_theta = acos(quat(:, 1));
exp_map = quat(:, 2:4);
for r = 1:row
    if(half_theta(r) < 1e-2) % for numerical stability
        vec_norms_correction_factor = ...
            1/(0.5 + ((2*half_theta(r))^2)/48); % theta/sin(theta/2)
    else
        vec_norms_correction_factor = ...
            2*half_theta(r)./sqrt(sum(exp_map(r, :).^2, 2));
    end
    exp_map(r, :) = exp_map(r, :).*vec_norms_correction_factor;
end
end

