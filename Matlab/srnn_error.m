% SRNN euclidean error
pose_seq = '11'; %  Animates this file number

generated_data = load(['/home/partha/ait-ra-ship/srnnResults/h3.6m/',...
        'checkpoints_dra_T_150_bs_100_tg_10_ls_10_fc_10_demo/', ...
        'forecast_iteration_14750_N_', pose_seq]);
    
original_data = load(['/home/partha/ait-ra-ship/srnnResults/h3.6m/',...
        'checkpoints_dra_T_150_bs_100_tg_10_ls_10_fc_10_demo/', ...
        'ground_truth_forecast_N_', pose_seq]);
    
error = original_data(1:100, 6:end) - generated_data(1:100, 6:end);
rmse = sqrt(sum(error.^2, 2));

rmse([2, 4, 8, 14, 25])

plot(rmse)