clc
close all
%Add utility paths
addpath('../providedCode/Release-v1.1/H36M');
addpath('../providedCode/Release-v1.1/H36MBL');
addpath('../providedCode/Release-v1.1/H36MGUI');
addpath(genpath('../providedCode/Release-v1.1/utils'));
addpath(genpath('../providedCode/Release-v1.1/external_utils'));

fno = 800;

% Read CDF formated raw positions
positions = cdfread('../Data/S1/MyPoseFeatures/D3_Positions/Directions.cdf');
Sequence = H36MSequence(1,13,2,1,fno);
Subject = Sequence.getSubject();
posSkel   = Subject.getPosSkel();
connect = skelConnectionMatrix(posSkel);
indices = find(connect);
[I, J] = ind2sub(size(connect), indices);
% handle(1) = plot(vals(:, 1), vals(:, 2), '.');
%axis ij % make sure the left is on the left.
% set(handle(1), 'markersize', 10);
figure
vals = reshape(positions{1}(fno, :),3, 32)';
% vals = bvh2xyz(posSkel, positions{1}(fno, :), 1);
plot3(vals(:, 1), vals(:, 2), vals(:, 3), 'o')
axis equal
hold on
grid on
for i = 1:length(indices)
  % modify with show part (3d geometrical thing)
  if strncmp(posSkel.tree(I(i)).name,'L',1)
    c = 'r';
  elseif strncmp(posSkel.tree(I(i)).name,'R',1)
    c = 'g';
  else 
    c = 'b';
  end
  
  text(double(vals(I(i), 1)), double(vals(I(i), 2)), double(vals(I(i), 3)), ...
       posSkel.tree(I(i)).name);
  
  % FIXME 
  handle(i+1) = line([vals(I(i), 1) vals(J(i), 1)], ...
                     [vals(I(i), 2) vals(J(i), 2)],...
                     [vals(I(i), 3) vals(J(i), 3)],'Color',c);
  set(handle(i+1), 'linewidth', 3);

end


angles = cdfread('../Data/S1/MyPoseFeatures/D3_Angles/Directions.cdf');
% posSkel   = Subject.getPosSkel();
angSkel   = Subject.getAnglesSkel();
vals = bvh2xyz(angSkel, angles{1}(fno, :));
figure
plot3(vals(:, 1), vals(:, 2), vals(:, 3), 'o')
axis equal

% Color the stuff properly
connect = skelConnectionMatrix(angSkel);
indices = find(connect);
[I, J] = ind2sub(size(connect), indices);
% handle(1) = plot(vals(:, 1), vals(:, 2), '.');
%axis ij % make sure the left is on the left.
% set(handle(1), 'markersize', 10);
hold on
grid on
for i = 1:length(indices)
  % modify with show part (3d geometrical thing)
  if strncmp(angSkel.tree(I(i)).name,'L',1)
    c = 'r';
  elseif strncmp(angSkel.tree(I(i)).name,'R',1)
    c = 'g';
  else 
    c = 'b';
  end
  
  angSkel.tree(I(i)).name
  
  % FIXME 
  handle(i+1) = line([vals(I(i), 1) vals(J(i), 1)], ...
                     [vals(I(i), 2) vals(J(i), 2)],...
                     [vals(I(i), 3) vals(J(i), 3)],'Color',c);
  set(handle(i+1), 'linewidth', 3);

end