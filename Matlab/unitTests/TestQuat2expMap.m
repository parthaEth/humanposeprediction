function passed = TestQuat2expMap(quiet_mode)
% This script tests the quat to expmap and inverse function
if(nargin < 1)
    quiet_mode = false;
end
passed = true;

addpath('../quaternionMath');

% special cases test expMap2Quat
exp_map = [pi/2, 0,    0;
           0,    pi/2, 0;
           0,    0     pi/2];

expected_quat = [0.7071    0.7071         0         0;
                 0.7071         0    0.7071         0;
                 0.7071         0         0    0.7071];
if(max(max(abs(expMap2Quat(exp_map) - expected_quat))) > 1e-4)
    if(~quiet_mode)
        expMap2Quat(exp_map)
        expected_quat
        error('Test expMap2Quat failed: with above vals');
    else
        passed = false;
    end
else
    if(~quiet_mode)
        disp('-----Passed expMap2Quat-----')
    end
end

% special cases test quat2expMap
quat = [0.7071    0.7071         0         0;
        0.7071         0    0.7071         0;
        0.7071         0         0    0.7071;
        1              0         0         0;
        0.9999    0.0079    0.0003    0.0080];

expected_exp_map = [pi/2,  0,     0;
                    0,     pi/2,  0;
                    0,     0      pi/2;
                    0,     0      0;
                    0.0158 0.0006 0.0160];
if(max(max(abs(expected_exp_map - quat2expMap(quat)))) > 10-6)
    if(~quiet_mode)
        quat2expMap(quat)
        expected_exp_map
        error('Test quat2expMap failed: with above vals');
    else
        passed = false;
    end
else
    if(~quiet_mode)
        disp('-----Passed quat2expMap-----')
    end
end

%randomized test
random_exp_map = rand(1000, 3);

random_quats = expMap2Quat(random_exp_map);
retrived_exp_maps = quat2expMap(random_quats);
if(max(max(abs(random_exp_map - retrived_exp_maps))) > 1e-8)
    if(~quiet_mode)
        random_exp_map
        retrived_exp_maps
        max(max(abs(random_exp_map - retrived_exp_maps)))
        error('Test failed: with above vals');
    else
        passed = false;
    end
else
    if(~quiet_mode)
        disp('-----Passed-----')
    end
end
end