% Runns all the tests placed in the current folder
% All the est function should take exactly one parameter and return exactly
% one value indicating if the test passed of failed.
% The passed argument requests for quiet mode i.e. a test failure doesn't
% stop program execution
files = dir('*.m');
quiet = true;
total_count = 0;
failed_count = 0;
for i = 1:size(files, 1)
    if(~strcmp(files(i).name, 'runAllTests.m') ...
        && files(i).name(end) ~= '~')
        total_count = total_count + 1;
        function_name = files(i).name(1:end-2);
        success = function_name(quiet);
        if(~success)
            failed_count = failed_count + 1;
            disp([function_name, ' failed !']);
        end
    end
end
disp([num2str(failed_count), ' out of ' , num2str(total_count),...
      ' tests failed']);
if(failed_count == 0)
    disp('------All the tests passed------')
else
    disp('------Some tests failed review them------')
end