function batchSizeAndValidationSplitGen(data_size, max_validation_split)
format long
% valid batch size and validation split generator
max_validation_samples = round(data_size * max_validation_split);

for i = max_validation_samples:-1:1
    training_data_size = data_size - i;
    
    factors_training_data = factor(training_data_size);
    factors_validation_data = factor(i);
    
    length_diff = ...
        length(factors_validation_data) - length(factors_training_data);
    if length_diff > 0
        factors_training_data = ...
            [factors_training_data, ones(1, length_diff)];
    elseif length_diff < 0
        factors_validation_data = ...
            [factors_validation_data, ones(1, -length_diff)];
    end
    
    matches = factors_training_data == factors_validation_data;
    if any(matches)
        disp 'valid splits are'
        validation_split = single(i/data_size);
        if validation_split * single(data_size) - i > 0
            validation_split = ...
                (validation_split + single((i - 1)/data_size))/2
        else
            validation_split
        end
        batchsizes = factors_training_data(matches)
        pause
    end
end
disp 'found none!'

end