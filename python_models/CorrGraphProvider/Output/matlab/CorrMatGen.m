% Generate correlation measure
rng(17)
addpath('../../../../Matlab/ploting')
addpath('../../../../providedCode/RNNexp/structural_rnn/CRFProblems/H3.6m/dataParser/Utils/')
addpath('../../../../providedCode/RNNexp/structural_rnn/CRFProblems/H3.6m/mhmublv/Motion/')

%% ------------------------------settings------------------------------ %%
write_to_video_file = true;
% Correlation measure to use
correlation_type =  'Dual total correlation';%'Pearson'; %'Kendall'; %'Spearman'; % %

% Evaluate on groundtruth or on generated data
gt_eval = true;

% Path to the file that generates this residuals
generated_seq_path = '../../../GeneratedSequences/';

% Path to groundtruth files
gt_file_path = '../../../../Data/S5/MyPoseFeatures/D3_Angles/expMap/';

% This is the width of the running window average estimator for calculating
% the residue average. Whic is then used to compute the dependency graph
window_width = 50;

% Frames per second is used to convert timesteps to indices
fps = 25;

% Offset_frames is supposed to tbe the seed sequence length.Then the time
% steps_ms signifies the amount of time in future for which the prediction
% is evaluated
offset_frames = floor(window_width/2) + 1;

% Chose the activity/activities for which the evaluation is performed
% activitie = 'directions_1'; % 'greeting_2', 'walking_1'
activity = '17';
% activity = 'walking_1';

% Mark down of indices
spine_idx = {3, [20, 21, 22], [23, 24, 25], [26, 27, 28], [29, 30, 31]};
l_arm_idx = {[32, 33, 34], [35, 36, 37], 38, [39, 40, 41]};
r_arm_idx = {[42, 43, 44], [45, 46, 47], 48, [49, 50, 51]};
l_leg_idx = {[12, 13, 14], 15, [16, 17, 18], 19};
r_leg_idx = {[4,  5,  6], 7,  [8,  9, 10], 11};
%% --------------------Compute the correlation Matrix------------------- %%
part_pairs = {'l_leg_r_leg', 'r_leg_spine', 'r_leg_l_hand', 'r_leg_r_hand', ...
              'l_leg_spine', 'l_leg_l_hand', 'l_leg_r_hand', ...
              'spine_l_hand', 'r_hand_spine', ...
               'r_hand_l_hand'};
index_tuples = {{l_leg_idx, r_leg_idx}, {r_leg_idx, spine_idx}, {l_arm_idx, r_leg_idx}, {r_arm_idx, r_leg_idx},...
                {l_leg_idx, spine_idx}, {l_arm_idx, l_leg_idx}, {r_arm_idx, l_leg_idx}, ...
                {l_arm_idx, spine_idx}, {r_arm_idx, spine_idx}, ...
                {r_arm_idx, l_arm_idx}};
part_pair_idx_map = containers.Map(part_pairs, index_tuples);

subplot(1,2,2)
ax = gca();
dep_graph_title_h = title(num2str(offset_frames - floor(window_width/2)));

% Skeleton plot
subplot(1, 2, 1)
skel_ax = gca();
skel_title_h = title(num2str(offset_frames));
load('../../../../Matlab/skeletons.mat')
skeleton = skeletons(1).angSkel;
c = zeros(1,78);
[ skel_expmap,~ ] = getExpmapFromSkeleton( skeleton, c );
    
if isnan(str2double(activity))
    skeleton_data_exp = load([gt_file_path, activity, '.txt']);
%     skeleton_data_exp = rand(3000, 99);
else
    skeleton_data_exp = dlmread([generated_seq_path, activity, '.csv'], ',', 9);
end
skeleton_data = getPosFromExpmap(skel_expmap, skeleton_data_exp);
[rows, cols] = size(skeleton_data);
skeleton_data = reshape(skeleton_data', 1, []);

skeleton = skeletons(1).posSkel;
cols_actual = length(skeleton.tree) * 3;
rows_original = rows*cols / cols_actual;
skeleton_data = reshape(skeleton_data, cols_actual, rows_original)';
skeleton.type = 'MyOwn';
handles_skeleton = initSkeletonPlot(skeleton_data(offset_frames, :), skeleton, skel_ax);

part_corr_mat = 9999999 * eye(5);
frame_idx_prev = inf;

% Remove the columns with little variance
std_file_name = '../../../column_wise_std.text';
fid = fopen(std_file_name);
std_threshold = textscan(fid, '%s %c %f', 1, 'delimiter', ' ');
std_threshold = std_threshold{end};
fclose(fid);
stds = dlmread(std_file_name, ',', 2);
skeleton_data_exp(:, stds < std_threshold) = [];
skeleton_data_exp = skeleton_data_exp - mean(skeleton_data_exp);
skeleton_data_exp = skeleton_data_exp ./ ...
    repmat(max(skeleton_data_exp) - min(skeleton_data_exp), size(skeleton_data_exp, 1), 1);
% skeleton_data_exp = skeleton_data_exp(2:end, :) - skeleton_data_exp(1:end-1, :);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% Only for testing %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% skeleton_data_exp(:, [l_arm_idx{:}] + 1) = skeleton_data_exp(:, [spine_idx{:}]+1) * ...
%                                         rand(length([spine_idx{:}]), length([l_arm_idx{:}]));
% skeleton_data_exp(:, [r_arm_idx{:}] + 1) = skeleton_data_exp(:, [spine_idx{:}]+1) * ...
%                                     rand(length([spine_idx{:}]), length([r_arm_idx{:}]));
% 
% skeleton_data_exp(:, [l_leg_idx{:}] + 1) = skeleton_data_exp(:, [l_arm_idx{:}] + 1) * ...
%                                     rand(length([l_arm_idx{:}]), length([l_leg_idx{:}]));
% skeleton_data_exp(:, [r_leg_idx{:}] + 1) = skeleton_data_exp(:, [r_arm_idx{:}] + 1) * ...
%                                         rand(length([r_arm_idx{:}]), length([r_leg_idx{:}]));
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
                                    


suptitle(['Window\_width = ', num2str(window_width), '; FPS = ', num2str(fps), '; Correlation = ', ...
          correlation_type]);

if(write_to_video_file)
%     set(gcf, 'Position', get(0,'Screensize')-[0, -499, 500, 500]);
    v = VideoWriter(['DepGraphOriginalSeq1_', activity, '.avi']);
    v.FrameRate = 16;
    open(v);
end

for frame_idx_raw = offset_frames:2000
    frame_idx = offset_frames + floor(2 * (frame_idx_raw - offset_frames) / window_width)...
                - floor(window_width/2);
    if frame_idx ~= frame_idx_prev
        part_count = 1;
        for row = 1:4
            for col = 1+row : 5
                parts = part_pair_idx_map(part_pairs{part_count}); 
                half_window = floor(window_width/2);
%                 rho = corr(skeleton_data_exp(frame_idx_raw-half_window:frame_idx_raw+half_window, ...
%                                              part_s{1}),...
%                            skeleton_data_exp(frame_idx_raw-half_window:frame_idx_raw+half_window,...
%                                              part_s{2}), 'type',...
%                            correlation_type);
                total_mi = 0;
                part_pair_count = 0;
                for i = parts{1}
                    for j = parts{2}
                        rows_under_window = frame_idx_raw-half_window:frame_idx_raw+half_window;
                        mi_curr = kernelizedMutualInfo(skeleton_data_exp(rows_under_window, i{1}+1), ...
                                                       skeleton_data_exp(rows_under_window, j{1}+1));
                        total_mi = total_mi + mi_curr;
                        part_pair_count = part_pair_count + 1;
                    end
                end
                part_corr_mat(row, col) = total_mi;
                part_corr_mat(col, row) = part_corr_mat(row, col);
                part_count = part_count + 1;
            end
        end
        frame_idx_prev = frame_idx;
        delete(allchild(ax))
        humanStructureGivenRes(-part_corr_mat, 2, ax); % residual as -ve correlation
        dep_graph_title_h.String = num2str(frame_idx_raw);
%         pause;
    end
    updateSkeletonPlot(handles_skeleton, skeleton_data(frame_idx_raw, :), skeleton, true, skel_ax);
    skel_title_h.String = num2str(frame_idx_raw);
    if(write_to_video_file)
        F = getframe(gcf);
        writeVideo(v, F);
    end
    pause(1/fps);
end
if(write_to_video_file)
    close(v);
end






















