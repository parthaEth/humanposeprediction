% Generate correlation measure
rng(17)
addpath('../../../../Matlab/ploting')
addpath('../../../../providedCode/RNNexp/structural_rnn/CRFProblems/H3.6m/dataParser/Utils/')
addpath('../../../../providedCode/RNNexp/structural_rnn/CRFProblems/H3.6m/mhmublv/Motion/')

%% ------------------------------settings------------------------------ %%
write_to_video_file = true;
% Correlation measure to use
correlation_type =  'MutualInfo.'; %'Dual total correlation';%'Pearson'; %'Kendall'; %'Spearman'; % %

% Evaluate on groundtruth or on generated data
gt_eval = true;

% Path to the file that generates this residuals
generated_seq_path = '../../../GeneratedSequences/';

% Path to groundtruth files
gt_file_path = '../../../../Data/S5/MyPoseFeatures/D3_Angles/expMap/';

% This is the width of the running window average estimator for calculating
% the residue average. Whic is then used to compute the dependency graph
window_width = 498;

% Chose the activity/activities for which the evaluation is performed
% activitie = 'directions_1'; % 'greeting_2', 'walking_1'
% activity = '17';
activitie_names = {'walking_1', 'eating_1', 'smoking_1', 'discussion_1'};
activities = {'walking_1', 'eating_1', 'smoking_1', 'discussion_1'};
% activities = {'17', '11', '13', '5'};

% Corr graph corresponding to these time stamps will be around which the
% dependency graphs will be calculated. The unit is second.
% time_steps_to_consider = [2, 4, 6, 8];
time_steps_to_consider = 10;

% Frame rate of the data so time steps can be converted to frame index
fps = 25;

% Mark down of indices
spine_idx = {3, [20, 21, 22], [23, 24, 25], [26, 27, 28], [29, 30, 31]};
l_arm_idx = {[32, 33, 34], [35, 36, 37], 38, [39, 40, 41]};
r_arm_idx = {[42, 43, 44], [45, 46, 47], 48, [49, 50, 51]};
l_leg_idx = {[12, 13, 14], 15, [16, 17, 18], 19};
r_leg_idx = {[4,  5,  6], 7,  [8,  9, 10], 11};

% Evaluation is being done for which netwrk
% network_name = 'LSTM3LR';
% network_name = 'AutoEncoderLSTM';
% network_name = 'ERD';
%% --------------------Compute the correlation Matrix------------------- %%
part_pairs = {'l_leg_r_leg', 'r_leg_spine', 'r_leg_l_hand', 'r_leg_r_hand', ...
    'l_leg_spine', 'l_leg_l_hand', 'l_leg_r_hand', ...
    'spine_l_hand', 'r_hand_spine', ...
    'r_hand_l_hand'};
index_tuples = {{l_leg_idx, r_leg_idx}, {r_leg_idx, spine_idx}, {l_arm_idx, r_leg_idx}, {r_arm_idx, r_leg_idx},...
    {l_leg_idx, spine_idx}, {l_arm_idx, l_leg_idx}, {r_arm_idx, l_leg_idx}, ...
    {l_arm_idx, spine_idx}, {r_arm_idx, spine_idx}, ...
    {r_arm_idx, l_arm_idx}};
part_pair_idx_map = containers.Map(part_pairs, index_tuples);

for activity_idx = 1:4
    activity = activities{activity_idx};
    
    if isnan(str2double(activity))
        skeleton_data_exp = load([gt_file_path, activity, '.txt']);
        offset_frames = 0;
        network_name = 'Groundtruth';
        %     skeleton_data_exp = rand(3000, 99);
    else
        skeleton_data_exp = dlmread([generated_seq_path, activity, '.csv'], ',', 9);
        offset_frames = 50;
    end
    
    % Remove the columns with little variance
    std_file_name = '../../../column_wise_std.text';
    fid = fopen(std_file_name);
    std_threshold = textscan(fid, '%s %c %f', 1, 'delimiter', ' ');
    std_threshold = std_threshold{end};
    fclose(fid);
    stds = dlmread(std_file_name, ',', 2);
    skeleton_data_exp(:, stds < std_threshold) = [];
    skeleton_data_exp = skeleton_data_exp - mean(skeleton_data_exp);
    skeleton_data_exp = skeleton_data_exp ./ ...
        repmat(max(skeleton_data_exp) - min(skeleton_data_exp), size(skeleton_data_exp, 1), 1);    
    
    for frame_idx_raw = time_steps_to_consider * fps
        frame_idx = offset_frames + floor(2 * (frame_idx_raw - offset_frames) / window_width)...
            - floor(window_width/2);
        part_count = 1;
        part_corr_mat = 1e3*eye(5);
        for row = 1:4
            for col = 1+row : 5
                parts = part_pair_idx_map(part_pairs{part_count});
                half_window = floor(window_width/2);
                %                 rho = corr(skeleton_data_exp(frame_idx_raw-half_window:frame_idx_raw+half_window, ...
                %                                              part_s{1}),...
                %                            skeleton_data_exp(frame_idx_raw-half_window:frame_idx_raw+half_window,...
                %                                              part_s{2}), 'type',...
                %                            correlation_type);
                total_mi = 0;
                part_pair_count = 0;
                for i = parts{1}
                    for j = parts{2}
                        rows_under_window = frame_idx_raw-half_window:frame_idx_raw+half_window;
                        mi_curr = kernelizedMutualInfo(skeleton_data_exp(rows_under_window, i{1}+1), ...
                            skeleton_data_exp(rows_under_window, j{1}+1));
                        total_mi = total_mi + mi_curr;
                        part_pair_count = part_pair_count + 1;
                    end
                end
                part_corr_mat(row, col) = total_mi;
                part_corr_mat(col, row) = part_corr_mat(row, col);
                part_count = part_count + 1;
            end
        end
        humanStructureGivenRes(-part_corr_mat, 3);
        if exist('dependency_matrices.mat', 'file') == 2
            load('dependency_matrices.mat')
        else
            dependency_matrices = containers.Map();
        end
        
        current_key = [network_name, '_', activitie_names{activity_idx}, '_',num2str(frame_idx_raw)];
%         if isKey(dependency_matrices, current_key)
%             x = input(...
%                 'Results for this network is already in record over write(y/n)?');
%             if x~= 'y' && x~= 'Y'
%                 clear all
%                 return
%             end
%         end
%         
        dependency_matrices(current_key) = part_corr_mat;
        
        save('dependency_matrices.mat', 'dependency_matrices')
    end
end