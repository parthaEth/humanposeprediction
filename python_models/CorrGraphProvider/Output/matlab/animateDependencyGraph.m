% animate skeleton alongside dependency graph
% This script generates the Dependency graph from the  residual scores
clc
clear all

addpath('../../../../Matlab/ploting')
addpath('../../../../providedCode/RNNexp/structural_rnn/CRFProblems/H3.6m/dataParser/Utils/')
addpath('../../../../providedCode/RNNexp/structural_rnn/CRFProblems/H3.6m/mhmublv/Motion/')

%% ------------------------------settings------------------------------ %%
% Path to the directoryin which 
% residue_dir = '../currentNetwork/';
residue_dir = '../groundtruthS5data/';

% Path to the file that generates this residuals
generated_seq_path = '../../../GeneratedSequences/';

% Path to groundtruth files
gt_file_path = '../../../../Data/S5/MyPoseFeatures/D3_Angles/expMap/';

% This is the width of the running window average estimator for calculating
% the residue average. Whic is then used to compute the dependency graph
window_width = 1;

% Frames per second is used to convert timesteps to indices
fps = 25;

% Offset_frames is supposed to tbe the seed sequence length.Then the time
% steps_ms signifies the amount of time in future for which the prediction
% is evaluated
offset_frames = 1;

% Chose the activity/activities for which the evaluation is performed
% activitie = 'directions_1'; % 'greeting_2', 'walking_1'
% activity = '17';
activity = 'walking_1';
%% --------------------Compute the correlation Matrix------------------- %%
part_pairs = ...
    {'l_leg_r_leg', 'l_leg_r_hand', 'l_leg_spine', 'l_leg_l_hand', 'r_leg_r_hand', 'r_leg_spine', ...
     'r_leg_l_hand', 'r_hand_spine', 'r_hand_l_hand', 'spine_l_hand'};

% Skeleton plot
subplot(1, 2, 1)
skel_ax = gca();
load('../../../../Matlab/skeletons.mat')
skeleton = skeletons(1).angSkel;
c = zeros(1,78);
[ skel_expmap,~ ] = getExpmapFromSkeleton( skeleton, c );
    
if isnan(str2double(activity))
    skeleton_data = load([gt_file_path, activity, '.txt']);
else
    skeleton_data = dlmread([generated_seq_path, activity, '.csv'], ',', 9);
end
skeleton_data = getPosFromExpmap(skel_expmap, skeleton_data);
[rows, cols] = size(skeleton_data);
skeleton_data = reshape(skeleton_data', 1, []);

skeleton = skeletons(1).posSkel;
cols_actual = length(skeleton.tree) * 3;
rows_original = rows*cols / cols_actual;
skeleton_data = reshape(skeleton_data, cols_actual, rows_original)';
skeleton.type = 'MyOwn';
handles_skeleton = initSkeletonPlot(skeleton_data(offset_frames, :), skeleton, skel_ax);

% Dependency graph plot
subplot(1, 2, 2)
ax = gca();
column = 1;
for part_pair = part_pairs
    residuals = load([residue_dir, activity, '/', part_pair{1}, '.csv']);
    avg_perjoint_residual(:, column) = mean(residuals, 2);
    column = column + 1;
end

% Updating the plots
for f_idx = offset_frames + 1: offset_frames + 300
    
    updateSkeletonPlot(handles_skeleton, skeleton_data(f_idx, :), skeleton, true, skel_ax);
    
    start_row = round(f_idx - (window_width -1)/2);
    end_row = round(f_idx + (window_width -1)/2);
    current_corravg_perjoint_residual = mean(avg_perjoint_residual(start_row:end_row, :), 1);

    computeDependencyGraph(current_corravg_perjoint_residual, residue_dir, activity, ax);
    pause(1/fps);
    delete(allchild(ax))
end