function [frame] = computeDependencyGraph(current_corravg_perjoint_residual, residue_dir, activity, ...
                                          axis_h, write_dep_graph_picture)
%COMPUTEDEPENDENCYGRAPH Computes and draws the dependency graph of human
%skeleton and optionaly saves it's images.
%   

    if nargin < 5
        write_dep_graph_picture = false;
    end

    res_mat = zeros(5, 5);
    count = 1;
    for row = 1:4
        for col = 1+row : 5
            res_mat(row, col) = current_corravg_perjoint_residual(count);
            res_mat(col, row) = res_mat(row, col);
            count = count + 1;
        end
    end
    humanStructureGivenRes(res_mat, 2, axis_h)
    frame = getframe;
    if write_dep_graph_picture
        imwrite(frame.cdata, [residue_dir, activity{1}, num2str(f_idx), '.jpg'])
        close all
    end
end

