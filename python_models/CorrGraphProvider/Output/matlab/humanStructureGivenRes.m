function humanStructureGivenRes( residual, num_edges_per_node, axis_h )
%HUMANSTRUCTUREGIVENCORR Draws the st-graph given pearson_correlation
%matrix of limbs

if nargin < 2
    figure
    num_edges_per_node = 3;
elseif nargin < 3
    figure
else
    axes(axis_h)
end

raddi = 0.25;
parts = {'RightLeg', 'LeftLeg', 'Spine', 'LeftArm', 'RightArm'};
centres = [1.5, -4;
          -1.5, -4;
             0,  0;
            -2,  -1;
             2,  -1];
el_centres = [1.5, -4 - raddi;
             -1.5, -4 - raddi;
                0,  0 + raddi;
               -2 - raddi,  -1;
                2 + raddi,  -1];
phi = [0, 0, 0, pi/2, pi/2];

viscircles(centres(3, :), raddi, 'LineWidth', 3, 'Color', 'r');
pos = centres(3, :) + [-0.5, 0.8];
text(pos(1), pos(2), parts{3});

hold on
viscircles(centres(2, :), raddi, 'LineWidth', 3, 'Color', 'r');
pos = centres(2, :) + [-0.5, -0.8];
text(pos(1), pos(2), parts{2});

viscircles(centres(1, :), raddi, 'LineWidth', 3, 'Color', 'r');
pos = centres(1, :) + [-0.5, -0.8];
text(pos(1), pos(2), parts{1});

viscircles(centres(4, :), raddi, 'LineWidth', 3, 'Color', 'r');
pos = centres(4, :) + [-0.4, 0.8];
text(pos(1), pos(2), parts{4});

viscircles(centres(5, :), raddi, 'LineWidth', 3, 'Color', 'r');
pos = centres(5, :) + [-0.4, 0.8];
text(pos(1), pos(2), parts{5});
axis equal

for i = 1:length(parts)
    soreted_res = sort(residual(:, i));
    thres = soreted_res(num_edges_per_node);
    for j = 1:length(parts)
        if residual(j, i) <= thres
            if i==j
                el = drawEllipse(2*raddi, raddi, phi(i), el_centres(i, 1), el_centres(j, 2), 'b');
                el.LineWidth = 3;
            else
                line(centres([i,j], 1), centres([i,j], 2), 'LineWidth', 3, 'color', 'b');
            end
        end
    end
end

axis off

end

