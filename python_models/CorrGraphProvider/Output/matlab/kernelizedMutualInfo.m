function [ mi ] = kernalizedMutualInfo( series1, series2, kernel_width, kernel_handle )
%KERNALIZEDMUTUALINFO Computes mutual information between two time series
%of arbitrary dimensions

if nargin == 2
    d = size(series1, 2) + size(series2, 2);
    n = size(series1, 1);
    kernel_width = 2*((4/(d+2))^(1/(d+4))) * n^(-1/(d+4));
    kernel_handle = NaN;
end
p_xy = estimateDensity([series1, series2], [series1, series2], kernel_width, kernel_handle);
p_x = estimateDensity(series1, series1, kernel_width, kernel_handle);
p_y = estimateDensity(series2, series2, kernel_width, kernel_handle);

h_xy = mean(-log(p_xy));
h_x = mean(-log(p_x));
h_y = mean(-log(p_y));

% lower_bound = -log(mean(p_x.*p_y./p_xy));
% upperer_bound = log(mean(p_xy./(p_x.*p_y)));
mi = h_x + h_y - h_xy;
% mi = h_xy-h_x;

% mi = mi/max(h_x, h_y);
% mi = mi/sqrt(h_x*h_y); % Dual total information
% mi = mi/h_xy;
% dbstop in kernalizedMutualInfo at 24 if ~(mi >= lower_bound && mi <= upperer_bound)
% mi = (mi - lower_bound) / (upperer_bound - lower_bound);
% mi = mi/(h_x + h_y);

end

function p_y = estimateDensity(samples, y, kernel_width, kernel_handle)
[n, d] = size(samples);
if isnan(kernel_width) 
    kernel_width = 2*((4/(d+2))^(1/(d+4))) * n^(-1/(d+4));
end
if isnan(kernel_handle)
    kernel_handle = @multivariateGaussian;
end
p_y = zeros(size(y, 1), 1);
s = cov(samples);
for i = 1:size(y, 1)
    p_y(i) = mean(kernel_handle(y(i, :), samples, d, s, kernel_width));
end
end

function r = multivariateGaussian(y, y_i, d, s, kernel_width)
diff = repmat(y, size(y_i, 1), 1)-y_i;
u = zeros(size(y_i, 1), 1);
for i = 1:size(y_i, 1)
    u(i) = diff(i, :) * ((s + 1e-9*eye(d))\diff(i, :)')/(kernel_width^2);
end
r = (1/(((2*pi)^(d/2))*(kernel_width^d)*sqrt(det(s + 1e-9*eye(d))))) *  exp(-u/2);
% In order to compute discritized entropy - removed the h^d term.
% r = (1/(((2*pi)^(d/2))*sqrt(det(s + 1e-9*eye(d)))) *  exp(-u/2);
end
