%% ------------------------------settings------------------------------ %%

% Evaluate on groundtruth or on generated data
gt_eval = true;

% Path to the file that generates this residuals
generated_seq_path = '../../../GeneratedSequences/';

% Path to groundtruth files
gt_file_path = '../../../../Data/S5/MyPoseFeatures/D3_Angles/expMap/';

% This is the width of the running window average estimator for calculating
% the residue average. Whic is then used to compute the dependency graph
window_width = 600;

% Frames per second is used to convert timesteps to indices
fps = 25;

% Offset_frames is supposed to tbe the seed sequence length.Then the time
% steps_ms signifies the amount of time in future for which the prediction
% is evaluated
offset_frames = floor(window_width/2) + 500;

% Chose the activity/activities for which the evaluation is performed
% activitie = 'directions_1'; % 'greeting_2', 'walking_1'
% activity = '17';
activity = 'walking_1';

% Mark down of indices
spine_idx = {3, [20, 21, 22], [23, 24, 25], [26, 27, 28], [29, 30, 31]};
l_arm_idx = {[32, 33, 34], [35, 36, 37], 38, [39, 40, 41]};
r_arm_idx = {[42, 43, 44], [45, 46, 47], 48, [49, 50, 51]};
l_leg_idx = {[12, 13, 14], 15, [16, 17, 18], 19};
r_leg_idx = {[4,  5,  6], 7,  [8,  9, 10], 11};

spine = {'Hips', 'Spine', 'Spine1', 'Neck', 'Head'};

right_leg = {'RightUpLeg', 'RightLeg', 'RightFoot', 'RightToeBase'};
left_leg = {'LeftUpLeg', 'LeftLeg', 'LeftFoot', 'LeftToeBase'};

left_arm = {'LeftShoulder', 'LeftArm', 'LeftForeArm', 'LeftHand',};
right_arm = {'RightShoulder', 'RightArm', 'RightForeArm', 'RightHand'};

part_names = {left_leg, right_leg, right_arm, spine, left_arm};
part_idx = {l_leg_idx, r_leg_idx, r_arm_idx, spine_idx, l_arm_idx};
%% -------------------------------------------------------------------------

if isnan(str2double(activity))
    skeleton_data_exp = load([gt_file_path, activity, '.txt']);
else
    skeleton_data_exp = dlmread([generated_seq_path, activity, '.csv'], ',', 9);
end

% Remove the columns with little variance
std_file_name = '../../../column_wise_std.text';
fid = fopen(std_file_name);
std_threshold = textscan(fid, '%s %c %f', 1, 'delimiter', ' ');
std_threshold = std_threshold{end};
fclose(fid);
stds = dlmread(std_file_name, ',', 2);
skeleton_data_exp(:, stds < std_threshold) = [];
skeleton_data_exp = skeleton_data_exp - mean(skeleton_data_exp);
skeleton_data_exp = skeleton_data_exp ./ ...
    repmat(max(skeleton_data_exp) - min(skeleton_data_exp), size(skeleton_data_exp, 1), 1);

mi_mat = 0;
frame_idx_prev = 0;

for frame_idx_raw = offset_frames:1000
    frame_idx = offset_frames + floor(2 * (frame_idx_raw - offset_frames) / window_width)...
                - floor(window_width/2);
    block_row = 0;
    row_in_blk = 0;
    col_in_blk = 0;
    block_col = 0;
    if frame_idx ~= frame_idx_prev
        for row_part = 1:5
            new_block_col_flag = true;
            for col_part = row_part : 5
                half_window = floor(window_width/2);
%                 rho = corr(skeleton_data_exp(frame_idx_raw-half_window:frame_idx_raw+half_window, ...
%                                              part_s{1}),...
%                            skeleton_data_exp(frame_idx_raw-half_window:frame_idx_raw+half_window,...
%                                              part_s{2}), 'type',...
%                            correlation_type);
                row_in_blk = block_row;
                for i = part_idx{row_part}
                    col_in_blk = block_col;
                    row_in_blk = row_in_blk + 1;
                    for j = part_idx{col_part}
                        col_in_blk = col_in_blk + 1;
                        rows_under_window = frame_idx_raw-half_window:frame_idx_raw+half_window;
%                         mi_curr = 0;
%                         for dim_joint1  = i{1}+1
%                             for dim_joint2 = j{1}+1
%                                 mi_curr = mi_curr + kernalizedMutualInfo(...
%                                     skeleton_data_exp(rows_under_window, dim_joint1), ...
%                                     skeleton_data_exp(rows_under_window, dim_joint2));
%                             end
%                         end
                        mi_curr = kernalizedMutualInfo(skeleton_data_exp(rows_under_window, i{1}+1), ...
                                                       skeleton_data_exp(rows_under_window, j{1}+1));
                        mi_mat(row_in_blk, col_in_blk) = mi_curr;
                    end
                    if new_block_col_flag
                        cols_first_blk = col_in_blk;
                        new_block_col_flag = false;
                    end
                end
                block_col = col_in_blk;
            end
            block_col = cols_first_blk;
            block_row = row_in_blk;
        end
        frame_idx_prev = frame_idx;
    end
%     mi_mat = mi_mat - mean(mean(mi_mat));
%     mi_mat = mi_mat / max(max(mi_mat));
    h = HeatMap(mi_mat, 'RowLabels', [part_names{:}], 'ColumnLabels', [part_names{:}]);
    % Check the grouping consistency
    groups = {1:4, 5:8, 9:12, 13:17, 18:21};
%     mi_mat(mi_mat==0)= mi_mat(mi_mat'==0);
    for rows = 1:length(groups)
        this_group_sum = sum(sum(mi_mat(groups{rows}, groups{rows})));
        for cols = 1:length(groups)
            if this_group_sum  < sum(sum(mi_mat(groups{rows}, groups{cols})))
                disp(['Problem in',  num2str([rows, cols]), ' block.'])
            end
        end
    end
    break
end