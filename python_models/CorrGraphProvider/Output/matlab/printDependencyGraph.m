% This script generates the Dependency graph from the  residual scores
clc
clear all

addpath('../../../../Matlab/ploting')
%% ------------------------------settings------------------------------ %%
% Path to the directoryin which 
residue_dir = '../currentNetwork/';

% This is the width of the running window average estimator for calculating
% the residue average. Whic is then used to compute the dependency graph
window_width = 15;

% Timesteps around which the window_width will beconsidered and dependency
% graph drawn. The unit is milisecond
timesteps_ms = [100, 200, 300, 500, 1000, 2000, 3000];

% Frames per second is used to convert timesteps to indices
fps = 25;

% Offset_frames is supposed to tbe the seed sequence length.Then the time
% steps_ms signifies the amount of time in future for which the prediction
% is evaluated
offset_frames = 50;

% Chose the activity/activities for which the evaluation is performed
% activities = {'directions_1', 'greeting_2', 'walking_1'};
activities = {'17'};
%% --------------------Compute the correlation Matrix------------------- %%
part_pairs = ...
    {'l_leg_r_leg', 'l_leg_r_hand', 'l_leg_spine', 'l_leg_l_hand', 'r_leg_r_hand', 'r_leg_spine', ...
     'r_leg_l_hand', 'r_hand_spine', 'r_hand_l_hand', 'spine_l_hand'};

for activity = activities 
    column = 1;
    for part_pair = part_pairs
        residuals = ...
            load([residue_dir, activity{1}, '/', part_pair{1}, '.csv']);
        avg_perjoint_residual(:, column) = mean(residuals, 2);
        column = column + 1;
    end

    frame_indices = offset_frames + round(timesteps_ms * fps / 1000);

    res_mat = zeros(5, 5);
    for f_idx = frame_indices
        start_row = round(f_idx - (window_width -1)/2);
        end_row = round(f_idx + (window_width -1)/2);
        current_corravg_perjoint_residual = ...
            mean(avg_perjoint_residual(start_row:end_row, :), 1);
        count = 1;
        for row = 1:4
            for col = 1+row : 5
                res_mat(row, col) = current_corravg_perjoint_residual(count);
                res_mat(col, row) = res_mat(row, col);
                count = count + 1;
            end
        end
        humanStructureGivenRes(res_mat)
        frame = getframe;
        imwrite(frame.cdata, [residue_dir, activity{1}, num2str(f_idx), '.jpg'])
        close all
    end
end