clc
clear all
addpath('../')
% rng(17)

% dims = [25, 6];
% randomseries = rand(dims(1), dims(2));
% mi = kernalizedMutualInfo(randomseries, randomseries)
% 
% randomseries = rand(dims(1), dims(2));
% mi = kernalizedMutualInfo(randomseries, randomseries + 0.1*rand(dims(1), dims(2)))
% 
% randomseries = rand(dims(1), dims(2));
% mi = kernalizedMutualInfo(randomseries, randomseries + 0.5*rand(dims(1), dims(2)))
% 
% randomseries1 = rand(dims(1), dims(2));
% randomseries2 = rand(dims(1), dims(2)+2);
% mi = kernalizedMutualInfo(randomseries1, randomseries2)



% Test with joint gaussian distributions
n = 50;
dim = 3;
mu = rand(1, dim*2);
std = rand(dim*2);
variance = std'*std;
R = chol(variance);
z = repmat(mu,n,1) + randn(n,2*dim)*R;

% mu = [1 2];
% variance = [1 0.8; 0.8 2];
% R = chol(variance);
% z = repmat(mu,n,1) + randn(n,2)*R;

% 
e = exp(1);
entropy_x = 0.5*log((2*pi*e)^dim*det(variance(1:dim,1:dim)));
entropy_y = 0.5*log((2*pi*e)^dim*det(variance(dim+1:end,dim+1:end)));
entropy_xy = 0.5*log((2*pi*e)^(dim*2)*det(variance));

mi_expt = 0.5*log(det(variance(1:dim,1:dim)) * det(variance(dim+1:end, dim+1:end))/det(variance))
% mi_expt = mi_expt / entropy_xy
% mi_comp = kernalizedMutualInfo(z(:, 1:dim), z(:, dim+1:end))
series2 = z(:, 1:dim) * rand(dim, dim);
mi_comp = kernalizedMutualInfo(z(:, 2:dim), series2(:, 1:dim-1))
% % diff = mi_expt - mi_comp

% 
% z = rand(100, 1);
% z1 = rand(100, 1);
% mi_comp = kernalizedMutualInfo(z, z1)