#!/usr/bin/env python

# /*
#  * Created by Partha on 03/06/2017.
#  */

import sys
sys.path.append('../')
import numpy

import gflags
from gflags import FLAGS
import keras

from pre_post_processing import inputGenerator
import getNetwork
import generalUtility
import iohandler

gflags.DEFINE_boolean('all', False, 'Train and test all models')
gflags.DEFINE_boolean('l_leg_r_leg', False, 'Flag to train/retain/test leg-leg model')
gflags.DEFINE_boolean('l_leg_r_hand', False, 'Flag to train/retain/test l_leg_r_hand model')
gflags.DEFINE_boolean('l_leg_spine', False, 'Flag to train/retain/test l_leg_spine model')
gflags.DEFINE_boolean('l_leg_l_hand', False, 'Flag to train/retain/test l_leg_l_hand model')
gflags.DEFINE_boolean('r_leg_r_hand', False, 'Flag to train/retain/test r_leg_r_hand model')
gflags.DEFINE_boolean('r_leg_spine', False, 'Flag to train/retain/test r_leg_spine model')
gflags.DEFINE_boolean('r_leg_l_hand', False, 'Flag to train/retain/test r_leg_l_hand model')
gflags.DEFINE_boolean('r_hand_spine', False, 'Flag to train/retain/test r_hand_spine model')
gflags.DEFINE_boolean('r_hand_l_hand', False, 'Flag to train/retain/test r_hand_l_hand model')
gflags.DEFINE_boolean('spine_l_hand', False, 'Flag to train/retain/test spine_l_hand model')
gflags.DEFINE_boolean('all_networks', True, 'Use all the networks')
gflags.DEFINE_boolean('train_model', False, 'Flag to train/ test above specified model')
gflags.DEFINE_boolean('load_pre_trained_weights', True, 'Flag to train/ test above specified model')
gflags.DEFINE_boolean('test_on_groundtruth', True, 'Generate residuals for ground truth data or generated data')
gflags.DEFINE_boolean('use_recurrent_architecture', True, 'Use recurrent architecture or feedforward one for dependency '
                                                          'calculation')

gflags.DEFINE_integer('epochs', 2, 'Number of epochs each model is trained')
gflags.DEFINE_integer('mini_seq_length', 1, "This doesn't have any effect here. Kept as datagenerator needs it")


def main():
    try:
        if not (FLAGS.load_pre_trained_weights or FLAGS.train_model):
            ValueError("Either load pretrained weights or train models now. Both 'train' and 'load_pre_trained_weights'"
                       " flags can not be False")
        if FLAGS.num_files == -1 and FLAGS.train_model:
            load_preprocessing_params_from_file = False
        else:
            load_preprocessing_params_from_file = True

        spine_idx, l_arm_idx, r_arm_idx, l_leg_idx, r_leg_idx = generalUtility.getBodyPartIndices("H3.6M")
        part_pairs = ["l_leg_r_leg", "l_leg_r_hand", "l_leg_spine", "l_leg_l_hand", "r_leg_r_hand", "r_leg_spine",
                      "r_leg_l_hand", "r_hand_spine", "r_hand_l_hand", "spine_l_hand"]
        index_tuples = [[l_leg_idx, r_leg_idx], [r_arm_idx, l_leg_idx], [spine_idx, l_leg_idx], [l_arm_idx, l_leg_idx],
                        [r_arm_idx, r_leg_idx], [spine_idx, r_leg_idx], [l_arm_idx, r_leg_idx], [spine_idx, r_arm_idx],
                        [r_arm_idx, l_arm_idx], [spine_idx, l_arm_idx]]
        part_pair_idx_map = dict(zip(part_pairs, index_tuples))

        if FLAGS.all_networks:
            for network in part_pairs:
                setattr(FLAGS, network, True)

        # Prepare data
        input_provider = inputGenerator.InputProvider(load_preprocessing_params_from_file,
                                                      not FLAGS.test_on_groundtruth,
                                                      FLAGS.test_on_groundtruth)
        if FLAGS.train_model:
            input_provider.loadTimeSeriesData()
            train_data, target_labels, current_file_config = input_provider.getTrainingSeq(FLAGS.mini_seq_length, True)
            if not FLAGS.use_recurrent_architecture:  # Flatten time dimension iff not recurrent architecture is used
                train_data = numpy.reshape(train_data, (-1, train_data.shape[-1]))

        # Choose network
        network = {}
        for network_name in part_pair_idx_map:
            # We have to re-instanciate the optimizer every time we want to train a network or else the LR and other
            # state will be retained
            # optimizer = keras.optimizers.SGD(lr=0.001, momentum=0.9, decay=0.0, nesterov=True, clipvalue=25)
            # optimizer = keras.optimizers.Adagrad(lr=0.0001, epsilon=1e-08, decay=0.0001)
            optimizer = keras.optimizers.Adam(lr=0.001, beta_1=0.9, beta_2=0.999, epsilon=1e-08, decay=0.00001)
            # optimizer = keras.optimizers.RMSprop(lr=0.0001, clipvalue=25, decay=0.05)
            callbacks = []
            lr_schedule = keras.callbacks.ReduceLROnPlateau(monitor='val_loss', factor=0.5, patience=3, verbose=1,
                                                            mode='min', epsilon=0.001, cooldown=0, min_lr=0)
            callbacks.append(lr_schedule)
            early_stopper = keras.callbacks.EarlyStopping(monitor='val_loss', patience=2, min_delta=0)
            callbacks.append(early_stopper)

            print "Network_name = " + str(network_name)
            if getattr(FLAGS, network_name) or FLAGS.all:
                if FLAGS.use_recurrent_architecture:
                    network[network_name] = \
                        getNetwork.getRecurrentNetwork((FLAGS.mini_seq_length, len(part_pair_idx_map[network_name][0])),
                                                       len(part_pair_idx_map[network_name][1]))
                else:
                    network[network_name] = getNetwork.getNetwork(len(part_pair_idx_map[network_name][0]),
                                                                  len(part_pair_idx_map[network_name][1]))

                if FLAGS.load_pre_trained_weights:
                    network[network_name].load_weights(network_name + ".h5")

            network[network_name].compile(optimizer=optimizer, loss="mse")

            # Training
            if FLAGS.train_model:
                if not FLAGS.use_recurrent_architecture:
                    x_train = train_data[:, part_pair_idx_map[network_name][0]]
                    y_train = train_data[:, part_pair_idx_map[network_name][1]]
                else:
                    x_train = train_data[:, :, part_pair_idx_map[network_name][0]]
                    y_train = train_data[:, :, part_pair_idx_map[network_name][1]]

                network[network_name].fit(x_train, y_train, shuffle=True, nb_epoch=FLAGS.epochs, callbacks=callbacks,
                                          batch_size=FLAGS.batch_size, validation_split=FLAGS.validation_split,
                                          verbose=1)
                network[network_name].save_weights(network_name + ".h5")

            # Test phase
            while True:
                data_test, current_file_config = input_provider.getTestSeq(FLAGS.seed_seq_length,
                                                                           FLAGS.folder_path_to_data_files,
                                                                           FLAGS.file_type, FLAGS.num_files)
                if FLAGS.use_recurrent_architecture:
                    network[network_name] = \
                        getNetwork.getRecurrentNetwork((1, len(part_pair_idx_map[network_name][0])),
                                                       len(part_pair_idx_map[network_name][1]), stateful=True)
                if data_test is None:
                    break

                data_test = numpy.reshape(data_test, (-1, data_test.shape[-1]))
                x_test = data_test[:, part_pair_idx_map[network_name][0]]
                y_test = data_test[:, part_pair_idx_map[network_name][1]]

                residue = numpy.zeros((FLAGS.seed_seq_length, len(part_pair_idx_map[network_name][1])))
                for i in range(FLAGS.seed_seq_length):
                    y_pred = network[network_name].predict(x_test[None, i:i+1, :], batch_size=1, verbose=0)
                    residue[i, :] = numpy.square(y_test[i, :] - y_pred[0, 0, :])

                action = current_file_config.seed_seq_origination_file.split("/")[-1].split(".")[0]
                iohandler.writeToFile(action, network_name, residue)
            print "Done with " + str(network_name)

    except(KeyboardInterrupt, SystemExit):
        print 'Interrupted'
        if FLAGS.train_model:
            # Save the model weights
            for network_name in network:
                network[network_name].save_weights(network_name + ".h5")
            print "<<<<<<<<<<<<<<<<Saved Models>>>>>>>>>>>>>>>>>>>>>>>"

if __name__ == "__main__":
    FLAGS(sys.argv)
    main()