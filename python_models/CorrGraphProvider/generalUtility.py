#!/usr/bin/env python

# /*
#  * Created by Partha on 04/06/2017.
#  */

def getBodyPartIndices(dataset_type):
    """Generates the index list of different body parts for different data sets"""
    def expandToAll3AxisIndices(index_list):
        expanded_list = []
        for idx in index_list:
            for i in range(3):
                expanded_list.append((idx-1) * 3 + i)

        return expanded_list

    if dataset_type == "Holden":
        spine_base_indices = [10, 11, 12, 13, 1, 22]
        right_arm_base_indices = [18, 19, 20, 21]
        left_arm_base_indices = [14, 15, 16, 17]
        left_leg_base_indices = [2, 3, 4, 5]
        right_leg_base_indices = [6, 7, 8, 9]

        spine_idx = expandToAll3AxisIndices(spine_base_indices)
        l_arm_idx = expandToAll3AxisIndices(left_arm_base_indices)
        r_arm_idx = expandToAll3AxisIndices(right_arm_base_indices)
        l_leg_idx = expandToAll3AxisIndices(left_leg_base_indices)
        r_leg_idx = expandToAll3AxisIndices(right_leg_base_indices)

        return spine_idx, l_arm_idx, r_arm_idx, l_leg_idx, r_leg_idx

    elif dataset_type == "H3.6M":
        spine_idx = [0, 1, 2, 3, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31]
        l_arm_idx = [32, 33, 34, 35, 36, 37, 38, 39, 40, 41]
        r_arm_idx = [42, 43, 44, 45, 46, 47, 48, 49, 50, 51]
        l_leg_idx = [12, 13, 14, 15, 16, 17, 18, 19]
        r_leg_idx = [4,  5,  6,  7,  8,  9, 10, 11]
        return spine_idx, l_arm_idx, r_arm_idx, l_leg_idx, r_leg_idx

    else:
        NotImplementedError("Implementation for dataset type " + str(dataset_type) + " isn't yet available")