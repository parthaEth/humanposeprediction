#!/usr/bin/env python

# /*
#  * Created by Partha on 03/06/2017.
#  */

from keras.layers import Input, Dense, LSTM
from keras.constraints import maxnorm
from keras.models import Model
from keras.regularizers import l2


def getNetwork(input_dim, out_dim):
    i = Input(shape=(input_dim,), dtype='float32')
    o = Dense(16, activation='relu', kernel_constraint=maxnorm(3), bias_constraint=maxnorm(3))(i)
    o = Dense(out_dim, activation='linear', kernel_constraint=maxnorm(3), bias_constraint=maxnorm(3))(o)
    return Model(inputs=i, outputs=o)


def getRecurrentNetwork(input_dim, out_dim, stateful=False):
    if stateful:
        i = Input(batch_shape=(1, input_dim[0], input_dim[1]), dtype='float32')
    else:
        i = Input(shape=input_dim, dtype='float32')

    o = LSTM(16, return_sequences=True, inner_init='orthogonal', stateful=stateful,
             W_regularizer=l2(3))(i)
    o = LSTM(out_dim, return_sequences=True, inner_init='orthogonal', stateful=stateful,
             W_regularizer=l2(3))(o)
    return Model(inputs=i, outputs=o)