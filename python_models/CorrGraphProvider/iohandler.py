#!/usr/bin/env python

# /*
#  * Created by Partha on 05/06/2017.
#  */

import os
import numpy
import gflags
from gflags import FLAGS

gflags.DEFINE_string('destination_folder', './Output/currentNetwork', 'The folder where generated files will be stored')


def writeToFile(action, network_name, residues):
    destination_dir = FLAGS.destination_folder + "/" + action
    if not os.path.isdir(destination_dir):
        os.mkdir(destination_dir)
    numpy.savetxt(destination_dir + "/" + network_name + '.csv', residues, delimiter=',')
