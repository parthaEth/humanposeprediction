import os, sys
sys.path.append('../')
import pre_post_processing.inputGeneratorHolden
from inputGenerator import *
import pre_post_processing.outputPostProcessor
from ModelBuilder import model_architectures
import gflags
from gflags import FLAGS
import glog as log
import numpy
from numpy import linalg as LA


def parseToListOfInts(list_nums):
    collected_ints = []
    for nums in list_nums:
        collected_ints.append(int(nums))

    return collected_ints


def expandToAll3AxisIndices(index_list):
    expanded_list = []
    for idx in index_list:
        for i in range(3):
            expanded_list.append((idx-1) * 3 + i)

    return expanded_list


def recoverOriginalScale(pose):
    min_max = numpy.load('mean_and_std.npz')
    shift_vector, scale_vector = min_max['Xmin'], min_max['Xmax']

    pose *= scale_vector[:-4]
    pose += shift_vector[:-4]


def computeL2errInRealScale(pose1, pose2, output_post_processor):
    if FLAGS.dataset == 'holden':
        recoverOriginalScale(pose1)
        recoverOriginalScale(pose2)
    else:
        output_post_processor.accumulateTimeseriesRow(pose1)
        output_post_processor.accumulateTimeseriesRow(pose2)
        pose = output_post_processor.getRescaledAndResized()
        pose1 = pose[0, :]
        pose2 = pose[1, :]

    err = numpy.sum((pose1 - pose2)**2)**0.5

    if FLAGS.dataset == 'holden':
        current_scale_to_average_per_joint_in_cm = 0.33321
        return err * current_scale_to_average_per_joint_in_cm
    else:
        return err


gflags.DEFINE_integer('seq_index', 1, 'Choose the sequence on which to drop joints')
gflags.DEFINE_integer('time_step', 100, 'Choose the time step which to perform inpaint on')

gflags.DEFINE_list('joint_indx_to_drop', ['7', '8', '9', '10', '11', '12', '13', '14','15', '16', '17'],
                   'Indices of the joints which will be dropped from the input')
gflags.DEFINE_string('model_path', '../savedWeights/holdenDataset/AutoEncoder/model_weights.h5',
                     'Full path to file containing model weights')
gflags.DEFINE_string('dataset', 'holden', 'holden/ h36m')

gflags.DEFINE_boolean('dropout_corruption', False,
                      'Flag to decide between dropout corruption or random noise corruption')
gflags.DEFINE_boolean('on_train_data', True, 'Flag to decide between Training data or test data')

dir_path = os.path.dirname(os.path.realpath(__file__))
parent_dir, tail = os.path.split(dir_path)
folder_path_to_data_files = parent_dir + "/Data"
parent_dir_to_negative_smpls = parent_dir + "/negativeSamples"
parent_dir_to_generated_smpls = dir_path + "/GeneratedSequences"

batch_size = 1
validation_split = 0.1
down_sample_in_time_by_n = 2
num_files = 4
seed_seq_length = 300
file_type = ".txt"

def main(argv):
    FLAGS(argv)  # parse flags

    if FLAGS.dataset == 'holden':
        input_provider = pre_post_processing.inputGeneratorHolden.InputProviderHolden(
            batch_size, validation_split, down_sample_in_time_by_n, num_files)

        if FLAGS.on_train_data:
            test_data = input_provider.getTrainingSeq(100, True)
            test_data = test_data[0]
            test_data = test_data[0:1, :, :]
        else:
            for i in range(FLAGS.seq_index):
                test_data, current_file_config = input_provider.getTestSeq(seed_seq_length,
                                                                           parent_dir_to_generated_smpls,
                                                                           file_type, num_files)
        output_post_processor = ''
    elif FLAGS.dataset == 'h36m':
        input_provider = InputProvider(batch_size, validation_split, down_sample_in_time_by_n,
                                       load_preprocessing_params_from_file=True, seed_seq_length=0,
                                       test_subject=5, classifier_rnn=False, classifier_rnn_on_grnd_truth=False)
        input_provider.resetCurrentFileReadHead()
        if FLAGS.on_train_data:
            test_data = input_provider.getTrainingSeq(100, True)
            test_data = test_data[0]
            test_data = test_data[0:1, :, :]
        else:
            for i in range(FLAGS.seq_index):
                test_data, current_file_config = input_provider.getTestSeq(seed_seq_length,
                                                                           '/home/partha/ait-ra-ship/Data',
                                                                           file_type, num_files)

        output_post_processor = input_provider.getOutputPostProcessor()

    else:
        print "Dataset not avialable"

    if FLAGS.dataset == 'holden':
        batch_input_shape = (batch_size, 1, test_data.shape[2]-4)
    else:
        batch_input_shape = (batch_size, 1, test_data.shape[2])

    auto_encoder_model = model_architectures.getAutoencoderOnly(batch_input_shape, batch_input_shape[-1], 0)
    auto_encoder_model.load_weights(FLAGS.model_path, by_name=False)

    dropped_joint_indxs = parseToListOfInts(FLAGS.joint_indx_to_drop)
    dropped_indxs = expandToAll3AxisIndices(dropped_joint_indxs)

    if FLAGS.dataset == 'holden':
        test_pose = test_data[:, FLAGS.time_step:FLAGS.time_step+1, :-4]
    else:
        test_pose = test_data[:, FLAGS.time_step:FLAGS.time_step + 1, :]

    test_pose_dropped_requested = numpy.array(test_pose)

    if FLAGS.dropout_corruption:
        test_pose_dropped_requested[:, :, dropped_indxs] = 0
    else:
        range_scale = 0.4
        test_pose_dropped_requested[:, :, dropped_indxs] += numpy.random.uniform(low=-range_scale, high=range_scale,
                                                                                 size=len(dropped_indxs))

    error_without_rec = computeL2errInRealScale(numpy.array(test_pose[0, 0, :]),
                                                numpy.array(test_pose_dropped_requested[0, 0, :]),
                                                output_post_processor)

    reconstructed_pose = auto_encoder_model.predict(numpy.array(test_pose_dropped_requested))

    rec_error = computeL2errInRealScale(numpy.array(test_pose[0, 0, :]), numpy.array(reconstructed_pose[0, 0, :]),
                                        output_post_processor)

    print "Reconstruction error = " + str(rec_error) + " cm/joint, error introduced = " + str(error_without_rec) + \
          " cm/joint"

    if FLAGS.dataset == 'holden':
        original_and_recovered = numpy.zeros((3, test_data.shape[2]-4))
        original_and_recovered[0, :] = test_pose[0, 0, :]
        original_and_recovered[1, :] = test_pose_dropped_requested[0, 0, :]
        original_and_recovered[2, :] = reconstructed_pose[0, 0, :]

        recoverOriginalScale(original_and_recovered)
    else:
        output_post_processor.accumulateTimeseriesRow(test_pose[0, 0, :])
        output_post_processor.accumulateTimeseriesRow(test_pose_dropped_requested[0, 0, :])
        output_post_processor.accumulateTimeseriesRow(reconstructed_pose[0, 0, :])
        original_and_recovered = output_post_processor.getRescaledAndResized()


    if os.path.exists('corrupted_recovered_pose.txt'):
        os.remove('corrupted_recovered_pose.txt')
        print "Removed Old"

    numpy.savetxt('corrupted_recovered_pose.txt', original_and_recovered , delimiter=",")
    print "File written"


if __name__ == "__main__":
    main(sys.argv)
