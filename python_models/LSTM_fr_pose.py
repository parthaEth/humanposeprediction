#!/usr/bin/env python

# /*
#  * Created by Partha on 12/11/2016.
#  */

from keras.models import Sequential
from ModelBuilder import model_architectures
import numpy, math


# learning rate schedule, Not in use as of now
def snakeyDecay(epoch):
    decay = 0.01
    initial_lrate = 0.001
    lrate = initial_lrate/(1 + decay * epoch)
    return lrate

def resetRecurssively(model):
    print "Reseting " + model.name
    for i in range(len(model.layers)):
        if model.layers[i].name.find('model') >= 0:
            resetRecurssively(model.layers[i])
        elif hasattr(model.layers[i], 'stateful'):
            print "Reseting layer " + model.layers[i].name
            model.layers[i].reset_states()

def getBodyPartIndices(dataset_type):
    """Generates the index list of different body parts for different data sets"""
    def expandToAll3AxisIndices(index_list):
        expanded_list = []
        for idx in index_list:
            for i in range(3):
                expanded_list.append((idx-1) * 3 + i)

        return expanded_list

    if dataset_type == "Holden":
        spine_base_indices = [10, 11, 12, 13, 1, 22]
        right_arm_base_indices = [18, 19, 20, 21]
        left_arm_base_indices = [14, 15, 16, 17]
        left_leg_base_indices = [2, 3, 4, 5]
        right_leg_base_indices = [6, 7, 8, 9]

        spine_idx = expandToAll3AxisIndices(spine_base_indices)
        l_arm_idx = expandToAll3AxisIndices(left_arm_base_indices)
        r_arm_idx = expandToAll3AxisIndices(right_arm_base_indices)
        l_leg_idx = expandToAll3AxisIndices(left_leg_base_indices)
        r_leg_idx = expandToAll3AxisIndices(right_leg_base_indices)

        return spine_idx, l_arm_idx, r_arm_idx, l_leg_idx, r_leg_idx

    elif dataset_type == "H3.6M":
        spine_idx = [0, 1, 2, 3, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31]
        l_arm_idx = [32, 33, 34, 35, 36, 37, 38, 39, 40, 41]
        r_arm_idx = [42, 43, 44, 45, 46, 47, 48, 49, 50, 51]
        l_leg_idx = [12, 13, 14, 15, 16, 17, 18, 19]
        r_leg_idx = [4,  5,  6,  7,  8,  9, 10, 11]
        return spine_idx, l_arm_idx, r_arm_idx, l_leg_idx, r_leg_idx

    else:
        print "Fatal error: Body part table for provided database : " + dataset_type + " isn't available"
        exit(16)


class PoseSquenceLearnerLSTM:
    """Creates and lets one train save interrupt model training for human poses"""
    def __init__(self,
                 input_dropout_rate,
                 layer_sizes,
                 data_dim,
                 burn_in_length_before_training,
                 batch_size,
                 statefulness,
                 seq_length,
                 look_ahead_step,
                 callbacks,
                 classifier_rnn,
                 holden_dataset,
                 network_name,
                 optimizer='RMSprop',
                 loss='mean_squared_error'):
        """Initialize LSTM model"""
        self._batch_size = 1
        self._nb_classes = 10
        self._burn_in_length_before_training = 0
        self._stateful = True
        self._stateful = statefulness
        self._burn_in_length_before_training = burn_in_length_before_training
        self._batch_size = batch_size
        self._look_ahead_step = look_ahead_step
        self._callbacks = callbacks
        self._trainable_length = seq_length - look_ahead_step

        self._classifier_rnn = classifier_rnn
        self._replace_dim = 6
        self._holden_dataset = holden_dataset
        self._network_name = network_name

        if len(layer_sizes) < 1:
            print "There has to be a minimum of one layer in a network"
        else:
            # lrate = LearningRateScheduler(snakeyDecay)
            # self._callbacks.append(lrate)

            self._loss = loss
            self._optimizer = optimizer

            W_regularizer_val = 0.0000001

            if holden_dataset:
                out_dim = data_dim - 4
                batch_input_shape = (batch_size, self._trainable_length, data_dim - 4)
            else:
                out_dim = data_dim
                batch_input_shape = (batch_size, self._trainable_length, data_dim)

            if network_name == "ERD":
                if classifier_rnn:
                    print "Fatal error: This model for Action classification is not yet avaiable"
                    exit(17)

                self._model = model_architectures.getERD(batch_input_shape, W_regularizer_val, self._stateful, out_dim,
                                                         holden_dataset, input_dropout_rate)
                self._model.compile(loss=loss, optimizer=optimizer)
            elif network_name == "LSTM3LR":
                if classifier_rnn:
                    self._loss_weights = [0.6, 0.4]
                    self._model = \
                        model_architectures.getLSTM3LR(batch_input_shape, W_regularizer_val, self._stateful, out_dim,
                                                       classifier_rnn=True, holden_dataset=holden_dataset,
                                                       input_dropout_rate=input_dropout_rate)
                    self._model.compile(loss=[loss, 'mse'], loss_weights=self._loss_weights,
                                        optimizer=optimizer, metrics={'ActionClass':'accuracy'})
                else:
                    self._model = \
                        model_architectures.getLSTM3LR(batch_input_shape, W_regularizer_val, self._stateful, out_dim,
                                                       classifier_rnn=False, holden_dataset=holden_dataset,
                                                       input_dropout_rate=input_dropout_rate)
                    self._model.compile(loss=loss, optimizer=optimizer)
            elif network_name == "AutoEncoderLSTM":
                if classifier_rnn:
                    print "Fatal error: This model for Action classification is not yet avaiable"
                    exit(18)
                self._loss_weights = [0.9, 0.1]
                self._model = model_architectures.getDropoutAutoEncoderLSTM(batch_input_shape,
                                                                            W_regularizer_val, self._stateful, out_dim,
                                                                            holden_dataset,
                                                                            input_dropout_rate=input_dropout_rate)
                self._model.compile(loss=[loss, loss], loss_weights=self._loss_weights, optimizer=optimizer)

            elif network_name == "SRNN":
                if classifier_rnn:
                    print "Fatal error: This model for Action classification is not yet avaiable"
                    exit(19)

                if self._holden_dataset:
                    dataset_type = "Holden"
                else:
                    dataset_type = "H3.6M"

                spine_idx, l_arm_idx, r_arm_idx, l_leg_idx, r_leg_idx = getBodyPartIndices(dataset_type)
                self._model = model_architectures.getSRNN(batch_input_shape, W_regularizer_val, self._stateful, out_dim,
                                                          spine_idx, l_arm_idx, r_arm_idx, l_leg_idx, r_leg_idx,
                                                          input_dropout_rate)
                self._model.compile(loss=loss, optimizer=optimizer)
            elif network_name == "AutoencoderOnly":
                self._model = model_architectures.getAutoencoderOnly(batch_input_shape, out_dim, input_dropout_rate)
                self._model.compile(loss=loss, optimizer=optimizer)
            else:
                network_names = network_name.split(',')
                if self._holden_dataset:
                    dataset_type = "Holden"
                else:
                    dataset_type = "H3.6M"
                spine_idx, l_arm_idx, r_arm_idx, l_leg_idx, r_leg_idx = getBodyPartIndices(dataset_type)
                self._model = model_architectures.getStackedModel(network_names, batch_input_shape, W_regularizer_val,
                                                                  self._stateful, out_dim, holden_dataset,
                                                                  input_dropout_rate, classifier_rnn, spine_idx,
                                                                  l_arm_idx, r_arm_idx, l_leg_idx, r_leg_idx)
                self._model.compile(loss=loss, optimizer=optimizer)

    def freezeFirstNLayers(self, n):
        if n < 0:
            n = len(self._model.layers) + n
        for i in range(n):
            if len(self._model.layers[i].get_weights()) > 0:
                print "Freezing " + self._model.layers[i].name
                if hasattr(self._model.layers[i], 'layers'):
                    PoseSquenceLearnerLSTM.freezeLayersOfModel(self._model.layers[i])
                else:
                    self._model.layers[i].trainable = False

        self._model.compile(loss=self._model.loss, optimizer=self._model.optimizer)

    def freezelastNLayers(self, n):
        if n < 0:
            n = len(self._model.layers) + n
        for i in range(n):
            if len(self._model.layers[-(i+1)].get_weights()) > 0:
                print "Freezing " + self._model.layers[-(i+1)].name
                if hasattr(self._model.layers[-(i+1)], 'layers'):
                    PoseSquenceLearnerLSTM.freezeLayersOfModel(self._model.layers[-(i+1)])
                else:
                    self._model.layers[-(i+1)].trainable = False

        self._model.compile(loss=self._model.loss, optimizer=self._model.optimizer)

    @staticmethod
    def freezeLayersOfModel(model):
        for layer_idx in range(len(model.layers)):
            if hasattr(model.layers[layer_idx], 'layers'):
                PoseSquenceLearnerLSTM.freezeLayersOfModel(model.layers[layer_idx])
            else:
                model.layers[layer_idx].trainable = False

    def freezeByid(self, list):
        if len(list) != len(self._model.layers):
            exit('The number of flags provided doesn\'t match number of layers in the model flgs provided = '
                  + str(len(list)) + 'number of latters = ' + str(len(self._model.layers)))
        for i in range(len(list)):
            if list[i] == 1:
                if len(self._model.layers[i].get_weights()) <= 0:
                    print "Fatal Error: Trying to freez layer with no weights"
                    exit(18)

                print "Freezing " + self._model.layers[i].name
                self._model.layers[i].trainable = False
            self._model.compile(loss=self._loss, optimizer=self._optimizer)

    def train(self, time_series, target_data, validation_split, nb_epoch, shuffle, sigma, piggy_back_train_order):
        """Time_series is expected to be a 2D array arranged as timestepsXfeatures"""

        time_steps = time_series.shape[1]
        # target_data[:, :, :6] = 0
        # time_series = time_series.reshape(1, time_steps, time_series.shape[1])
        if not self._stateful and self._burn_in_length_before_training > 0:
            print "FATAL error: giving burn-in before training starts is impossible if the LSTM is not stateful." \
                  " Please make the LSTM layers stateful"
            exit(1)
        elif self._burn_in_length_before_training > 0:
            print "FATAL ERROR: You reached an outdated code section fix it now"
            exit(9)
            for i in range(self._burn_in_length_before_training):
                self._model.predict(time_series[i, :], verbose=0)

            # This part of the code is now outdated needs to be fixed or removed!
            return self._model.fit(
                time_series[:, self._burn_in_length_before_training:time_steps-self._look_ahead_step, :],
                time_series[:, self._burn_in_length_before_training + self._look_ahead_step:time_steps, :],
                nb_epoch=1, batch_size=self._batch_size, verbose=1, shuffle=False,
                validation_split=validation_split, callbacks=self._callbacks)
        else:
            if piggy_back_train_order <= 0:
                if self._network_name == "AutoencoderOnly":
                    target_data = time_series[:, :self._trainable_length, :]
                else:
                    if self._classifier_rnn:
                        # time_series[:, :, :6] = 0
                        target_data = \
                            [target_data[:, self._look_ahead_step:self._trainable_length + self._look_ahead_step, :],
                             time_series[:, self._look_ahead_step:self._trainable_length + self._look_ahead_step, :]]
                    else:
                        if self._holden_dataset:
                            time_series = time_series[:, :, :-4]

                        if self._network_name == 'AutoEncoderLSTM':
                            target_data = \
                                [time_series[:,
                                     self._look_ahead_step:self._trainable_length + self._look_ahead_step, :],
                                 time_series[:,:self._trainable_length, :]]
                        else:
                            target_data = \
                                time_series[:, self._look_ahead_step:self._trainable_length + self._look_ahead_step, :]

                hist = numpy.zeros((nb_epoch, 2))
                if sigma > 0:
                    for epoch in range(nb_epoch):
                        validation_length = int(math.ceil(time_series.shape[0] * validation_split))
                        # train_data_len = time_series.shape[0] - validation_length
                        idx = numpy.random.permutation(time_series.shape[0])
                        if self._network_name == 'AutoEncoderLSTM':
                            time_series = time_series[idx, :, :]
                            target_data = [target_data[0][idx, :, :], target_data[1][idx, :, :]]

                            validation_data = time_series[0:validation_length, :, :]
                            train_data = time_series[validation_length:, :, :]

                            validation_target = [target_data[0][0:validation_length, :, :],
                                                 target_data[1][0:validation_length, :, :]]

                            train_target = [target_data[0][validation_length:, :, :],
                                            target_data[1][validation_length:, :, :]]
                        else:
                            time_series = time_series[idx, :, :]
                            target_data = target_data[idx, :, :]

                            validation_data = time_series[0:validation_length, :, :]
                            train_data = time_series[validation_length:, :, :]

                            validation_target = target_data[0:validation_length, :, :]
                            train_target = target_data[validation_length:, :, :]

                        input_noise = numpy.random.normal(loc=0.0, scale=sigma,
                                                          size=(train_data.shape[0],
                                                          self._trainable_length,
                                                          time_series.shape[2]))
                        # input_noise1 = numpy.random.normal(loc=0.0, scale=sigma,
                        #                                   size=(train_data.shape[0],
                        #                                         self._trainable_length,
                        #                                         time_series.shape[2]))
                        # input_noise2 = numpy.random.normal(loc=0.0, scale=sigma,
                        #                                    size=(train_data.shape[0],
                        #                                          self._trainable_length,
                        #                                          time_series.shape[2]))
                        # input_noise3 = numpy.random.normal(loc=0.0, scale=sigma,
                        #                                    size=(train_data.shape[0],
                        #                                          self._trainable_length,
                        #                                          time_series.shape[2]))
                        # input_noise4 = numpy.random.normal(loc=0.0, scale=sigma,
                        #                                    size=(train_data.shape[0],
                        #                                          self._trainable_length,
                        #                                          time_series.shape[2]))
                        # input_noise5 = numpy.random.normal(loc=0.0, scale=sigma,
                        #                                    size=(train_data.shape[0],
                        #                                          self._trainable_length,
                        #                                          time_series.shape[2]))
                        # train_data = numpy.concatenate((train_data[:, :self._trainable_length, :],
                        #                                 train_data[:, :self._trainable_length, :] + input_noise,
                        #                                 train_data[:, :self._trainable_length, :] + 2*input_noise1,
                        #                                 train_data[:, :self._trainable_length, :] + 3*input_noise2,
                        #                                 train_data[:, :self._trainable_length, :] + 4*input_noise3,
                        #                                 train_data[:, :self._trainable_length, :] + 6 * input_noise4,
                        #                                 train_data[:, :self._trainable_length, :] + 8 * input_noise5))
                        # train_target = numpy.concatenate((train_target, train_target, train_target, train_target, train_target, train_target, train_target))

                        # Notice that look ahead timesteps is not in effect while performing categorical RNN training
                        # TODO(Partha): Fix this
                        training_hist = \
                            self._model.fit(
                                train_data[:, :self._trainable_length, :] + input_noise,
                                train_target,
                                nb_epoch=1, batch_size=self._batch_size, verbose=1, shuffle=True,
                                validation_data=(validation_data[:, :self._trainable_length, :], validation_target),
                                callbacks=self._callbacks)

                        hist[epoch, :] = numpy.array([training_hist.history['loss'],
                                                      training_hist.history['val_loss']]).T
                else:
                    # Notice that look ahead timesteps is not in effect while performing categorical RNN training
                    # TODO(Partha): Fix this
                    training_hist = \
                        self._model.fit(
                            time_series[:, :self._trainable_length, :],
                            # time_series[:, :self._trainable_length, :],
                            target_data,
                            nb_epoch=nb_epoch, batch_size=self._batch_size, verbose=1, shuffle=shuffle,
                            validation_split=validation_split, callbacks=self._callbacks)

                    hist[:, 0] = numpy.array(training_hist.history['loss']).T
                    hist[:, 1] = numpy.array(training_hist.history['val_loss']).T

            else:
                hist = numpy.zeros((nb_epoch * piggy_back_train_order, 2))
                burn_in_lapse = 30
                for curr_piggy_order in range(piggy_back_train_order):
                    for epoch in range(nb_epoch):
                        pridected_train_input = time_series[:, :self._trainable_length, :]

                        for i in range(curr_piggy_order + 1):
                            # for batch in range(time_series.shape[0]/self._batch_size):
                            pridected_train_input = \
                                self._model.predict(pridected_train_input, self._batch_size, verbose=0)
                            pridected_train_input[:, :burn_in_lapse, :] = time_series[:, (i+1)*self._look_ahead_step:(i+1)*self._look_ahead_step + burn_in_lapse, :]

                        training_hist = self._model.fit(
                            pridected_train_input,
                            time_series[:, (curr_piggy_order + 2) * self._look_ahead_step:
                                           (curr_piggy_order + 2) * self._look_ahead_step + self._trainable_length, :],
                            nb_epoch=1, batch_size=self._batch_size, verbose=1, shuffle=shuffle,
                            validation_split=validation_split, callbacks=self._callbacks)
# #<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<REMOVE THIS>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>># #
#                         numpy.savetxt("original.txt", time_series[0, :self._trainable_length, :], delimiter=",")                                                    #
#                         numpy.savetxt("predicted.txt", pridected_train_input[0, :self._trainable_length, :], delimiter=",")                                         #
#                         numpy.savetxt("desired.txt", time_series[0, (curr_piggy_order + 2) * self._look_ahead_step:                                                 #
#                                                                  (curr_piggy_order + 2) * self._look_ahead_step + self._trainable_length, :], delimiter=",")        #
# #<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<REMOVE ABOVE>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>># #

                        hist[epoch + nb_epoch * curr_piggy_order, :] = numpy.array([training_hist.history['loss'],
                                                      training_hist.history['val_loss']]).T

                        print "<-----" + str(100 * (1.0 + epoch + nb_epoch * curr_piggy_order) /
                                           (nb_epoch * piggy_back_train_order)) +\
                              "% Done of piggy back training------->"
                        if hist[epoch + nb_epoch * curr_piggy_order, 1] < 3 * 1e-4:
                            break

        return hist

    def predict(self, test_data, seed_seq_length, num_predicted_time_steps, output_post_processor):
        if test_data.shape[1] * test_data.shape[0] < seed_seq_length :
            print "test_data must have the shape (1, num_time_steps, features). Also num_time_steps in test_data " \
                  "must be greater than or equal to seed_seq_length."
            exit(4)
        predicted_seq = []
        if self._holden_dataset:
            test_data = test_data[:, :, :-4]

        seq_ind = 0
        mini_seq_time_indx = 0

        # if self._classifier_rnn:
        #     test_data[:, :, :6] = 0

        for time_step in range(seed_seq_length):
            if mini_seq_time_indx + 1 > test_data.shape[1]:
                seq_ind += 1
                mini_seq_time_indx = 0
            predicted_seq = \
                self._model.predict(test_data[seq_ind:seq_ind + 1, mini_seq_time_indx:mini_seq_time_indx+1, :])

            # output_post_processor.accumulateTimeseriesRow(test_data[seq_ind, mini_seq_time_indx+1, :])
            if self._network_name == 'AutoEncoderLSTM':
                    output_post_processor.accumulateTimeseriesRow(predicted_seq[1][0, :, :])
            elif self._classifier_rnn:
                output_post_processor.accumulateTimeseriesRow(predicted_seq[0][0, :, :])
            else:
                output_post_processor.accumulateTimeseriesRow(predicted_seq[0, :, :])

            mini_seq_time_indx += 1

        if self._classifier_rnn:
            return

        for time_step in range(num_predicted_time_steps):
            if self._network_name == 'AutoEncoderLSTM':
                predicted_seq = self._model.predict(predicted_seq[0])
                output_post_processor.accumulateTimeseriesRow(predicted_seq[1][0, :, :])
            else:
                predicted_seq = self._model.predict(predicted_seq)
                output_post_processor.accumulateTimeseriesRow(predicted_seq[0, :, :])

    def predictOneTimeStep(self, test_data):
        # print self._model.stateful
        predicted_seq = self._model.predict(test_data)
        # predicted_seq[0, 0, 3:self._replace_dim] = numpy.array([0.47073982, 0.59012714 - 0.07, 0.48639886])
        # predicted_seq[0, 0, 3:self._replace_dim] = test_data[0, 0, 3:self._replace_dim]
        return predicted_seq

    def writePredictionsToFile(self, output_post_processor, dest_dir="", filename=""):
        if self._classifier_rnn:
            output_post_processor.dumpRawAccumulatedVals(filename, dest_dir)
        else:
            output_post_processor.dumpRescaledAndResizedVals(filename, dest_dir)

    def resetModel(self):
        if self._stateful:
            resetRecurssively(self._model)
        else:
            Warning("Reset state is called on a non stateful RNN")

    def saveWeights(self, model_name):
        self._model.save_weights(model_name)

    def loadWeights(self, model_name, by_name=False):
        if not by_name:
            self._model.load_weights(model_name, by_name=False)
        else:
            self._model.load_weights(model_name, by_name=True)
            for i in range(len(self._model.layers)):
                if self._model.layers[i].name.find("Model") >= 0 or self._model.layers[i].name.find("model") >= 0:
                    print "Loading weights for " + self._model.layers[i].name
                    self._model.layers[i].load_weights(model_name, by_name=True)

    def visualizeModel(self, output_file_name):
        """To view the model"""
        self._model.summary()
        if output_file_name != "":
            from keras.utils.vis_utils import plot_model
            plot_model(self._model, to_file = output_file_name, show_shapes=True)

    def fixAutoEncoderWeights(self):
        self._timedistributeddense_1.trainable = False
        self._timedistributeddense_2.trainable = False
        self._timedistributeddense_3.trainable = False
        self._timedistributeddense_4.trainable = False
