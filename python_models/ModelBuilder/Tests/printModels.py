from ModelBuilder import model_architectures
from keras.utils.visualize_util import plot

batch_input_shape = [10, 120, 66]
model = model_architectures.getSRNN(batch_input_shape, W_regularizer_val=0.1, stateful=False, output_data_shape=66,
                                    spine_idx=[1, 10], l_arm_idx=[1, 10], r_arm_idx=[1, 10],
                                    l_leg_idx=[1, 10], r_leg_idx=[1, 10])
model.summary()
plot(model, to_file = 'srnn_model.png')