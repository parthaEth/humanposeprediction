#!/usr/bin/env python

# /*
#  * Created by Partha on 14/02/2017.
#  */


import keras
import keras.backend as K
from keras.layers import LSTM, Dropout, TimeDistributed, Input, Lambda, merge, Activation, Dense
from keras.regularizers import l2
from keras.constraints import maxnorm
from keras.models import Model

def getStackedModel(network_names, batch_input_shape, W_regularizer_val, stateful, output_data_shape, holden_dataset,
                    input_dropout_rate, classifier_rnn, spine_idx, l_arm_idx,
                    r_arm_idx, l_leg_idx, r_leg_idx):
    I = Input(batch_shape=batch_input_shape, dtype='float32', name='current_pose_input')
    out = Lambda(lambda x: K.dropout(x, level=input_dropout_rate))(I)
    for curren_net in network_names:
        if curren_net == 'SRNN':
            out = getSRNN(batch_input_shape, W_regularizer_val, stateful, output_data_shape,
                          spine_idx, l_arm_idx, r_arm_idx, l_leg_idx, r_leg_idx, input_dropout_rate=0, input=out)
        elif curren_net == 'ERD':
            out = getERD(batch_input_shape, W_regularizer_val, stateful, output_data_shape, holden_dataset,
                         input_dropout_rate=0, input=out)
        elif curren_net == 'AutoEncoderLSTM':
            out = getDropoutAutoEncoderLSTM(batch_input_shape, W_regularizer_val, stateful, output_data_shape,
                                            holden_dataset, input_dropout_rate=0, input=out)
        elif curren_net == 'LSTM3LR':
            out = getLSTM3LR(batch_input_shape, W_regularizer_val, stateful, output_data_shape, classifier_rnn,
                             holden_dataset, input_dropout_rate=0, input=out)
        elif curren_net == 'AutoencoderOnly':
            out = getAutoencoderOnly(batch_input_shape, output_data_shape, input_dropout_rate=0, input=out)
        else:
            print "Fatal error : Requested network - " + curren_net + " have not been implemented"
            exit(0)

    model = Model(input=I, output=out)

    return model

def getAutoencoderOnly(batch_input_shape, output_data_shape, input_dropout_rate, input=""):
    if input == "":
        I = Input(batch_shape=batch_input_shape, dtype='float32', name='current_pose_input')
    else:
        I = input
    
    _intermediate_out = Lambda(lambda x: K.dropout(x, level=input_dropout_rate))(I)

    _intermediate_out = TimeDistributed(Dense(3000, activation='relu', W_constraint=maxnorm(3)),
                                        batch_input_shape=batch_input_shape,
                                        name='timedistributeddense_1')(_intermediate_out)

    _intermediate_out = Dropout(0.001, batch_input_shape=batch_input_shape)(_intermediate_out)

    _intermediate_out = TimeDistributed(Dense(3000, activation='relu', W_constraint=maxnorm(3)),
                                        batch_input_shape=batch_input_shape,
                                        name='timedistributeddense_2')(_intermediate_out)

    _intermediate_out = Dropout(0.001, batch_input_shape=batch_input_shape)(_intermediate_out)

    _intermediate_out = TimeDistributed(Dense(3000, activation='relu', W_constraint=maxnorm(3)),
                                        batch_input_shape=batch_input_shape,
                                        name='timedistributeddense_3')(_intermediate_out)

    _intermediate_out = Dropout(0.001, batch_input_shape=batch_input_shape)(_intermediate_out)

    _intermediate_out = TimeDistributed(Dense(output_data_shape, activation='relu', W_constraint=maxnorm(3)),
                                        batch_input_shape=batch_input_shape,
                                        name='timedistributeddense_4')(_intermediate_out)
    if input == "":
        return Model(input=I, output=_intermediate_out)
    else:
        return _intermediate_out


def getERD(batch_input_shape, W_regularizer_val, stateful, output_data_shape, holden_dataset, input_dropout_rate,
           input=""):

    if input == "":
        I = Input(batch_shape=batch_input_shape, dtype='float32', name='current_pose_input')
    else:
        I = input

    _intermediate = Lambda(lambda x: K.dropout(x, level=input_dropout_rate))(I)
    _intermediate = TimeDistributed(Dense(3000, activation='relu', W_constraint=maxnorm(3)),
                                          batch_input_shape=batch_input_shape,
                                          name='timedistributeddense_1')(_intermediate)

    # model.add(Dropout(0.5, batch_input_shape=batch_input_shape))
    # model.add(Lambda(lambda x: K.dropout(x, level=0.05)))
    _intermediate = TimeDistributed(Dense(3000, activation='relu', W_constraint=maxnorm(3)),
                                    batch_input_shape=batch_input_shape, name='timedistributeddense_2')(_intermediate)

    # model.add(Dropout(0.1, batch_input_shape=batch_input_shape))
    # model.add(Lambda(lambda x: K.dropout(x, level=0.05)))
    _intermediate = TimeDistributed(Dense(3000, activation='relu', W_constraint=maxnorm(3)),
                                    batch_input_shape=batch_input_shape, name='timedistributeddense_3')(_intermediate)

    if holden_dataset:
        _intermediate = TimeDistributed(Dense(output_data_shape, activation='relu', W_constraint=maxnorm(3)),
                                        batch_input_shape=batch_input_shape,
                                        name='timedistributeddense_4')(_intermediate)

    _intermediate = LSTM(1000, return_sequences=True, inner_init='orthogonal',
                     batch_input_shape=batch_input_shape, stateful=stateful,
                     W_regularizer=l2(W_regularizer_val), name='lstm_in_1')(_intermediate)

    _intermediate = LSTM(1000, return_sequences=True, inner_init='orthogonal',
                         batch_input_shape=batch_input_shape, stateful=stateful,
                         W_regularizer=l2(W_regularizer_val), name='lstm_2')(_intermediate)

    _intermediate = LSTM(1000, return_sequences=True, inner_init='orthogonal',
                         batch_input_shape=batch_input_shape, stateful=stateful,
                         W_regularizer=l2(W_regularizer_val), name='lstm_6')(_intermediate)
    # if count > 1:
    # model.add(Dropout(0.5, batch_input_shape=batch_input_shape))
    #
    # model.add(TimeDistributedDense(layer_size, batch_input_shape=batch_input_shape, activation='relu',
    #                                      W_constraint=maxnorm(3)))
    # count += 1

    _intermediate = TimeDistributed(Dense(3000, activation='relu', W_constraint=maxnorm(3)),
                                    batch_input_shape=batch_input_shape, name='decoder_0')(_intermediate)

    # model.add(Dropout(0.5, batch_input_shape=batch_input_shape))

    _intermediate=  TimeDistributed(Dense(3000, activation='relu', W_constraint=maxnorm(3)),
                                    batch_input_shape=batch_input_shape, name='decoder_6')(_intermediate)

    # model.add(Dropout(0.5, batch_input_shape=batch_input_shape))

    if holden_dataset:
        _intermediate = TimeDistributed(Dense(3000, activation='relu', W_constraint=maxnorm(3)),
                                        batch_input_shape=batch_input_shape, name='decoder_7')(_intermediate)

    # model.add(Dropout(0.25, batch_input_shape=batch_input_shape))

    _intermediate = TimeDistributed(Dense(output_data_shape, activation='relu', W_constraint=maxnorm(3)),
                                    batch_input_shape=batch_input_shape, name='decoder_2')(_intermediate)

    if input == "":
        return Model(input=I, output=_intermediate)
    else:
        return _intermediate

def getLSTM3LR(batch_input_shape, W_regularizer_val, stateful, output_data_shape, classifier_rnn, holden_dataset,
               input_dropout_rate, input=""):
    recurrent_dopout_rate = 0.0
    if input == "":
        I = Input(batch_shape=batch_input_shape, dtype='float32', name='current_pose_input_test')
    else:
        I = input

    _intermediate_out = Lambda(lambda x: K.dropout(x, level=input_dropout_rate))(I)

    lstm_1 = LSTM(1000, return_sequences=True, inner_init='orthogonal',
                   batch_input_shape=batch_input_shape, stateful=stateful,
                   W_regularizer=l2(W_regularizer_val), name='lstm_1')(_intermediate_out)

    lstm_2 = LSTM(1000, return_sequences=True, inner_init='orthogonal',
                   batch_input_shape=batch_input_shape, stateful=stateful,
                   W_regularizer=l2(W_regularizer_val), name='lstm_2')(lstm_1)

    if holden_dataset:
        lstm_2 = LSTM(1000, return_sequences=True, inner_init='orthogonal',
                       batch_input_shape=batch_input_shape, stateful=stateful,
                       W_regularizer=l2(W_regularizer_val), name='lstm_6')(lstm_2)

    predicted_pose = LSTM(output_data_shape, return_sequences=True, inner_init='orthogonal',
                          batch_input_shape=batch_input_shape, stateful=stateful,
                          W_regularizer=l2(W_regularizer_val), name='lstm_3')(lstm_2)

    if classifier_rnn:
        lstm_4 = LSTM(1000, return_sequences=True, inner_init='orthogonal',
                      batch_input_shape=batch_input_shape, stateful=stateful,
                      W_regularizer=l2(W_regularizer_val), dropout_W=recurrent_dopout_rate, 
                      dropout_U=recurrent_dopout_rate, name='lstm_4')(lstm_2)   

        lstm_5 = LSTM(1000, return_sequences=True, inner_init='orthogonal',
                      batch_input_shape=batch_input_shape, stateful=stateful,
                      W_regularizer=l2(W_regularizer_val), dropout_W=recurrent_dopout_rate,
                      dropout_U=recurrent_dopout_rate, name='lstm_5')(lstm_4)

        # class_labels = LSTM(15, return_sequences=True, inner_init='orthogonal',
        #                    batch_input_shape=batch_input_shape, stateful=stateful,
        #                    W_regularizer=l2(W_regularizer_val), name='lstm_7')(lstm_5)i
        lstm_5 = Dropout(0.98, batch_input_shape=batch_input_shape)(lstm_5)
        # Now predicting unnatural calss along with action class.
        class_labels = TimeDistributed(Dense(15, activation='relu', W_constraint=maxnorm(3)),
                                       batch_input_shape=batch_input_shape)(lstm_5)

        class_labels = Activation('softmax', name='ActionClass')(class_labels)

        output = [class_labels, predicted_pose]
    else:
        output = predicted_pose

    if input == "":
        return Model(input=I, output=output)
    else:
        return output

def getDropoutAutoEncoderLSTM(batch_input_shape, W_regularizer_val, stateful, output_data_shape, holden_dataset,
                              input_dropout_rate, input=""):

    if input == "":
        I = Input(batch_shape=batch_input_shape, dtype='float32', name='current_pose_input')
    else:
        I = input

    _intermediate_out = Lambda(lambda x: K.dropout(x, level=input_dropout_rate))(I)

    _intermediate_out = TimeDistributed(Dense(3000, activation='relu', W_constraint=maxnorm(3)),
                                        batch_input_shape=batch_input_shape,
                                        name='timedistributeddense_1')(_intermediate_out)

    # _intermediate_out = Dropout(0.08, batch_input_shape=batch_input_shape)(_intermediate_out)

    _intermediate_out = TimeDistributed(Dense(3000, activation='relu', W_constraint=maxnorm(3)),
                                        batch_input_shape=batch_input_shape,
                                        name='timedistributeddense_2')(_intermediate_out)

    # _intermediate_out = Dropout(0.01, batch_input_shape=batch_input_shape)(_intermediate_out)

    _intermediate_out = TimeDistributed(Dense(3000, activation='relu', W_constraint=maxnorm(3)),
                                        batch_input_shape=batch_input_shape,
                                        name='timedistributeddense_3')(_intermediate_out)

    # _intermediate_out = Dropout(0.05, batch_input_shape=batch_input_shape)(_intermediate_out)

    _intermediate_out = TimeDistributed(Dense(output_data_shape, activation='relu', W_constraint=maxnorm(3)),
                                        batch_input_shape=batch_input_shape,
                                        name='timedistributeddense_4')(_intermediate_out)

    _intermediate_out1 = Dropout(0.001, batch_input_shape=batch_input_shape)(_intermediate_out)
    _intermediate_out1 = keras.layers.noise.GaussianNoise(0.005)(_intermediate_out1)

    recurrent_part = LSTM(1000, return_sequences=True, inner_init='orthogonal',
                   batch_input_shape=batch_input_shape, stateful=stateful,
                   W_regularizer=l2(W_regularizer_val), name="lstm_1")(_intermediate_out1)

    recurrent_part = LSTM(1000, return_sequences=True, inner_init='orthogonal',
                   batch_input_shape=batch_input_shape, stateful=stateful,
                   W_regularizer=l2(W_regularizer_val), name="lstm_2")(recurrent_part)

    if holden_dataset:
        recurrent_part = LSTM(1000, return_sequences=True, inner_init='orthogonal',
                       batch_input_shape=batch_input_shape, stateful=stateful,
                       W_regularizer=l2(W_regularizer_val), name="lstm_6")(recurrent_part)

    recurrent_part = LSTM(output_data_shape, return_sequences=True, batch_input_shape=batch_input_shape,
                   inner_init='orthogonal', stateful=stateful,
                   W_regularizer=l2(W_regularizer_val), name="lstm_3")(recurrent_part)

    output=[recurrent_part, _intermediate_out]

    if input == "":
        return Model(input=I, output=output)
    else:
        return output


def getSRNN(batch_input_shape, W_regularizer_val, stateful, output_data_shape,
            spine_idx, l_arm_idx, r_arm_idx, l_leg_idx, r_leg_idx, input_dropout_rate, input=""):

    recurrent_dopout_rate = 0.0

    def getNodeRnn(node_name, batch_input_shape, out_shape, stateful):
        node_1 = LSTM(512, return_sequences=True, inner_init='orthogonal',
                    batch_input_shape=batch_input_shape, stateful=stateful,
                    W_regularizer=l2(W_regularizer_val), dropout_W=recurrent_dopout_rate,
                    dropout_U=recurrent_dopout_rate, name="LSTM_" + node_name)
        node_2 = TimeDistributed(Dense(256, activation='relu', W_constraint=maxnorm(300)),
                                 batch_input_shape=batch_input_shape, name="FC_" + node_name)
        node_3 = TimeDistributed(Dense(256, activation='relu', W_constraint=maxnorm(300)),
                                 batch_input_shape=batch_input_shape, name="FC_" + node_name + '1')

        node_d_1 = Dropout(0.0, batch_input_shape=batch_input_shape)

        node_4 = TimeDistributed(Dense(100, activation='relu', W_constraint=maxnorm(300)),
                                 batch_input_shape=batch_input_shape, name="FC_" + node_name + '2')

        node_d_2 = Dropout(0.0, batch_input_shape=batch_input_shape)

        node_5 = TimeDistributed(Dense(out_shape, activation='relu', W_constraint=maxnorm(300)),
                                 batch_input_shape=batch_input_shape, name="FC_" + node_name + '3')

        # node = Dropout(5e-1, batch_input_shape=batch_input_shape)(node)
        #
        # node = TimeDistributedDense(out_shape, batch_input_shape=batch_input_shape, activation='relu',
        #                      W_constraint=maxnorm(3), name="FC_" + node_name + '4')(node)

        return [node_1, node_2, node_3, node_d_1, node_4, node_d_2, node_5]

    def getEdgeRnn(edge_name, batch_input_shape, stateful):
        edge_1 = TimeDistributed(Dense(256, activation='relu', W_constraint=maxnorm(300)),
                                 batch_input_shape=batch_input_shape, name="FC_" + edge_name)

        edge_d_1 = Dropout(0.0, batch_input_shape=batch_input_shape)

        edge_2 = TimeDistributed(Dense(256, activation='relu', W_constraint=maxnorm(300)),
                                 batch_input_shape=batch_input_shape, name="FC_" + edge_name + '1')

        edge_3 = LSTM(512, return_sequences=True, inner_init='orthogonal',
                    batch_input_shape=batch_input_shape, stateful=stateful,
                    W_regularizer=l2(W_regularizer_val), name="LSTM_" + edge_name)

        # edge = Dropout(1e-3, batch_input_shape=batch_input_shape)(edge)

        return [edge_1, edge_d_1, edge_2, edge_3]

    # def connectWithInputOutputSkipConnection(layers, input):
    #     out = input
    #     hidden_layer_outs = []
    #     for layer_idx in range(len(layers) - 1):
    #         out = layers[layer_idx](out)
    #         if not layers[layer_idx + 1].name.find('dropout') >= 0:
    #             hidden_layer_outs.append(out)
    #             out = merge([out, input], mode='concat', concat_axis=-1)
    #
    #     hidden_layer_outs.append(out)
    #
    #     out = layers[-1](merge(hidden_layer_outs, mode='concat', concat_axis=-1))
    #     return out

    def connectWithInputOutputSkipConnection(layers, input, batch_input_shape):
        out = input
        hidden_layer_outs = []
        for layer_idx in range(len(layers) - 1):
            out = layers[layer_idx](out)
            if not layers[layer_idx + 1].name.find('dropout') >= 0:
                hidden_layer_outs.append(out)
                out = merge([out, TimeDistributed(Dense(layers[layer_idx].output_shape[-1], activation='relu'),
                                                  batch_input_shape = batch_input_shape)(input)], mode='sum', concat_axis=-1)

        out = layers[-1](out)
        for hidden_out_idx in range(len(hidden_layer_outs)):
            hidden_layer_outs[hidden_out_idx] = \
                TimeDistributed(Dense(layers[-1].output_shape[-1], activation='relu'),
                                batch_input_shape=batch_input_shape)(hidden_layer_outs[hidden_out_idx])

        hidden_layer_outs.append(out)
        out = merge(hidden_layer_outs, mode='sum', concat_axis=-1)

        return out

    def selectIndices(x, indices):
        out = x[:, :, 0:1]
        for i in range(len(indices)-1):
            out = K.concatenate([out, x[:, :, indices[i + 1]:indices[i + 1] + 1]], axis=-1)
        return out
        # return x[:, :, K.variable(indices, dtype='int32')]

    def getReverseMap(forward_map):
        reverse_map = []
        for i in range(66):
            for j in range(len(forward_map)):
                if i == forward_map[j]:
                    reverse_map.append(j)
                    break

        return reverse_map

    if input == "":
        I = Input(batch_shape=batch_input_shape, dtype='float32', name='current_pose_input')
    else:
        I = input

    _intermediate_out = Lambda(lambda x: K.dropout(x, level=input_dropout_rate))(I)

    samples = batch_input_shape[0]
    time_steps = batch_input_shape[1]
    left_arm_feature_num = len(l_arm_idx)
    right_arm_feature_num = len(r_arm_idx)

    if left_arm_feature_num != right_arm_feature_num:
        print "Fatal ERROR: Both hands have to have same number of features associated"
        exit(16)

    left_leg_feature_num = len(l_leg_idx)
    right_leg_feature_num = len(r_leg_idx)

    if left_leg_feature_num != right_leg_feature_num:
        print "Fatal ERROR: Both legs have to have same number of features associated"
        exit(17)

    spine_feature_num = len(spine_idx)

    # Node inputs
    left_arm_in = Lambda(selectIndices, arguments={'indices': l_arm_idx}, name='l_arm')(_intermediate_out)
    right_arm_in = Lambda(selectIndices, arguments={'indices': l_arm_idx}, name='r_arm')(_intermediate_out)
    left_leg_in = Lambda(selectIndices, arguments={'indices': l_leg_idx}, name='l_leg')(_intermediate_out)
    right_leg_in = Lambda(selectIndices, arguments={'indices': r_leg_idx}, name='r_leg')(_intermediate_out)
    spine_in = Lambda(selectIndices, arguments={'indices': spine_idx}, name='Spine')(_intermediate_out)

    # left_arm_in = TimeDistributedDense(left_arm_feature_num, batch_input_shape=batch_input_shape, activation='relu',
    #                                          W_constraint=maxnorm(3))(I)
    # right_arm_in = TimeDistributedDense(right_arm_feature_num, batch_input_shape=batch_input_shape, activation='relu',
    #                                          W_constraint=maxnorm(3))(I)
    # left_leg_in = TimeDistributedDense(left_leg_feature_num, batch_input_shape=batch_input_shape, activation='relu',
    #                                          W_constraint=maxnorm(3))(I)
    # right_leg_in = TimeDistributedDense(left_leg_feature_num, batch_input_shape=batch_input_shape, activation='relu',
    #                                          W_constraint=maxnorm(3))(I)
    # spine_in = TimeDistributedDense(spine_feature_num, batch_input_shape=batch_input_shape, activation='relu',
    #                                          W_constraint=maxnorm(3))(I)

    # Node RNNs
    spine_node = getNodeRnn('spine', batch_input_shape=[samples, time_steps, spine_feature_num + 512 * 3],
                            out_shape=spine_feature_num, stateful=stateful)
    arm_node = getNodeRnn('arm', batch_input_shape=[samples, time_steps, left_arm_feature_num + 512 * 3],
                          out_shape=left_arm_feature_num, stateful=stateful)
    leg_node = getNodeRnn('leg', batch_input_shape=[samples, time_steps, right_leg_feature_num + 512 * 2],
                          out_shape=right_leg_feature_num, stateful=stateful)

    # Edge RNNs
    l_arm_r_arm_edge = getEdgeRnn('l_arm_r_arm_edge',
                                  batch_input_shape=[samples, time_steps, left_arm_feature_num*2], stateful=stateful)
    l_leg_r_leg_edge = getEdgeRnn('l_leg_r_leg_edge',
                                  batch_input_shape=[samples, time_steps, left_leg_feature_num * 2], stateful=stateful)
    leg_spine_edge = getEdgeRnn('leg_spine_edge',
                                batch_input_shape=[samples, time_steps, left_leg_feature_num + spine_feature_num],
                                stateful=stateful)
    arm_spine_edge = getEdgeRnn('arm_spine_edge',
                                batch_input_shape=[samples, time_steps, left_arm_feature_num + spine_feature_num],
                                stateful=stateful)
    spine_spine_edge = getEdgeRnn('spine_spine_edge',
                                  batch_input_shape=[samples, time_steps, spine_feature_num], stateful=stateful)
    arm_arm_edge = getEdgeRnn('arm_arm_edge', batch_input_shape=[samples, time_steps, left_arm_feature_num],
                              stateful=stateful)
    leg_leg_edge = getEdgeRnn('leg_leg_edge', batch_input_shape=[samples, time_steps, left_leg_feature_num],
                              stateful=stateful)

    # Connecting everything together
    # edge_connections
    spine_spine_edge = connectWithInputOutputSkipConnection(spine_spine_edge, spine_in, batch_input_shape)
    l_arm_l_arm_edge = connectWithInputOutputSkipConnection(arm_arm_edge, left_arm_in, batch_input_shape)
    r_arm_r_arm_edge = connectWithInputOutputSkipConnection(arm_arm_edge, right_arm_in, batch_input_shape)
    l_arm_r_arm_edge = \
        connectWithInputOutputSkipConnection(l_arm_r_arm_edge,
                                             merge([left_arm_in, right_arm_in], mode='concat', concat_axis=-1),
                                             batch_input_shape)
    l_leg_l_leg_edge = connectWithInputOutputSkipConnection(leg_leg_edge, left_leg_in, batch_input_shape)
    r_leg_r_leg_edge = connectWithInputOutputSkipConnection(leg_leg_edge, right_leg_in, batch_input_shape)
    l_leg_r_leg_edge = \
        connectWithInputOutputSkipConnection(l_leg_r_leg_edge,
                                             merge([left_leg_in, right_leg_in], mode='concat', concat_axis=-1),
                                             batch_input_shape)
    spine_l_arm_edge = \
        connectWithInputOutputSkipConnection(arm_spine_edge,
                                             merge([spine_in, left_arm_in], mode='concat', concat_axis=-1),
                                             batch_input_shape)
    spine_r_arm_edge = \
        connectWithInputOutputSkipConnection(arm_spine_edge,
                                             merge([spine_in, right_arm_in], mode='concat', concat_axis=-1),
                                             batch_input_shape)
    spine_l_leg_edge = \
        connectWithInputOutputSkipConnection(leg_spine_edge,
                                             merge([spine_in, left_leg_in], mode='concat', concat_axis=-1),
                                             batch_input_shape)
    spine_r_leg_edge = \
        connectWithInputOutputSkipConnection(leg_spine_edge,
                                             merge([spine_in, right_leg_in], mode='concat', concat_axis=-1),
                                             batch_input_shape)


    # node connections
    left_arm = \
        connectWithInputOutputSkipConnection(arm_node,
                                             merge([left_arm_in, spine_l_arm_edge, l_arm_l_arm_edge, l_arm_r_arm_edge],
                                                   mode='concat', concat_axis=-1), batch_input_shape)
    right_arm = connectWithInputOutputSkipConnection(arm_node, merge([right_arm_in, spine_r_arm_edge, r_arm_r_arm_edge,
                                                                      l_arm_r_arm_edge], mode='concat', concat_axis=-1),
                                                     batch_input_shape)

    both_hand = merge([spine_l_arm_edge, spine_r_arm_edge], mode='sum', concat_axis=-1)
    both_legs = merge([spine_l_leg_edge, spine_r_leg_edge], mode='sum', concat_axis=-1)
    spine = connectWithInputOutputSkipConnection(spine_node, merge([spine_in, spine_spine_edge, both_hand, both_legs],
                                                                    mode='concat', concat_axis=-1), batch_input_shape)

    left_leg = connectWithInputOutputSkipConnection(leg_node, merge([left_leg_in, l_leg_l_leg_edge, l_leg_r_leg_edge],
                                                                     mode='concat', concat_axis=-1), batch_input_shape)
    right_leg = connectWithInputOutputSkipConnection(leg_node, merge([right_leg_in, r_leg_r_leg_edge, l_leg_r_leg_edge],
                                                                      mode='concat', concat_axis=-1), batch_input_shape)

    output = merge([spine, left_arm, right_arm, left_leg, right_leg], mode='concat', concat_axis=-1)

    # This order hs to match the previous merge order
    forward_map = spine_idx + l_arm_idx + r_arm_idx + l_leg_idx + r_leg_idx
    reverse_map = getReverseMap(forward_map)
    output = Lambda(selectIndices, arguments={'indices': reverse_map}, name='Output_rearrange')(output)

    if input == "":
        return Model(input=I, output=output)
    else:
        return output
