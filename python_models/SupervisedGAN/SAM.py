#!/usr/bin/env python

# /*
#  * Created by Partha on 09/02/2017.
#  */

#TODO(Partha): Derive from a models base class almost all the models share something!

import numpy

from keras.layers import TimeDistributedDense, LSTM, Dropout, Input
from keras.models import Model
from keras.constraints import maxnorm
from keras.models import Sequential
from keras.regularizers import l2
import gflags
import glog as log

FLAGS = gflags.FLAGS
gflags.DEFINE_list('list_generator_autoenc_neurons', ['3000', '3000', '3000', '52'],
                   'Number of neurons as comma seperated vals')
gflags.DEFINE_list('list_generator_neurons', ['1000', '1000'], 'Number of neurons as comma seperated vals')
gflags.DEFINE_list('list_discriminator_neurons', ['1000', '1000', '1000', '1000'], 'Number of neurons as comma seperated vals')

gflags.DEFINE_boolean('discreminator_only', False, 'Wheather to build the supervised GAN model or just the '
                                                   'discriminator')
gflags.DEFINE_boolean('generator_only', False, 'Wheather to build the supervised GAN model or just the generator')

gflags.DEFINE_float('W_regularizer_val', 0.00001, 'l2 regularization applied to all layers')

# TODO(Partha): Move these to utility package
def parseToListOfInts(list_encoder_neurons):
    layer_sizes = []
    for num_neurons in list_encoder_neurons:
        layer_sizes.append(int(num_neurons))

    return layer_sizes

def parseToListOfFloats(list_decimal_vals_as_string):
    list_decimal_vals = []
    for num_neurons in list_decimal_vals_as_string:
        list_decimal_vals.append(float(num_neurons))

    return list_decimal_vals


class SupervisedAdversarial:
    def __init__(self, data_dim, batch_size, seq_length, stateful, optimizer, loss):
        self._trainable_length = seq_length - 1
        if stateful:
            batch_input_shape = (batch_size, self._trainable_length, data_dim)
        else:
            batch_input_shape = (None, self._trainable_length, data_dim)


        self._batch_size = batch_size
        self._stateful = stateful

        if FLAGS.generator_only and FLAGS.discreminator_only:
            log.fatal("Conflicting settings. Requesting both generator only and discriminator only architecture.")
            exit(0)

        g_input = Input(batch_shape=batch_input_shape, dtype='float32', name='current_pose_input')

        if not FLAGS.discreminator_only:
            # Construct the generator
            layer_sizes = parseToListOfInts(FLAGS.list_generator_neurons)
            auto_enc_layer_sizes = parseToListOfInts(FLAGS.list_generator_autoenc_neurons)

            count = 0
            hg = g_input
            for current_layer_size in auto_enc_layer_sizes:
                hg = TimeDistributedDense(current_layer_size, batch_input_shape=batch_input_shape, activation='relu',
                                          W_constraint=maxnorm(3), name='genn_dense' + str(count))(hg)
                count += 1

            for current_layer_size in layer_sizes:
                hg = LSTM(current_layer_size, return_sequences=True, inner_init='orthogonal',
                          batch_input_shape=batch_input_shape, stateful=self._stateful,
                          W_regularizer=l2(FLAGS.W_regularizer_val), name='generator' + str(count))(hg)
                count += 1

            hg = LSTM(data_dim, return_sequences=True, inner_init='orthogonal',
                      batch_input_shape=batch_input_shape, stateful=self._stateful,
                      W_regularizer=l2(FLAGS.W_regularizer_val), name='generator' + str(count))(hg)

            self._generator = Model(g_input, hg, name='predicted_pose_out')
            self._generator.compile(optimizer=optimizer, loss=loss[0])
            self._generator.summary()

        # Construct the discriminator
        layer_sizes = parseToListOfInts(FLAGS.list_discriminator_neurons)

        if not FLAGS.generator_only:
            layer_names = ['lstm_1', 'lstm_2', 'lstm_4', 'lstm_5']
            count = 0
            d_input = Input(batch_shape=batch_input_shape, dtype='float32', name='current_pose_input')
            hd = d_input
            for current_layer_size in layer_sizes:
                hd = LSTM(current_layer_size, return_sequences=True, inner_init='orthogonal',
                          batch_input_shape=batch_input_shape, stateful=self._stateful,
                          W_regularizer=l2(FLAGS.W_regularizer_val), name=layer_names[count])(hd)
                count += 1

            hd = Dropout(0.98, batch_input_shape=batch_input_shape)(hd)

            hd = TimeDistributedDense(1, activation='sigmoid', batch_input_shape=batch_input_shape,
                                      name='categorical_output')(hd)
            self._discriminator = Model(d_input, hd, name='categorical_output')
            self._discriminator.compile(optimizer=optimizer, loss=loss[1], metrics=['accuracy'])
            self._discriminator.summary()

        if not (FLAGS.discreminator_only or FLAGS.generator_only):
            # Construct the SupervisedAdversarialModel
            sam_input = Input(batch_shape=batch_input_shape, dtype='float32', name='current_pose_input')
            predicted_pose_out = self._generator(sam_input)
            categorical_output = self._discriminator(predicted_pose_out)
            self._supervised_adversarial_model = Model(sam_input, output=[predicted_pose_out, categorical_output])
            self._supervised_adversarial_model.compile(optimizer=optimizer,
                loss={'predicted_pose_out': loss[0], 'categorical_output': loss[1]}, metrics=['accuracy'],
                loss_weights=[0.9, 0.1])
            self._supervised_adversarial_model.summary()

    @staticmethod
    def makeTrainable(network, trainable_flag):
        for layer_idx in range(len(network.layers)):
            network.layers[layer_idx].trainable = trainable_flag
        network.compile(loss=network.loss, optimizer=network.optimizer, metrics=network.metrics)

    def trainDiscreminatorOnly(self, x_train, ytrain, nb_epochs, validation_split, callbacks):
        """Train the discreminator network alone. Can be used both when using only this network and in the
        discriminator update phase"""
        print "Training Discriminator"
        self.makeTrainable(self._discriminator, True)
        self._discriminator.fit(x_train, ytrain, shuffle=True, nb_epoch=nb_epochs, callbacks=callbacks,
                                batch_size=self._batch_size, validation_split=validation_split, verbose=1)

    def pretrainGenerator(self, x_train, nb_epoch, validation_split, callbacks):
        """Note that the generator predicts the one time step ahead version of input so there is no provission to
        specify target, y_train. n batches of data must be present in x_trin, where n can rnage from 1 to any
        arbitrary integer"""
        print "Training Generator"
        self.makeTrainable(self._generator, True)
        self._generator.fit(x_train[:, 0:-1, :], x_train[:, 1:, :], shuffle=True, nb_epoch=nb_epoch,
                            callbacks=callbacks, batch_size=self._batch_size, 
                            validation_split=validation_split, verbose=1)

    def trainGeneratorInGANSetting(self, x_train, validation_split, callbacks):
        """In this function the discriminator is fixed but the generator is allowed to change"""
        print "Training Generator in GAN Style"
        self.makeTrainable(self._discriminator, False)
        self.makeTrainable(self._generator, True)
        label_shape = [x_train.shape[0], x_train.shape[1] - 1, 1]
        self._supervised_adversarial_model.fit(x_train[:, 0:-1, :], [x_train[:, 1:, :], numpy.ones(label_shape)],
                                               shuffle=True, nb_epoch=1, callbacks=callbacks,
                                               batch_size=1, validation_split=validation_split, verbose=1)

    # def trainDiscreminatorInGANSetting(self, x_train, validation_split, callbacks):
    #     _____________________________THIS FUNCTION ISNOT NEEDED _____________________________
    #     """In this function the discriminator is fixed but the generator is allowed to change"""
    #     self.makeTrainable(self._discriminator, True)
    #     self.makeTrainable(self._generator, False)
    #     label_shape = [x_train.shape[0], x_train.shape[1] - 1, 1]
    #     self._supervised_adversarial_model.fit(x_train[:, 0:-1, :], [x_train[:, 1:, :], numpy.zeros(label_shape)],
    #                                            shuffle=True, nb_epoch=1, callbacks=callbacks,
    #                                            batch_size=self._batch_size, validation_split=validation_split)

    def getGeneratedTrainingExamples(self, x):
        return self._generator.predict(x)

    def predict(self, test_data, seed_seq_length, num_predicted_time_steps, output_post_processor):
        """test_data must have the shape (1, num_time_steps, features)"""
        log.check_ge(test_data.shape[1], seed_seq_length, "Requested seed is longer than data available test_data.shape"
                                                          " = " + str(test_data.shape) + " requested sseed length = " +
                                                          str(seed_seq_length))
        for time_step in range(seed_seq_length):
            # seed here
            predicted_seq = \
                self._generator.predict(test_data[0:1, time_step:time_step + 1, :])
            # output_post_processor.accumulateTimeseriesRow(test_data[seq_ind, mini_seq_time_indx, :])
            output_post_processor.accumulateTimeseriesRow(predicted_seq[0, :, :])

        for i in range(num_predicted_time_steps):
            predicted_seq = self._generator.predict(predicted_seq)
            output_post_processor.accumulateTimeseriesRow(predicted_seq[0, :, :])

    def saveGenerator(self, model_name):
        self._generator.save_weights(model_name)

    def saveDiscriminator(self, model_name):
        self._discriminator.save_weights(model_name)

    def loadGenerator(self, model_name, by_name):
        self._generator.load_weights(model_name, by_name)

    def loadDiscriminator(self, model_name, by_name):
        self._discriminator.load_weights(model_name, by_name)

    def loadWeights(self, model_name, by_name):
        if not FLAGS.discreminator_only:
            self.loadGenerator(model_name + str('_generator.h5'), by_name)

        if not FLAGS.generator_only:
            self.loadDiscriminator(model_name + str('_discriminator.h5'), by_name=True)
           # for i in range(6):
           #     print "Freezing layer" + self._discriminator.layers[i].name
           #     self._discriminator.layers[i].trainable = False
           # self._discriminator.compile(loss=self._discriminator.loss, optimizer=self._discriminator.optimizer,
           #                             metrics=self._discriminator.metrics)
           # print "compiled with " + str(self._discriminator.metrics)

    def saveWeights(self, model_name):
        self.saveGenerator(model_name + str('_generator.h5'))
        self.saveDiscriminator(model_name + str('_discriminator.h5'))

    def visualizeModel(self, output_file_name):
        from keras.utils.visualize_util import plot
        if FLAGS.discreminator_only:
            plot(self._discriminator, to_file=output_file_name + '_discriminator.png')
        if FLAGS.generator_only:
            plot(self._discriminator, to_file=output_file_name + '_generator.png')
        else:
            plot(self._supervised_adversarial_model, to_file=output_file_name)
            plot(self._discriminator, to_file=output_file_name + '_generator.png')
            plot(self._discriminator, to_file=output_file_name + '_discriminator.png')

    def writePredictionsToFile(self, output_post_processor, dest_dir="", filename=""):
        output_post_processor.dumpRescaledAndResizedVals(filename, dest_dir)

    def resetModel(self):
        if self._stateful:
            self._generator.reset_states()
        else:
            RuntimeError("Reset state is called on a non stateful RNN")
