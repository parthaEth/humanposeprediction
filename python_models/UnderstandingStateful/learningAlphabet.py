#!/usr/bin/env python

# /*
#  * Created by Partha on 11/11/2016.
#  */

# Stateful LSTM to learn one-char to one-char mapping
import numpy
import os
from keras.models import load_model
from keras.models import Sequential
from keras.layers import Dense
from keras.layers import LSTM
from keras.utils import np_utils
import sys


# fix random seed for reproducibility
numpy.random.seed(7)
replicate_times = 2
# define the raw dataset
alphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
# create mapping of characters to integers (0-25) and the reverse
char_to_int = dict((c, i) for i, c in enumerate(alphabet))
int_to_char = dict((i, c) for i, c in enumerate(alphabet))

alphabet_copy = alphabet
for i in range(replicate_times - 1):
    alphabet_copy += alphabet


# prepare the dataset of input to output pairs encoded as integers
seq_length = 2
burn_in_length = 5
play_forward_time = 8

force_retraining = False

if len(sys.argv) <= 1:
    force_retraining = False
elif sys.argv[0]:
    force_retraining = True

if (not os.path.exists("./AlphabetModel.h5")) or force_retraining:
    dataX = []
    dataY = []

    for i in range(burn_in_length, replicate_times*len(alphabet) - seq_length, 1):
        seq_in = alphabet_copy[i:i + seq_length]
        seq_out = alphabet_copy[i + seq_length]
        dataX.append([char_to_int[char] for char in seq_in])
        dataY.append(char_to_int[seq_out])
        print seq_in, '->', seq_out

    # reshape X to be [samples, time steps, features]
    X = numpy.reshape(dataX, (len(dataX), seq_length, 1))

    # normalize
    X = X / float(len(alphabet))

    # one hot encode the output variable
    y = np_utils.to_categorical(dataY)

    # create and fit the model
    batch_size = 1
    model = Sequential()
    # model.add(LSTM(32, batch_input_shape=(batch_size, X.shape[1], X.shape[2]), stateful=True, return_sequences=True))
    model.add(LSTM(32, input_dim=X.shape[2], stateful=True, return_sequences=True))
    print [batch_size, X.shape[1], X.shape[2]]
    exit(0)
    model.add(LSTM(32, return_sequences=False))
    model.add(Dense(y.shape[1], activation='softmax'))
    model.compile(loss='categorical_crossentropy', optimizer='sgd', metrics=['accuracy'])

    for i in range(400):
        seed = [0]
        for j in range(burn_in_length):
            x = numpy.reshape(seed, (1, len(seed), 1))
            x = x / float(len(alphabet))
            prediction = model.predict(x, verbose=0)
            index = numpy.argmax(prediction)
            seed = [seed[0] + 1]
        model.fit(X, y, nb_epoch=2, batch_size=batch_size, verbose=2, shuffle=False)
        model.reset_states()

    model.save("AlphabetModel.h5", True)

    # summarize performance of the model
    scores = model.evaluate(X, y, batch_size=batch_size, verbose=0)
    model.reset_states()
    print("Model Accuracy: %.2f%%" % (scores[1]*100))
else:
    model = load_model("AlphabetModel.h5")
    model.summary()
# demonstrate some model predictions
seed = [char_to_int[alphabet[0]]]
for i in range(0, len(alphabet)-1):
    x = numpy.reshape(seed, (1, len(seed), 1))
    x = x / float(len(alphabet))
    prediction = model.predict(x, verbose=0)
    index = numpy.argmax(prediction)
    print int_to_char[seed[0]], "->", int_to_char[index]
    seed = [index]
model.reset_states()
# demonstrate a random starting point
letter = "N"
seed = [char_to_int[letter]]
print "New start: ", letter

seed = [seed[0] - burn_in_length];
for i in range(burn_in_length):
    x = numpy.reshape(seed, (1, len(seed), 1))
    x = x / float(len(alphabet))
    prediction = model.predict(x, verbose=0)
    index = numpy.argmax(prediction)
    print "seeding", int_to_char[seed[0]], "-> Produced", int_to_char[index]
    seed = [seed[0] + 1]

for i in range(0, play_forward_time):
    x = numpy.reshape(seed, (1, len(seed), 1))
    x = x / float(len(alphabet))
    prediction = model.predict(x, verbose=0)
    index = numpy.argmax(prediction)
    print int_to_char[seed[0]], "->", int_to_char[index]
    seed = [index]
model.reset_states()
