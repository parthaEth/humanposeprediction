clc
clear all
close all
% This function animates groundtruth and predicted sequence side by side or
% on top of each other for visual comparison

addpath('../../Matlab/ploting')
addpath('../../Matlab/')

% -------------- Settings --------------%
fps = 25; % This is just display fps
pose_seq = '18';
express_error_in_normalized_scale = true;
write_to_video_file = true; %set to true if an avi is desired
my_own_data = false;
is_expmap_data = true;
skip_first_n_frames = 2;
person = 1; % Whose skeleton to use. Data has been normalized with s1
R0 = eye(3);
T0 = [0 0 0];
% -------------- Settings --------------%

% file_name_with_path = ...
%     ['../GeneratedSequences/', pose_seq, '.csv'];
file_name_with_path = '1.csv';

[original_file_name, seq_start_row, ...
 seq_length, dwon_sampling_rate, FPS] = parseHeader(file_name_with_path);

generated_data = dlmread(file_name_with_path, ',', 9);

for i = 1:size(generated_data, 1)
    drawSkeleton(generated_data(i, :), i > seq_length)
%     pause(0.1)
end