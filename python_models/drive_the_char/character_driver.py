import sys
sys.path.append('../')

import matlab.engine
import numpy
import time
from inputGenerator import *
import keras
from LSTM_fr_pose import *
import gflags
from gflags import FLAGS
import glog as log
import getDesiredOrientation

eng = matlab.engine.start_matlab()

gflags.DEFINE_list('list_num_neurons', ['1000', '1000'], 'Number of neurons as comma seperated vals')

gflags.DEFINE_integer('seed_seq_length', 50, 'Seed sequence length to start off the character')
gflags.DEFINE_integer('down_sample_in_time_by_n', 2, 'Down sampling the original sequence in time by this factor')
gflags.DEFINE_integer('test_subject', 5, 'Style of this subject will be replicated')
gflags.DEFINE_integer('num_files', -1, 'Number of test style files to load')
gflags.DEFINE_integer('skip_seed_seqs', 17, 'Skip these many initial sequences')

gflags.DEFINE_string('file_type', '.txt', 'Input file type (.csv, .txt etc.)')
gflags.DEFINE_string('folder_path_to_data', '/home/partha/ait-ra-ship/Data', 'full path to where data is located')
FLAGS(sys.argv)

def parseToListOfInts(list_num_neurons):
    layer_sizes = []
    for num_neurons in list_num_neurons:
        layer_sizes.append(int(num_neurons))

    return layer_sizes

input_provider = InputProvider(1, 0.1, FLAGS.down_sample_in_time_by_n,
                               True, FLAGS.seed_seq_length,
                               FLAGS.test_subject, False, False)
input_provider.setEpocs(1)
test_data, current_file_config = input_provider.getTestSeq(FLAGS.seed_seq_length,
                                                           FLAGS.folder_path_to_data,
                                                           FLAGS.file_type, FLAGS.num_files)
layer_sizes = parseToListOfInts(FLAGS.list_num_neurons)

pose_sequence_prediction = PoseSquenceLearnerLSTM(0,
                                                 layer_sizes,
                                                 test_data.shape[2],
                                                 burn_in_length_before_training=0,
                                                 batch_size=1,
                                                 statefulness=True,
                                                 seq_length=2,
                                                 look_ahead_step=1,
                                                 callbacks=[],
                                                 classifier_rnn=False,
                                                 holden_dataset=False,
                                                 network_name='AutoEncoderLSTM',
                                                 optimizer='RMSprop',
                                                 loss='mean_squared_error')
pose_sequence_prediction.loadWeights('../model_weights.h5')
input_provider.resetCurrentFileReadHead()
output_post_processor = input_provider.getOutputPostProcessor()

# Skipping sequences untill walking action is obtained
for i in range(FLAGS.skip_seed_seqs):
    test_data, current_file_config = input_provider.getTestSeq(FLAGS.seed_seq_length + 100,
                                                               FLAGS.folder_path_to_data,
                                                               FLAGS.file_type, FLAGS.num_files)

predicted_pose = []
for i in range(FLAGS.seed_seq_length):
    predicted_pose = pose_sequence_prediction._model.predict(test_data[0:1, i:i+1, :])
    predicted_pose[0, 0, 3:6] = test_data[0, i, 3:6]
    predicted_pose_changed = numpy.array(predicted_pose)
    output_post_processor.accumulateTimeseriesRow(predicted_pose_changed[0, :, :])
    current_pose = output_post_processor.getRescaledAndResized()
    eng.drawSkeleton(matlab.double(current_pose[0, :].tolist()), False, nargout=0)
    # print current_pose

event_capturer = getDesiredOrientation.KeyEventCapture()
while 1:
    predicted_pose = pose_sequence_prediction._model.predict(predicted_pose)
    predicted_pose[0, 0, [3, 5]] = test_data[0, FLAGS.seed_seq_length, [3, 5]]

    predicted_pose[0, 0, 3:6] = \
        event_capturer.getDesiredOrientation(numpy.array([0.47073982, predicted_pose[0, 0, 4], 0.48639886]))

    predicted_pose_changed = numpy.array(predicted_pose)
    output_post_processor.accumulateTimeseriesRow(predicted_pose_changed[0, :, :])
    current_pose = output_post_processor.getRescaledAndResized()
    eng.drawSkeleton(matlab.double(current_pose[0, :].tolist()), True, nargout=0)
