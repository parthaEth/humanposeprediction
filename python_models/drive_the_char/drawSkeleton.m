function drawSkeleton(current_incremental_pose, drivable, ...
                      figure_window_number)
persistent first_call R0 T0 handles axis_handle skeleton skel_expmap ...
           already_made_drivable rows cols cols_actual rows_original v

if isempty(first_call)
    addpath('../../Matlab/ploting')
    addpath('../../Matlab/')
    addpath('../../providedCode/RNNexp/structural_rnn/CRFProblems/H3.6m/dataParser/Utils/')
    addpath('../../providedCode/RNNexp/structural_rnn/CRFProblems/H3.6m/mhmublv/Motion')
    
    set(gcf, 'Position', get(0,'Screensize')-[0, 0, 500, 500]);
    v = VideoWriter('newfile.avi');
    v.FrameRate = 16;
    open(v);
    
    already_made_drivable = false;
    
    first_call = false;
    R0 = eye(3);
    T0 = [0 0 0];
    
    load('../../Matlab/skeletons.mat')
    person = 1;
    skeleton = skeletons(person).angSkel;
    c = zeros(1,78);
    [ skel_expmap,~ ] = getExpmapFromSkeleton( skeleton, c );
  
    [R0, channels_reconstruct] = ...
        revertCoOrdinateIncrementally(current_incremental_pose, R0, T0);
    T0 = channels_reconstruct(1, 1:3);

    drawable_pose = getPosFromExpmap(skel_expmap, channels_reconstruct);
    [rows, cols] = size(drawable_pose);
    drawable_pose = reshape(drawable_pose', 1, rows*cols);
    
    skeleton = skeletons(person).posSkel;
    cols_actual = length(skeleton.tree) * 3;
    skeleton.type = 'MyOwn';
    rows_original = rows*cols / cols_actual;
    
    drawable_pose = ...
        reshape(drawable_pose, cols_actual, rows_original)';
    
%     if nargin < 3
%         figure;
%     else
%         figure(figure_window_number);
%     end
    title('Walking Predicted', 'Interpreter', 'none')
    axis_handle = gca;
    
    handles = ...
        initSkeletonPlot(drawable_pose(1, :),...
        skeleton, axis_handle);
    colorSkel(handles, false);
    drawnow
end

if drivable && ~already_made_drivable
     colorSkel(handles, true);
     already_made_drivable = true;
end

[R0, channels_reconstruct] = ...
        revertCoOrdinateIncrementally(current_incremental_pose, R0, T0);
T0 = channels_reconstruct(1, 1:3);

drawable_pose = getPosFromExpmap(skel_expmap, channels_reconstruct);
drawable_pose = reshape(drawable_pose', 1, rows*cols);
drawable_pose = ...
        reshape(drawable_pose, cols_actual, rows_original)';
    
updateSkeletonPlot(handles, drawable_pose, skeleton, true,...
                   axis_handle);
drawnow

F = getframe(gcf);
writeVideo(v, F);



end
