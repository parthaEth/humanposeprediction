import pygame
import gflags
from gflags import FLAGS

pygame.init()
pygame.display.set_mode((100, 100))

gflags.DEFINE_float('turn_acceleration', 0.08, "Speed at which the button press effects the character's movement")
gflags.DEFINE_float('max_turn_rate_right', 0.8701, "Max speed at which the character is allowed to turn right")
gflags.DEFINE_float('max_turn_rate_left', 0.5101, "Max speed at which the character is allowed to turn left")
gflags.DEFINE_float('zero_turn_rate_val', 0.60, "Speed at which the button press effects the character's movement")


class KeyEventCapture:
    def __init__(self):
        self._button_state = 0

    def getDesiredOrientation(self, prev_orientation):
        for event in pygame.event.get():
            if event.type == pygame.KEYDOWN and event.key == pygame.K_RIGHT:
                self._button_state = 1
            elif event.type == pygame.KEYDOWN and event.key == pygame.K_LEFT:
                self._button_state = -1
            elif event.type == pygame.KEYUP:
                self._button_state = 0

        if self._button_state == 1:
            if prev_orientation[1] < FLAGS.max_turn_rate_right:
                prev_orientation[1] += FLAGS.turn_acceleration
                print "Turn Right"
            else:
                prev_orientation[1] = FLAGS.max_turn_rate_right
        elif self._button_state == -1:
            if prev_orientation[1] > FLAGS.max_turn_rate_left:
                prev_orientation[1] -= FLAGS.turn_acceleration
                print "Turn Left"
            else:
                prev_orientation[1] = FLAGS.max_turn_rate_left
        else:
            prev_orientation[1] = FLAGS.zero_turn_rate_val
        return prev_orientation