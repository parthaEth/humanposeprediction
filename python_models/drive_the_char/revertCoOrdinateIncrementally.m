function [R, channels_reconstruct] = revertCoOrdinateIncrementally(channels_self,R_prev,T_prev)
    if size(channels_self, 1) > 1
	error(['Expected just one time step data got ',...
                num2str(size(channels_self, 1))]);
    end
    addpath('../../providedCode/RNNexp/structural_rnn/CRFProblems/H3.6m/mhmublv/Motion');
    channels_reconstruct = channels_self;
    rootRotInd = 4:6;
    R_diff = expmap2rotmat(channels_self(1,rootRotInd));
    R = R_diff*R_prev;
    channels_reconstruct(1,rootRotInd) = rotmat2expmap(R);
    T = T_prev + ((R_prev^-1)*(channels_self(1,1:3))')';
    channels_reconstruct(1,1:3) = T;
end
