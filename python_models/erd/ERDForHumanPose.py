#!/usr/bin/env python

# /*
#  * Created by Partha on 12/11/2016.
#  */
"""This script trains and tests a the encoder recurrent decoder (ERD) network"""
# TODO(Partha): Move the common functions to a base class and derive from there
import numpy

from keras.layers import TimeDistributedDense, LSTM, Dropout, BatchNormalization
from keras.constraints import maxnorm
from keras.models import Sequential
from keras.regularizers import l2
import gflags
import glog as log


FLAGS = gflags.FLAGS
gflags.DEFINE_float('W_regularizer_val', 0.0001, 'l2 regularization factor')
gflags.DEFINE_float('max_norm_val', 3, 'Max norm of dense layers')


class ERDForHumanPose:
    def __init__(self, dense_layer_sizes, rnn_layer_sizes, data_dim, batch_size,
                 seq_length, stateful, optimizer, loss):

        self._trainable_length = seq_length - 1
        self._stateful = stateful
        self._batch_size = batch_size
        self._loss = loss
        self._optimizer = optimizer

        batch_input_shape = (batch_size, self._trainable_length, data_dim)

        self._model = Sequential()

        self._io_names = ['encoder_input', 'encoder_out', 'rnn_input', 'rnn_output', 'decoder_input', 'decoder_out']
        self._hidden_layer_names = ['encoder_hidden', 'rnn_hidden', 'decoder_hadden']

        # Encoder
        io_layer_count = 0
        self._model.add(TimeDistributedDense(dense_layer_sizes[0], batch_input_shape=batch_input_shape,
                                             activation='relu', W_constraint=maxnorm(FLAGS.max_norm_val),
                                             name=self._io_names[io_layer_count]))
        io_layer_count += 1
        self._model.add(Dropout(0.2, batch_input_shape=batch_input_shape))

        hidden_layer_count = 0
        for layer_size in dense_layer_sizes[1:-1]:
            self._model.add(TimeDistributedDense(layer_size, batch_input_shape=batch_input_shape, activation='relu',
                                                 W_constraint=maxnorm(FLAGS.max_norm_val),
                                                 name=self._hidden_layer_names[0]+str(hidden_layer_count)))
            # self._model.add(Dropout(0.5, batch_input_shape=batch_input_shape))
            # self._model.add(BatchNormalization(epsilon=0.001, mode=0, axis=-1, momentum=0.99, weights=None, beta_init='zero',
            #                    gamma_init='one', gamma_regularizer=None, beta_regularizer=None))
            hidden_layer_count += 1

        self._model.add(TimeDistributedDense(dense_layer_sizes[-1], batch_input_shape=batch_input_shape,
                                             activation='relu', W_constraint=maxnorm(FLAGS.max_norm_val),
                                             name=self._io_names[io_layer_count]))
        io_layer_count += 1

        # RNN
        self._model.add(LSTM(rnn_layer_sizes[0], return_sequences=True, inner_init='orthogonal',
                             batch_input_shape=batch_input_shape, stateful=self._stateful,
                             W_regularizer=l2(FLAGS.W_regularizer_val), name=self._io_names[io_layer_count]))
        io_layer_count += 1

        hidden_layer_count = 0
        for layer_size in rnn_layer_sizes[1:-1]:
            self._model.add(LSTM(layer_size, return_sequences=True, inner_init='orthogonal',
                                 batch_input_shape=batch_input_shape, stateful=self._stateful,
                                 W_regularizer=l2(FLAGS.W_regularizer_val),
                                 name=self._hidden_layer_names[1]+str(hidden_layer_count)))
            hidden_layer_count += 1

        self._model.add(LSTM(rnn_layer_sizes[-1], return_sequences=True, inner_init='orthogonal',
                             batch_input_shape=batch_input_shape, stateful=self._stateful,
                             W_regularizer=l2(FLAGS.W_regularizer_val), name=self._io_names[io_layer_count]))
        io_layer_count += 1

        # Decoder
        reversed_sizes = list(dense_layer_sizes)
        reversed_sizes.reverse()

        self._model.add(TimeDistributedDense(reversed_sizes[0], batch_input_shape=batch_input_shape, activation='relu',
                                             W_constraint=maxnorm(FLAGS.max_norm_val),
                                             name=self._io_names[io_layer_count]))
        io_layer_count += 1

        hidden_layer_count = 0
        for layer_size in reversed_sizes[1:-1]:
            self._model.add(TimeDistributedDense(layer_size, batch_input_shape=batch_input_shape, activation='relu',
                                                 W_constraint=maxnorm(FLAGS.max_norm_val),
                                                 name=self._hidden_layer_names[2]+str(hidden_layer_count )))
            self._model.add(Dropout(0.05, batch_input_shape=batch_input_shape))
            # self._model.add(BatchNormalization(epsilon=0.001, mode=0, axis=-1, momentum=0.99, weights=None, beta_init='zero',
            #                    gamma_init='one', gamma_regularizer=None, beta_regularizer=None))
            hidden_layer_count += 1

        self._model.add(TimeDistributedDense(data_dim, batch_input_shape=batch_input_shape, activation='relu',
                                             W_constraint=maxnorm(FLAGS.max_norm_val),
                                             name=self._io_names[io_layer_count]))

        self._model.compile(loss=loss, optimizer=optimizer)
        self._model.summary()
        
    def freezNamedLayers(self, name_list):
        for name in name_list:
            for layer_num in range(len(self._model.layers)):
                if self._model.layers[layer_num].name == name:
                    self._model.layers[layer_num].trainable = False
                    break
        self._model.compile(loss=self._loss, optimizer=self._optimizer)
        self._model.summary()

    def freezeIOLayers(self):
        self.freezNamedLayers(self._io_names)

    def freezeFirstNhiddenLayers(self, n):
        names = []
        for j in range(3):
            for i in range(n):
                names.append(self._hidden_layer_names[j] + str(n))

        self.freezNamedLayers(names)

    def train(self, x_train, nb_epoch, validation_split, callbacks):
        """train the ERD on human pose sequence data"""
        hist = numpy.zeros((nb_epoch, 2))

        training_hist = self._model.fit(x_train[:, 0:-1, :], x_train[:, 1:, :], shuffle=True, nb_epoch=nb_epoch,
                                        callbacks=callbacks, batch_size=self._batch_size,
                                        validation_split=validation_split)

        hist[:, 0] = numpy.array(training_hist.history['loss']).T
        hist[:, 1] = numpy.array(training_hist.history['val_loss']).T

        return hist

    def denoisingTraining(self, x_train, nb_epoch, validation_split, sigma, callbacks):
        """train the ERD on human pose sequence data with input noise corruption"""
        hist = numpy.zeros((nb_epoch, 2))

        for epoch in range(nb_epoch):
            input_noise = numpy.random.normal(loc=0.0, scale=sigma,
                                              size=(x_train.shape[0], self._trainable_length,
                                                    x_train.shape[2]))
            training_hist = self._model.fit(x_train[:, 0:-1, :] + input_noise, x_train[:, 1:, :], shuffle=True,
                                            nb_epoch=1, callbacks=callbacks, batch_size=self._batch_size,
                                            validation_split=validation_split)

            hist[epoch, 0] = numpy.array(training_hist.history['loss']).T
            hist[epoch, 1] = numpy.array(training_hist.history['val_loss']).T

        return hist

    def denoisingTrainingOnaSchedule(self, x_train, nb_epoch_per_sigma, validation_split, sigma_list, callbacks):
        hist = numpy.zeros((len(sigma_list * nb_epoch_per_sigma), 2))
        count = 0
        for sigma in sigma_list:
            hist[count:count + nb_epoch_per_sigma, :] = \
                self.denoisingTraining(x_train, nb_epoch_per_sigma, validation_split, sigma, callbacks)

        return hist

    def predict(self, test_data, seed_seq_length, num_predicted_time_steps, output_post_processor):
        """test_data must have the shape (1, num_time_steps, features)"""
        log.check_ge(test_data.shape[1], seed_seq_length, "Requested seed is longer than data available test_data.shape"
                                                          " = " + str(test_data.shape) + " requested sseed length = " +
                                                          str(seed_seq_length))
        for time_step in range(seed_seq_length):
            # seed here
            predicted_seq = \
                self._model.predict(test_data[0:1, time_step:time_step + 1, :])
            # output_post_processor.accumulateTimeseriesRow(test_data[seq_ind, mini_seq_time_indx, :])
            output_post_processor.accumulateTimeseriesRow(predicted_seq[0, :, :])

        for i in range(num_predicted_time_steps):
            predicted_seq = self._model.predict(predicted_seq)
            output_post_processor.accumulateTimeseriesRow(predicted_seq[0, :, :])

    def loadWeights(self, model_name, by_name):
        self._model.load_weights(model_name, by_name)

    def saveWeights(self, model_name):
        self._model.save_weights(model_name)

    def visualizeModel(self, output_file_name):
        """To view the model"""
        from keras.utils.visualize_util import plot
        plot(self._model, to_file = output_file_name)

    def resetModel(self):
        if self._stateful:
            self._model.reset_states()
        else:
            log.error("Reset state is called on a non stateful RNN")

    @staticmethod
    def writePredictionsToFile(output_post_processor, dest_dir="", filename=""):
        output_post_processor.dumpRescaledAndResizedVals(filename, dest_dir)