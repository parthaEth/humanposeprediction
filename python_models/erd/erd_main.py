#!/usr/bin/env python

# /*
#  * Created by Partha on 09/02/2017.
#  */

#TODO(Partha): More than 90% of the main files are shared between projects so some how code share!

import sys
sys.path.append('../')
import numpy
from pre_post_processing import inputGenerator
from ERDForHumanPose import *
import keras
import gflags
from gflags import FLAGS
import glog as log

gflags.DEFINE_string('output_folder', '../GeneratedSequences', 'dir path where program output will go')
gflags.DEFINE_string('mdl_name', 'model_weights.h5', 'Name of the trained model file')
gflags.DEFINE_string('mdl_image_output_name', 'model.png', 'Name of model visualization image file')

gflags.DEFINE_integer('random_seed', 10, 'Random number generator seed')
gflags.DEFINE_integer('num_increasing_noise_epochs', 10, 'Number of epochs for which the noise schedule is applied')
gflags.DEFINE_integer('num_predicted_time_steps', 300, 'Number of time steps to predict after the seed sequence')
gflags.DEFINE_integer('mini_seq_length', 100, 'splitting the long sequences into several sequences of this length ')

gflags.DEFINE_list('list_encoder_neurons', ['500', '500', '500', '500'], 'Number of neurons as comma seperated vals')
gflags.DEFINE_list('list_rnn_neurons', ['1000', '1000', '1000'], 'Number of neurons as comma seperated vals')
gflags.DEFINE_list('list_noise_sigma', ['0.01', '0.05', '0.1'], 'Number of neurons as comma seperated vals')

gflags.DEFINE_boolean('train_model', False, 'Flag to retain model')
gflags.DEFINE_boolean('stateful', False, 'If the conditioner LSTM would be made stateful')
gflags.DEFINE_boolean('discard_prev_training', False, 'Set this flag to discard previous training (cold start)')
gflags.DEFINE_boolean('log_to_tensorboard', False, 'If training time logging to tensorboard should be performed')
gflags.DEFINE_boolean('graphical_visualization', False,
                      'Set true to get graphical visualization of model and learning curve')

def parseToListOfInts(list_encoder_neurons):
    layer_sizes = []
    for num_neurons in list_encoder_neurons:
        layer_sizes.append(int(num_neurons))

    return layer_sizes

def parseToListOfFloats(list_decimal_vals_as_string):
    list_decimal_vals = []
    for num_neurons in list_decimal_vals_as_string:
        list_decimal_vals.append(float(num_neurons))

    return list_decimal_vals

def main(argv):
    """Input arguments are parsed and options are set"""
    try:
        FLAGS(argv)  # parse flags
        if FLAGS.discard_prev_training and not FLAGS.train_model :
            log.fatal("Requesting to discard previous training has been issued while training is not "
                      "being  done in the current effort. This setting is not feasible.")
            exit(0)

        if FLAGS.num_files == -1 and FLAGS.train_model:
            load_preprocessing_params_from_file = False
        else:
            load_preprocessing_params_from_file = True

        input_provider = inputGenerator.InputProvider(load_preprocessing_params_from_file, False, False)

        encoder_decoder_layer_sizes = parseToListOfInts(FLAGS.list_encoder_neurons)
        rnn_layer_sizes = parseToListOfInts(FLAGS.list_rnn_neurons)
        callbacks = []

        if FLAGS.train_model:
            input_provider.loadTimeSeriesData()
            train_data, target_labels, current_file_config = \
                input_provider.getTrainingSeq(FLAGS.mini_seq_length, True)

            # ----------------------TRAINING----------------------#
            if FLAGS.log_to_tensorboard:
                callbacks.append(keras.callbacks.TensorBoard(log_dir='./' + FLAGS.output_folder, histogram_freq=0,
                                                             write_graph=True, write_images=False))

            # optimizer = keras.optimizers.SGD(lr=0.001, momentum=0.9, decay=0.0, nesterov=True, clipvalue=25)
            # optimizer = keras.optimizers.Adagrad(lr=0.01, epsilon=1e-08, decay=0.0001)
            # optimizer = keras.optimizers.Adam(lr=0.0005, beta_1=0.9, beta_2=0.999, epsilon=1e-08, decay=0.00001)
            optimizer = keras.optimizers.RMSprop(lr=0.001, clipvalue=25, decay=0.05)
            lr_schedule = keras.callbacks.ReduceLROnPlateau(monitor='val_loss', factor=0.5, patience=3, verbose=1,
                                                            mode='min', epsilon=0.0001, cooldown=0, min_lr=0)
            callbacks.append(lr_schedule)

            pose_sequence_generator = ERDForHumanPose(encoder_decoder_layer_sizes, rnn_layer_sizes,
                                                       train_data.shape[2], FLAGS.batch_size, FLAGS.mini_seq_length,
                                                       FLAGS.stateful, optimizer, 'mse')
            if FLAGS.graphical_visualization:
                from plotting import visualiTrainingHistory
                pose_sequence_generator.visualizeModel(FLAGS.mdl_image_output_name)

            if not FLAGS.discard_prev_training:
                pose_sequence_generator.loadWeights(FLAGS.mdl_name, True)
                # pose_sequence_generator.freezeIOLayers()

            train_hist = pose_sequence_generator.train(train_data, FLAGS.num_epochs, FLAGS.validation_split,
                                                       callbacks)

            if FLAGS.num_increasing_noise_epochs != 0:
                num_epochs_persigma = FLAGS.num_increasing_noise_epochs // len(FLAGS.list_noise_sigma)
                list_noise_sigma = parseToListOfFloats(FLAGS.list_noise_sigma)
                train_hist = numpy.concatenate((train_hist,
                                               pose_sequence_generator.denoisingTrainingOnaSchedule(
                                                   train_data, num_epochs_persigma, FLAGS.validation_split,
                                                   list_noise_sigma, callbacks)), axis=0)

            numpy.savetxt('training_history.csv', train_hist, delimiter=",")

            # Save the model weights
            pose_sequence_generator.saveWeights(FLAGS.mdl_name)

        #----------------------TESTING----------------------#
        # Load the same model for prediction. Except now the sequence length is only 1 for real time prediction
        input_provider.resetCurrentFileReadHead()
        statefulness_in_test_time = True

        test_data, current_file_config = input_provider.getTestSeq(FLAGS.seed_seq_length,
                                                                   FLAGS.folder_path_to_data_files,
                                                                   FLAGS.file_type, FLAGS.num_files)

        pose_sequence_prediction = ERDForHumanPose(encoder_decoder_layer_sizes, rnn_layer_sizes,
                                                    test_data.shape[2], 1, 2, statefulness_in_test_time,
                                                    'rmsprop', 'mse')
        pose_sequence_prediction.loadWeights(FLAGS.mdl_name, True)

        output_post_processor = input_provider.getOutputPostProcessor()

        out_file_counter = 0
        while True:
            out_file_name = ""

            output_post_processor.setSeedOutputConfiguration(current_file_config)
            if not test_data.size:  # if test_data is empty
                break
            pose_sequence_prediction.predict(test_data, FLAGS.seed_seq_length, FLAGS.num_predicted_time_steps,
                                             output_post_processor)
            pose_sequence_prediction.writePredictionsToFile(output_post_processor, FLAGS.output_folder, out_file_name)
            pose_sequence_prediction.resetModel()
            print "<-----------------Test time saved file" + str(out_file_counter) + "----------------------------->"
            test_data, current_file_config = input_provider.getTestSeq(FLAGS.seed_seq_length,
                                                                       FLAGS.output_folder,
                                                                       FLAGS.file_type, FLAGS.num_files)

        if FLAGS.train_model:
            raw_input("All operations finished. Press ENTER to terminate.")

    except(KeyboardInterrupt, SystemExit):
        print 'Interrupted'
        if FLAGS.train_model:
            # Save the model weights
            print "<<<<<<<<<<<<<<<<Saved Model>>>>>>>>>>>>>>>>>>>>>>>"
            pose_sequence_generator.saveWeights(FLAGS.mdl_name)

            numpy.savetxt('training_history.csv', train_hist, delimiter=",")
            if FLAGS.graphical_visualization:
                from plotting import visualiTrainingHistory
                pose_sequence_generator.visualizeModel(FLAGS.mdl_image_output_name)
                visualiTrainingHistory(train_hist)
            raw_input("All operations finished. Press ENTER to terminate.")
    except gflags.FlagsError, e:
        print '%s\\nUsage: %s ARGS\\n%s' % (e, sys.argv[0], FLAGS)
if __name__ == "__main__":
    main(sys.argv)