#!/usr/bin/env python

# /*
#  * Created by Partha on 10/11/2016.
#  */

import numpy
import os
import math
from outputPostProcessor import *


class InputProvider:
    """Public class for all diskio and data preparation"""
    _action_categories = ["directions_", "discussion", "eating_", "greeting_", "phoning_", "posing", "purchase",
                          "sitting_", "sittingdown_", "smoking_", "takingphoto_", "waiting_", "walking_", "walkingdog_",
                          "walkingtogether_"]
    def __init__(self, batch_size, validation_split, down_sample_in_time_by_n, load_preprocessing_params_from_file,
                 seed_seq_length, test_subject, classifier_rnn, classifier_rnn_on_grnd_truth):
        self._all_poses = []
        self._list_file_attribs = []
        self._list_test_file_attribs = []
        self._previous_data_segment_sent = []
        self._epochs = 1
        self._current_epoch = 0
        self._current_file = 0
        self._shift_vector = []
        self._scale_vector = []
        self._deleted_column_vals = []
        self._deleted_column_idxes = []
        self._all_poses_test = []
        self._list_test_file_attribs = []
        self._get_all_train_data_in_one_go = False
        self._get_data_called = False
        self._batch_size = batch_size
        self._validation_split = validation_split
        self._down_sample_in_time_by_n = down_sample_in_time_by_n
        self._load_preprocessing_params_from_file = load_preprocessing_params_from_file
        self._seed_seq_length = seed_seq_length
        self._num_files_toread = -1
        self._test_subject = test_subject
        self._original_smapling_rate_in_HZ = 50.0
        self._current_test_file = 0
        self._classifier_rnn = classifier_rnn
        self._classifier_rnn_on_grnd_truth = classifier_rnn_on_grnd_truth
        self._data_location = "/MyPoseFeatures/D3_Angles/expMap/"
        # self._data_location = "/MyPoseFeatures/fakeTestData/"
        # self._data_location = "/MyPoseFeatures/norm3Dpos/"
        self._min_required_feature_std = 0.01  # Set to very low value for now
        # self._min_required_feature_std = 0.0001  # Set to very low value for now

    def setEpocs(self, epochs):
        self._epochs = epochs

    def isDataAvailable(self):
        if self._get_all_train_data_in_one_go:
            return not self._get_data_called
        else:
            # print self._current_epoch
            # print self._current_file
            return not (self._current_epoch == self._epochs -1 and self._current_file == len(self._list_file_attribs))

    def getNumAvailableSeqWithMinLength(self, mini_seq_length_during_training, minimum_required_length):
        num_valid_seqs = 0

        for file_idx in range(len(self._list_file_attribs)):
            org_seq_len = self._list_file_attribs[self._current_file].end_row - \
                          self._list_file_attribs[self._current_file].start_row
            num_mini_seq = int(math.floor(org_seq_len / (mini_seq_length_during_training * 1.0)))

            read_head = num_mini_seq * mini_seq_length_during_training

            if self._list_file_attribs[self._current_file].end_row - read_head + 1 > minimum_required_length:
                num_valid_seqs += 1

        return num_valid_seqs

    def getOutputPostProcessor(self):
        output_post_processor = OutputPostProcessor(self._shift_vector, self._scale_vector, self._deleted_column_vals,
                                                    self._deleted_column_idxes)
        return output_post_processor

    def getTestSeq(self, required_seq_length, base_folder_path, file_type, num_files_toread=-1):
        if len(self._all_poses_test) == 0:
            if self._classifier_rnn:
                if not self._classifier_rnn_on_grnd_truth:
                    self.loadGeneratedData(base_folder_path, file_type, num_files_toread)
            else:
                self.loadOnlyTestSubject(base_folder_path, self._test_subject, file_type, num_files_toread)

        if self._current_test_file < len(self._list_test_file_attribs):
            org_seq_len = self._list_test_file_attribs[self._current_test_file].end_row - \
                          self._list_test_file_attribs[self._current_test_file].start_row + 1
            if org_seq_len >= required_seq_length:
                current_file_config = OutputFileConfiguration(
                    self._list_test_file_attribs[self._current_test_file].name_file[:-2],
                    int(self._list_test_file_attribs[self._current_test_file].name_file[-1]),
                    required_seq_length, self._original_smapling_rate_in_HZ / self._down_sample_in_time_by_n,
                    self._down_sample_in_time_by_n)

                read_head = self._list_test_file_attribs[self._current_test_file].start_row

                xtest = numpy.zeros((1, required_seq_length, self._all_poses_test.shape[1]))
                xtest[0, :, :] = self._all_poses_test[read_head:read_head + required_seq_length, :]

                self._current_test_file += 1
                return xtest, current_file_config
            else:
                self._current_test_file += 1
                return self.getTestSeq(required_seq_length, base_folder_path, file_type, num_files_toread)
        else:
            return numpy.array([]), OutputFileConfiguration()

    def getUnusedTrainSeq(self, mini_seq_length_during_training):
        '''Because of the fixed mini seq lengths some of the training samples are always left. This function returns
        those'''
        # TODO(Partha): Fix this function as it will not work for the classifier RNN

        if self._current_file < len(self._list_file_attribs):
            org_seq_len = self._list_file_attribs[self._current_file].end_row - \
                          self._list_file_attribs[self._current_file].start_row
            num_mini_seq = int(math.floor(org_seq_len / (mini_seq_length_during_training * 1.0)))

            read_head = num_mini_seq * mini_seq_length_during_training + \
                        self._list_file_attribs[self._current_file].start_row;
            remaining_seq_len = self._list_file_attribs[self._current_file].end_row - read_head
            if remaining_seq_len > self._seed_seq_length:
                xtest = numpy.zeros((1, remaining_seq_len, self._all_poses.shape[1]))
                xtest[0, :, :] = self._all_poses[read_head:remaining_seq_len+read_head, :]

                current_file_config = OutputFileConfiguration(
                    self._list_file_attribs[self._current_file].name_file,
                    read_head - self._list_file_attribs[self._current_file].start_row,
                    self._seed_seq_length, self._original_smapling_rate_in_HZ/self._down_sample_in_time_by_n,
                    self._down_sample_in_time_by_n)

                self._current_file += 1
                return xtest, current_file_config
            elif self._current_file < len(self._list_file_attribs) - 1:
                self._current_file += 1
                return self.getUnusedTrainSeq(mini_seq_length_during_training)
            else:
                self._current_file = 0
                return numpy.array([]), OutputFileConfiguration()
        else:
            self._current_file = 0
            return numpy.array([]), OutputFileConfiguration()

    def resetCurrentFileReadHead(self):
        self._current_file = 0
        self._get_all_train_data_in_one_go = False
        self._get_data_called = False
        self._current_epoch = 0
        self._current_test_file = 0

    def getTrainingSeq(self, mini_seq_length, get_all_train_data_in_one_go):
        """Splits data into the shape (sequences, time_steps_per_seq, dataa_dim). Where each file is assumed to contain
        one long sequece and we care to back prop till mini_seq_length. So we split the big sequence into n small
        seqences of length mini_seq_length. The left over data is thrown away :(. The data format is
        (n, mini_seq_length, dataa_dim)"""
        self._get_all_train_data_in_one_go = get_all_train_data_in_one_go
        if get_all_train_data_in_one_go:
            current_file_config = OutputFileConfiguration()
            num_mini_seq = 0
            for num_files in range(len(self._list_file_attribs)):
                seq_len = self._list_file_attribs[num_files].end_row - \
                          self._list_file_attribs[num_files].start_row + 1

                num_mini_seq += int(math.floor(seq_len / (mini_seq_length * 1.0)))

            validation_data_len = int(math.ceil(num_mini_seq * self._validation_split))
            train_data_len = int(round(num_mini_seq - validation_data_len))
            if train_data_len % self._batch_size > 0 or validation_data_len % self._batch_size > 0:
                print "Fatal error: Batch size must divide number of all sequences available. num_mini_seq = " + \
                      str(train_data_len) + " validation_data_len = " + str(validation_data_len) + " and batch_size = " \
                      + str(self._batch_size)
                exit(8)
            x_train = numpy.zeros((num_mini_seq, mini_seq_length, self._all_poses.shape[1]))
            y_train = numpy.zeros((num_mini_seq, mini_seq_length, len(self._action_categories)))

            mini_seq_write_head = 0
            for num_files in range(len(self._list_file_attribs)):
                seq_len = self._list_file_attribs[num_files].end_row - \
                          self._list_file_attribs[num_files].start_row + 1

                num_mini_seq = int(math.floor(seq_len / (mini_seq_length * 1.0)))
                read_head = self._list_file_attribs[num_files].start_row
                for mini_seq_idx in range(num_mini_seq):
                    # TODO(Partha) IF you run out of memory may be improve this as this is just resizing and we are
                    # actually copying here
                    x_train[mini_seq_write_head, :, :] = \
                        self._all_poses[read_head:read_head + mini_seq_length, :]

                    #  Since the unnatural sequences are a conglomerate of different model's output they do not have
                    #  definite names. Hence if everything fails we label it as unnatural class
                    # y_train[mini_seq_write_head, :, -1] = 1

                    for i in range(len(self._action_categories)):
                        # Giving class labels depending upon file name.
                        if self._action_categories[i] in self._list_file_attribs[num_files].name_file:
                            y_train[mini_seq_write_head, :, i] = 1
                            break

                    if self._classifier_rnn and numpy.sum(numpy.sum(y_train[mini_seq_write_head, :, :])) != y_train.shape[1]:
                        print "From Input generator: More than one catagory marked! It's a bug. should be = " +\
                              str(y_train.shape[1]) + " Found = " + \
                              str(numpy.sum(numpy.sum(y_train[mini_seq_write_head, :, :])))
                        exit()

                    read_head += mini_seq_length
                    mini_seq_write_head += 1

            self._get_data_called = True
            for i in range(len(self._action_categories)):
                setattr(self, "num_" + self._action_categories[i], numpy.sum(numpy.sum(y_train[:, :, i])))
                print "num_" + self._action_categories[i] + " has" + \
                       str(getattr(self, "num_" + self._action_categories[i])) + " samples"

            # if self._classifier_rnn:
            #     x_train[0:5, :, :] = 0;

            return x_train, y_train, current_file_config
        else:
            # TODO(Partha): This part is outdated Need to clean up!
            print("This part might not be working properly! Clean up first")
            exit(20)
            if self._current_epoch < self._epochs and self._current_file < len(self._list_file_attribs):

                seq_len = self._list_file_attribs[self._current_file].end_row - \
                          self._list_file_attribs[self._current_file].start_row + 1

                num_mini_seq = int(math.floor(seq_len / (mini_seq_length * 1.0)))
                x_train = numpy.zeros((num_mini_seq, mini_seq_length, self._all_poses.shape[1]))
                y_train = numpy.zeros((num_mini_seq, mini_seq_length, 1))

                read_head = self._list_file_attribs[self._current_file].start_row

                for mini_seq_idx in range(num_mini_seq):
                    x_train[mini_seq_idx, :, :] = self._all_poses[read_head:read_head + mini_seq_length, :]
                    y_train[mini_seq_idx, :, :] = self._list_file_attribs[self._current_file].ground_truth_data_file
                    read_head += mini_seq_length

                current_file_config = \
                    OutputFileConfiguration(self._list_file_attribs[self._current_file].name_file,
                                            0,
                                            self._seed_seq_length,
                                            self._original_smapling_rate_in_HZ/self._down_sample_in_time_by_n,
                                            self._down_sample_in_time_by_n)

                self._current_file += 1
                return x_train, y_train, current_file_config
            elif self._current_epoch < self._epochs and self._current_file == len(self._list_file_attribs):
                self._current_epoch += 1
                self._current_file = 0
                return self.getTrainingSeq(mini_seq_length, False)
            else:
                self._current_file = 0
                return numpy.array([]), numpy.array([]), OutputFileConfiguration()

    def getPercentComplete(self):
        total_length = self._epochs * len(self._list_file_attribs)
        percent_complete = (self._current_epoch *
                            len(self._list_file_attribs) + self._current_file) * 100.0 / total_length
        return percent_complete

    def printCurrentlyLoadedData(self, num_points = -1):
        """Print only a few first points or all of them"""
        if(num_points < 0):
            print self._all_poses
        else:
            print self._all_poses[:num_points]

    def normalizeAccrossColumn(self, perform_on_train_data, perform_on_test_data):
        """Scaling 0-1 may want to switch to zero mean and 1-STD"""
        if self._load_preprocessing_params_from_file:
            shift_and_scale = numpy.loadtxt(open(('shift_and_scale.text'), "rb"), delimiter=",", skiprows=0)
            if shift_and_scale.ndim == 1: shift_and_scale = shift_and_scale[:, None]

            self._shift_vector = shift_and_scale[0, :]
            self._scale_vector = shift_and_scale[1, :]

            if perform_on_train_data:
                self._all_poses -= self._shift_vector
                self._all_poses /= self._scale_vector.astype(float)

            if perform_on_test_data:
                self._all_poses_test -= self._shift_vector
                self._all_poses_test /= self._scale_vector.astype(float)
        else:
            if not perform_on_train_data:
                print("Fatal error: Normalization can not be calculated if not performed on train data")
                exit(10)

            margin = 0.02  #Saw sturation happening close to value 1 perhaps because of not being able to saturate
            # nonlinearity  so keeping a small margine.
            self._shift_vector = numpy.min(self._all_poses, axis=0) - margin
            self._all_poses -= self._shift_vector
            self._scale_vector = numpy.max(self._all_poses, axis=0) + margin
            self._all_poses /= self._scale_vector.astype(float)
            numpy.savetxt('shift_and_scale.text', numpy.vstack((self._shift_vector, self._scale_vector)), delimiter=",")

            if perform_on_test_data:
                self._all_poses_test -= self._shift_vector
                self._all_poses_test /= self._scale_vector.astype(float)

    def removeNearConstFeature(self, min_required_std, perform_on_train_data, perform_on_test_data):
        """Removed all the dimensionality with very little or no variance. It removes columns.
         First three dimensions are left untouched as they correspond to the absolute position"""
        if self._load_preprocessing_params_from_file:
            std_each_col = numpy.loadtxt(open(('column_wise_std.text'), "rb"), delimiter=",", skiprows=1)
        else:
            if not perform_on_train_data:
                print("Fatal error: Normalization can not be calculated if not performed on train data")
                exit(10)

            std_each_col = numpy.std(self._all_poses, axis=0)
            if os.path.exists('column_wise_std.text'):
                os.remove('column_wise_std.text')

            file_handle = open('column_wise_std.text', 'ab')
            file_handle.write("std_threshold = " + str(min_required_std) + "\n\r")
            numpy.savetxt(file_handle, std_each_col.reshape(1, len(std_each_col)), delimiter=",")
            file_handle.close()

        if len(std_each_col.shape) == 0:
            self._deleted_column_idxes = numpy.array([])
            self._deleted_column_vals = numpy.array([])
        else:
            self._deleted_column_idxes = numpy.where(std_each_col < min_required_std)[0]

            if perform_on_train_data:
                self._deleted_column_vals = self._all_poses[0, std_each_col < min_required_std]
                self._all_poses = self._all_poses[:, std_each_col > min_required_std]

            if perform_on_test_data:
                self._deleted_column_vals = self._all_poses_test[0, std_each_col < min_required_std]
                self._all_poses_test = self._all_poses_test[:, std_each_col > min_required_std]

    def makePositionFrameIndependent(self):
        """X-Y dimensions made incremental => will not depend on root frame. Z co-ordinate forced to start from zero
        => it will only start from origin but preserves the fact that it can not drift freely as opposed to X and Y"""
        for file_number in range(len(self._list_file_attribs)):
            start_row = self._list_file_attribs[file_number].start_row
            end_row = self._list_file_attribs[file_number].end_row

            zero_row = numpy.zeros(2)
            subtraction_mat = numpy.vstack((self._all_poses[start_row, :2], self._all_poses[start_row:end_row - 1, :2]))
            subtraction_mat = numpy.hstack((subtraction_mat,
                                            self._all_poses[start_row, 2] * numpy.ones((end_row - start_row, 1))))

            self._all_poses[start_row:end_row, :3] -= subtraction_mat

    def downSample(self, smoothing, perform_on_train_data, perform_on_test_data):
        if smoothing:
            print "Fatal error: This option is not available yet"
            exit(8)

        new_start_row = 0

        if perform_on_train_data:
            list_file_attribs = []
            count_file = 0
            for file_idx in range(len(self._list_file_attribs)):
                time_steps_in_current_file = \
                    self._list_file_attribs[file_idx].end_row - self._list_file_attribs[file_idx].start_row + 1

                time_steps_after_sampling = int(time_steps_in_current_file - 1)/int(self._down_sample_in_time_by_n) + 1
                rows_to_be_retained = self._list_file_attribs[file_idx].start_row + self._down_sample_in_time_by_n * \
                                         (numpy.array(range(time_steps_after_sampling)))

                if file_idx == 0:
                    all_rows_to_be_retained = rows_to_be_retained
                else:
                    all_rows_to_be_retained = numpy.hstack((all_rows_to_be_retained, rows_to_be_retained))

                list_file_attribs.append(
                    FilesAndNumberOfPoses(self._list_file_attribs[file_idx].name_file + "_0", count_file,
                                          new_start_row, new_start_row + time_steps_after_sampling - 1,
                                          self._list_file_attribs[file_idx].ground_truth_data_file))
                count_file += 1
                new_start_row += time_steps_after_sampling

                #  Pack the secondery sequences
                time_steps_after_sampling -= 1
                for i in (range(self._down_sample_in_time_by_n - 1)):
                    rows_to_be_retained = \
                        i + 1 + self._list_file_attribs[file_idx].start_row + \
                            self._down_sample_in_time_by_n * (numpy.array(range(time_steps_after_sampling)))
                    all_rows_to_be_retained = numpy.hstack((all_rows_to_be_retained, rows_to_be_retained))

                    list_file_attribs.append(
                        FilesAndNumberOfPoses(self._list_file_attribs[file_idx].name_file + "_" + str(i + 1),
                                              count_file, new_start_row, new_start_row + time_steps_after_sampling - 1,
                                              self._list_file_attribs[file_idx].ground_truth_data_file))
                    count_file += 1
                    new_start_row += time_steps_after_sampling

            self._all_poses = self._all_poses[all_rows_to_be_retained, :]
            self._list_file_attribs = list_file_attribs

        if perform_on_test_data:
            list_test_file_attribs = []
            count_test_file = 0
            for file_idx in range(len(self._list_test_file_attribs)):
                time_steps_in_current_file = \
                    self._list_test_file_attribs[file_idx].end_row - self._list_test_file_attribs[file_idx].start_row + 1

                time_steps_after_sampling = int(time_steps_in_current_file - 1) / int(
                    self._down_sample_in_time_by_n) + 1
                rows_to_be_retained = self._list_test_file_attribs[file_idx].start_row + self._down_sample_in_time_by_n * \
                                                                                    (numpy.array(range(
                                                                                        time_steps_after_sampling)))

                if file_idx == 0:
                    all_rows_to_be_retained = rows_to_be_retained
                else:
                    all_rows_to_be_retained = numpy.hstack((all_rows_to_be_retained, rows_to_be_retained))

                list_test_file_attribs.append(
                    FilesAndNumberOfPoses(self._list_test_file_attribs[file_idx].name_file + "_0", count_test_file,
                                          new_start_row, new_start_row + time_steps_after_sampling - 1,
                                          self._list_test_file_attribs[file_idx].ground_truth_data_file))
                count_test_file += 1
                new_start_row += time_steps_after_sampling

                #  Pack the secondery sequences
                time_steps_after_sampling -= 1
                for i in (range(self._down_sample_in_time_by_n - 1)):
                    rows_to_be_retained = \
                        i + 1 + self._list_test_file_attribs[file_idx].start_row + \
                        self._down_sample_in_time_by_n * (numpy.array(range(time_steps_after_sampling)))
                    all_rows_to_be_retained = numpy.hstack((all_rows_to_be_retained, rows_to_be_retained))

                    list_test_file_attribs.append(
                        FilesAndNumberOfPoses(self._list_test_file_attribs[file_idx].name_file + "_" + str(i + 1),
                                              count_test_file, new_start_row, new_start_row + time_steps_after_sampling - 1,
                                              self._list_test_file_attribs[file_idx].ground_truth_data_file))
                    count_test_file += 1
                    new_start_row += time_steps_after_sampling

            self._all_poses_test = self._all_poses_test[all_rows_to_be_retained, :]
            self._list_test_file_attribs = list_test_file_attribs

    def loadTimeSeriesData(self, base_folder_path, file_type, num_files_toread=-1):
        """Reads around #number of rows from given directory across files."""
        # Read every file that is a csv file
        self._num_files_toread = num_files_toread
        break_flag = False
        count_file = 0
        end_row = -1
        for s in range(11):
            # if s == int(self._test_subject - 1):
            #     continue
            file_path = base_folder_path + "/S" + str(s + 1) + self._data_location
            if os.path.exists(file_path):
                print "Reading data for subject S" + str(s + 1)
                for f in os.listdir(file_path):
                    if self._classifier_rnn and f.find('eat') < 0 and f.find('walking_') < 0 and\
                       f.find('directions_') < 0 and f.find('purchases_') < 0 and f.find('greeting_') < 0 and\
                       f.find('sitting_') < 0 and f.find('sittingdown_') < 0 and s != 11:
                        continue
                    print "filename = " + f
                    if os.path.isfile(file_path + f) & (f[-4:] == file_type):
                        start_row = end_row + 1
                        if count_file == 0:
                            self._all_poses = numpy.loadtxt(open((file_path + f), "rb"), delimiter=",", skiprows=0)
                            if self._all_poses.ndim == 1: self._all_poses = self._all_poses[:, None]
                        else:
                            new_data = numpy.loadtxt(open((file_path + f), "rb"), delimiter=",", skiprows=0)
                            if new_data.ndim == 1: new_data = new_data[:, None]
                            self._all_poses = numpy.vstack((self._all_poses, new_data))
                        end_row = numpy.size(self._all_poses, axis=0) - 1
                        self._list_file_attribs.append(FilesAndNumberOfPoses(file_path + f, count_file,\
                                                                            start_row,\
                                                                            end_row, 1))
                        count_file += 1
                        if (count_file >= num_files_toread) and (num_files_toread > 0):
                            break_flag = True
                            break  # loading only one file to save debug time for now
            if break_flag:
                break

        if count_file == 0:
            print "No file found of specified type = *" + file_type
            exit(3)


        perform_on_train_data = True
        perform_on_test_data = False
        with_smoothing = False
        self.downSample(with_smoothing, perform_on_train_data, perform_on_test_data)
        self.removeNearConstFeature(self._min_required_feature_std, perform_on_train_data, perform_on_test_data)
        # Features that are constant across all files
        # self.makePositionFrameIndependent(perform_on_train_data, perform_on_test_data)
        self.normalizeAccrossColumn(perform_on_train_data, perform_on_test_data)  # Normalize across files
        self._load_preprocessing_params_from_file = True

    def loadNegativeSamples(self, parent_dir_to_negative_smpls, file_type, num_files_toread):
        end_row = self._list_file_attribs[-1].end_row
        count_file = self._list_file_attribs[-1].id
        count_file_begin = count_file

        for f in os.listdir(parent_dir_to_negative_smpls):
            print "filename = " + f
            if os.path.isfile(parent_dir_to_negative_smpls + '/' + f) & (f[-4:] == file_type):
                start_row = end_row + 1
                new_data = numpy.loadtxt(open((parent_dir_to_negative_smpls + '/' + f), "rb"), delimiter=",", skiprows=0)
                if new_data.ndim == 1: new_data = new_data[:, None]

                new_data = numpy.delete(new_data, self._deleted_column_idxes, 1)
                new_data -= self._shift_vector
                new_data /= self._scale_vector.astype(float)

                self._all_poses = numpy.vstack((self._all_poses, new_data))
                end_row = numpy.size(self._all_poses, axis=0) - 1
                self._list_file_attribs.append(FilesAndNumberOfPoses(parent_dir_to_negative_smpls + f,
                                                                     count_file,
                                                                     start_row,
                                                                     end_row, 0))
                count_file += 1
                if count_file - count_file_begin >= num_files_toread and (num_files_toread > 0):
                    break

        if count_file == count_file_begin:
            print "No file found of specified type"
            exit(3)

    def loadToTestData(self, file_path, file_type, num_files_toread, skiprows):
        break_flag = False
        count_file = 0
        end_row = -1
        for f in os.listdir(file_path):
            print "filename = " + f
            if os.path.isfile(file_path + '/' + f) & (f[-4:] == file_type):
                start_row = end_row + 1
                if count_file == 0:
                    self._all_poses_test = numpy.loadtxt(open((file_path + '/' + f), "rb"), delimiter=",",
                                                         skiprows=skiprows)
                    if self._all_poses_test.ndim == 1: self._all_poses_test = self._all_poses_test[:, None]
                else:
                    new_data = numpy.loadtxt(open((file_path + f), "rb"), delimiter=",", skiprows=skiprows)
                    if new_data.ndim == 1: new_data = new_data[:, None]
                    self._all_poses_test = numpy.vstack((self._all_poses_test, new_data))
                end_row = numpy.size(self._all_poses_test, axis=0) - 1
                self._list_test_file_attribs.append(FilesAndNumberOfPoses(file_path + f, count_file,\
                                                                    start_row,\
                                                                    end_row, 0))
                count_file += 1
                if (count_file >= num_files_toread) and (num_files_toread > 0):
                    break_flag = True
                    break  # loading only one file to save debug time for now

        if count_file == 0:
            print "No file found of specified type, loadToTestData will exit"
            exit(3)

        perform_on_train_data = False
        perform_on_test_data = True
        with_smoothing = False
        # Features that are constant across all files
        self.removeNearConstFeature(self._min_required_feature_std, perform_on_train_data, perform_on_test_data)
        # self.makePositionFrameIndependent(perform_on_train_data, perform_on_test_data)
        self.normalizeAccrossColumn(perform_on_train_data, perform_on_test_data)  # Normalize across files

    def loadGeneratedData(self, base_folder_path, file_type, num_files_toread):
        skip_rows = 5
        if base_folder_path[-1] != '\\' and base_folder_path[-1] != '/':
            base_folder_path += '/'

        self.loadToTestData(base_folder_path, file_type, num_files_toread, skip_rows)
        perform_on_train_data = False
        perform_on_test_data = True
        with_smoothing = False
        self.downSample(with_smoothing, perform_on_train_data, perform_on_test_data)

    def loadOnlyTestSubject(self, base_folder_path, subject_index, file_type, num_files_toread=-1):
        """Reads around #number of rows from given directory across files."""
        # Read every file that is a csv file
        skip_rows = 0
        file_path = base_folder_path + "/S" + str(subject_index) + self._data_location
        if os.path.exists(file_path):
            print "Reading data for subject S" + str(subject_index)
            self.loadToTestData(file_path, file_type, num_files_toread, skip_rows)

        perform_on_train_data = False
        perform_on_test_data = True
        with_smoothing = False
        self.downSample(with_smoothing, perform_on_train_data, perform_on_test_data)


class FilesAndNumberOfPoses:
    """This class conains the name of the action performed and the row indices where it is located"""
    name_file = ""
    start_row = 0
    end_row = 0
    id = 0
    ground_truth_data_file = 1

    def __init__(self, name, id, start_row, end_row, ground_truth_data_file):
        self.name_file = name
        self.id = id
        self.start_row = start_row
        self.end_row = end_row
        self.ground_truth_data_file = ground_truth_data_file
