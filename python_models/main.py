#!/usr/bin/env python

# /*
#  * Created by Partha on 10/11/2016.
#  */

import os
import getopt, sys
from inputGenerator import *
import keras
from LSTM_fr_pose import *
import pre_post_processing.inputGeneratorHolden


def usage():
    print "Please specify following:\n" +\
          "-h   or --help                   Print this message\n" +\
          "--input_dir_prefix               Input file path location (required)\n" +\
          "--file_type                      Input file type (.csv, .txt etc.)\n" +\
          "--verbose                        Verbosity level 0-3, 0 is highest priority logging\n" +\
          "--num_files                      Number of files to load from the specified folder\n" +\
          "--neurons_this_layer             Number of neurons in current layer. Specify as many times as number of" +\
                                                " required layers, e.g. -l 1000 -l 1000 etc.\n" +\
          "--look_ahead_step                Number of time steps that the RNN is asked to predict\n" +\
          "--burn_in_length                 Burn in length given to the LSTM before starting to train\n"+\
          "--validation_split               Percentage of training data to be used for validation. 10 = 10%\n" +\
          "--train_model                    Flag to skip training\n" +\
          "--seed_seq_length                Length of sequence to seed the NN with before starting to predict\n" +\
          "--num_predicted_time_steps       Number of time steps to predict after the seed sequence\n" +\
          "--discard_prev_training          Set this flag to resume training from previous weights\n" +\
          "--num_epochs                     Number of epochs to train the model\n" +\
          "--save_model_every_n_epoch       Wheather to save the model params after every epoche of training \n" +\
          "--mini_seq_length                splitting the long sequences into several sequences of this length \n" +\
          "--output_folder                  Path to the folder in which test sequences are generated \n" +\
          "--stateful                       Pass this flag if you want a stateful RNN \n" +\
          "--test_on_train_data             Set this flag to test the model on training data \n" +\
          "--batch_size                     One gradient step every these many example sequences \n" +\
          "--log_to_tensorboard             If training time logging to tensorboard should be performed \n" +\
          "--down_sample_in_time_by_n       --down_sample_in_time_by_n=10 means every 10th data is used \n" +\
          "--piggy_back_train_order         Train in piggy back setting for these many orders \n" +\
          "--test_subject                   All data except for this subject will be used for taining \n" +\
          "--skip_trainin_viz               Save a png image of the constructed model \n" +\
          "--classifier_rnn                 The classifier RNN which makes a decision between generated / true seq\n" +\
          "--classifier_rnn_on_grnd_truth   Test classifier_rnn on ground truth data. Should produce always 1s\n" +\
          "--parent_dir_to_negative_smpls   Full path to the negativeSamples folder\n" +\
          "--parent_dir_to_generated_smpls  Full path to the dir where 'tobe evaluated samples are located' \n" +\
          "--holden_dataset                 Specify the dataset to be used \n" +\
          "--network_name                   ERD/ LSTM3LR/ AutoEncoderLSTM, are the choices \n" +\
          "--select_GPU_with_id             Provide the GPU id on which to run the program this is a mandatory flag\n " +\
          "--input_dropout_rate             The dropout rate in the input. Should be provided preferrably only in " \
                                            "test time\n" +\
          "--load_weights_by_name           Load pretrained file by name or try to load all layers \n"


def main():
    """Input arguments are parsed and options are set"""
    try:
        # defaults here
        numpy.random.seed(100)
        select_GPU_with_id = -1
        dir_path = os.path.dirname(os.path.realpath(__file__))
        parent_dir, tail = os.path.split(dir_path)
        folder_path_to_data_files = parent_dir + "/Data"
        parent_dir_to_negative_smpls = parent_dir + "/negativeSamples"
        parent_dir_to_generated_smpls = dir_path + "/GeneratedSequences"
        file_type = ".csv"
        num_files = -1
        layer_sizes = [1000, 1000]
        input_dropout_rate = 0
        global verbose
        verbose = 0
        look_ahead_step = 1
        burn_in_length = 0
        flags_layer_size_option_first_call = True
        validation_split = 0.1
        mini_seq_length = 100
        skip_training = True
        seed_seq_length = 20
        num_predicted_time_steps = 100
        discard_prev_training = False
        num_epochs = 1
        dont_save_model = False
        save_model_every_n_epoch = 20
        output_folder = "GeneratedSequences"
        stateful = False
        test_on_train_data = False
        batch_size = 1
        log_to_tensorboard = False
        down_sample_in_time_by_n = 1
        increasing_noise_training = False
        # input_noise_sigma = [0.01, 0.05, 0.1, 0.2, 0.3, 0.5, 0.75]  # Suggested in the paper
        input_noise_sigma = [0.01,0,0,0.01,0,0,0,0.01, 0,0,0]
        piggy_back_train_order = 0  # <=0 => Never train on generated data
        test_subject = 5
        skip_trainin_viz = False
        classifier_rnn = False
        classifier_rnn_on_grnd_truth = False
        holden_dataset = False
        network_name = "LSTM3LR"
        load_weights_by_name = False

        # if not input_noise_sigma[0] == 0:
        #     zero_elm = [0]
        #     input_noise_sigma = zero_elm + input_noise_sigma
        try:
            opts, args = getopt.getopt(sys.argv[1:], "-h", ["help", "verbose=", "num_files=",
                                                            "input_dir_prefix=", "num_files=",
                                                            "neurons_this_layer=", "look_ahead_step=",
                                                            "validation_split=", "mini_seq_length=", "train_model",
                                                            "seed_seq_length=", "num_predicted_time_steps=",
                                                            "discard_prev_training", "num_epochs=",
                                                            "dont_save_model", "save_model_every_n_epoch=",
                                                            "output_folder=", "test_on_train_data",
                                                            "batch_size=", "stateful", "log_to_tensorboard",
                                                            "down_sample_in_time_by_n=", "file_type=",
                                                            "increasing_noise_training",
                                                            "piggy_back_train_order=", "test_subject=",
                                                            "skip_trainin_viz", "classifier_rnn", 
                                                            "classifier_rnn_on_grnd_truth", "holden_dataset",
                                                            "network_name=", "select_GPU_with_id=",
                                                            "input_dropout_rate=", "load_weights_by_name"])
        except getopt.GetoptError as err:
            # print help information and exit:
            print str(err)  # will print something like "option -a not recognized"
            usage()
            sys.exit(2)
        for o, a in opts:
            if o == "--verbose=":
                verbose = int(a)
            elif o in ("-h", "--help"):
                usage()
                sys.exit()
            elif o == "--input_dir_prefix":
                folder_path_to_data_files = a
            elif o == "--file_type":
                file_type = a
            elif o == "--num_files":
                num_files = int(a)
            elif o == "--neurons_this_layer":
                if flags_layer_size_option_first_call:
                    flags_layer_size_option_first_call = False
                    layer_sizes = []
                layer_sizes.append(int(a))
            elif o == "--look_ahead_step":
                print "This functionality need to be fixed. Particularly: In the trining time the error should be" \
                      " calculated after look_ahead_step number of predictions. The implementation is not that way at" \
                      " the moment. Use a value of 1 always for now, or just do not set the parameter."
                look_ahead_step = int(a)
            elif o == "--burn_in_length":
                burn_in_length = int(a)
            elif o == "--validation_split":
                validation_split = float(a)
            elif o == "--mini_seq_length":
                mini_seq_length = int(a)
            elif o == "--train_model":
                skip_training = False
            elif o == "--seed_seq_length":
                seed_seq_length = int(a)
            elif o == "--num_predicted_time_steps":
                num_predicted_time_steps = int(a)
            elif o == "--discard_prev_training":
                discard_prev_training = True
            elif o == "--num_epochs":
                num_epochs = int(a)
            elif o == "--dont_save_model":
                dont_save_model = True
            elif o == "--save_model_every_n_epoch":
                save_model_every_n_epoch = int(a)
            elif o == "--output_folder":
                output_folder = a
                if not (output_folder.endswith('/') or output_folder.endswith('\\')):
                    output_folder += '/'
            elif o == "--stateful":
                stateful = True
            elif o == "--test_on_train_data":
                test_on_train_data = True
            elif o == "--batch_size":
                batch_size = int(a)
            elif o == "--log_to_tensorboard":
                log_to_tensorboard = True
            elif o == "--down_sample_in_time_by_n":
                down_sample_in_time_by_n = int(a)
            elif o == "--increasing_noise_training":
                increasing_noise_training = True
            elif o == "--piggy_back_train_order":
                piggy_back_train_order = int(a)
            elif o == "--test_subject":
                test_subject = int(a)
            elif o == "--skip_trainin_viz":
                skip_trainin_viz = True
            elif o == "--classifier_rnn":
                classifier_rnn = True
            elif o == "--classifier_rnn_on_grnd_truth":
                classifier_rnn_on_grnd_truth = True
            elif o == "--parent_dir_to_negative_smpls":
                parent_dir_to_negative_smpls = a
            elif o == "--parent_dir_to_generated_smpls":
                parent_dir_to_generated_smpls = a
            elif o == "--holden_dataset":
                holden_dataset = True
            elif o == "--network_name":
                network_name = a
            elif o == "--select_GPU_with_id":
                select_GPU_with_id = int(a)
            elif o == "--input_dropout_rate":
                input_dropout_rate = float(a)
            elif o == "--load_weights_by_name":
                load_weights_by_name = True
            else:
                assert False, "unhandled option"
         # ------------------------------------ Input parameter validation ------------------------------------------#

        if select_GPU_with_id < 0:
            print "Fatal error: Either you haven't provided the select_GPU_with_id flag or given a negative value. " \
                  "If you have only one GPU you can provide 0 to this."
            exit(17)

        if discard_prev_training and skip_training :
            print "Fatal error: Requesting to discard previous training has been issued while training is not being " \
                  " done in the current effort. This setting is not feasible."
            exit(5)

        if stateful == False and skip_training:
            print "Fatal error: The model is always stateful in predic time / test time, since otherwise there \
                   is no way of performing pointby point prediction."
            exit(6)

        if stateful and batch_size > 1:
            print "Fatal error: This configuration is not yet supported"
            exit(7)

        if num_epochs < len(input_noise_sigma) and not skip_training and increasing_noise_training:
            print "Fatal error: num_epoch must be >= len(input_noise_sigma). " \
                  "Remember one zero noise epoch is always added in the beginning!"
            exit(8)

        # Select the GPU
        os.environ["CUDA_VISIBLE_DEVICES"] = str(select_GPU_with_id)

        if piggy_back_train_order > 0:
            num_epochs = int(num_epochs/(piggy_back_train_order + 1))
            if num_epochs <= 0:
                print "Fatal error: Too few number of epochs requested for the given number of piggy_back_train_order"
                exit(9)
        else:
            piggy_back_train_order = 0

        if num_files == -1 and not skip_training:
            load_preprocessing_params_from_file = False
        else:
            load_preprocessing_params_from_file = True

        if holden_dataset:
            input_provider = pre_post_processing.inputGeneratorHolden.InputProviderHolden(
                batch_size, validation_split, down_sample_in_time_by_n, num_files)
        else:
            input_provider = InputProvider(batch_size, validation_split, down_sample_in_time_by_n,
                                           load_preprocessing_params_from_file, seed_seq_length,
                                           test_subject, classifier_rnn, classifier_rnn_on_grnd_truth)
            input_provider.setEpocs(num_epochs)

            if not skip_training or test_on_train_data:
                input_provider.loadTimeSeriesData(folder_path_to_data_files, file_type, num_files)
                # if classifier_rnn:
                #     input_provider.loadNegativeSamples(parent_dir_to_negative_smpls, file_type, num_files)

        get_all_train_data_in_one_go = not stateful

        if not skip_training or test_on_train_data:
            train_data, target_labels, current_file_config = \
                input_provider.getTrainingSeq(mini_seq_length, get_all_train_data_in_one_go)

        # ----------------------TRAINING----------------------#
        callbacks = []
        lr_schedule = keras.callbacks.ReduceLROnPlateau(monitor='val_loss', factor=0.5, patience=3, verbose=1,
                                                        mode='min', epsilon=0.001, cooldown=0, min_lr=0)
        callbacks.append(lr_schedule)
        if not skip_training:
            if log_to_tensorboard:
                tb = keras.callbacks.TensorBoard(log_dir='./' + output_folder + '/log', histogram_freq=0,
                                                 write_graph=True, write_images=False)
                callbacks.append(tb)
            if save_model_every_n_epoch > 0:
                model_checkpoint = keras.callbacks.ModelCheckpoint(
                    'weights/model_weights.{epoch:02d}-{val_loss:.8f}.hdf5',
                    monitor='val_loss', save_weights_only=True)
                callbacks.append(model_checkpoint)
            # optimizer = keras.optimizers.SGD(lr=0.4, momentum=0.9, decay=0.0, nesterov=True, clipvalue=5)
            # optimizer = keras.optimizers.RMSprop(lr=0.00001, clipvalue=25, decay=0.05)
            # optimizer = keras.optimizers.Adagrad(lr=0.000001, epsilon=1e-08, decay=0.07)
            # optimizer = keras.optimizers.Adam(lr=0.0001, beta_1=0.9, beta_2=0.999, epsilon=1e-08, decay=0.1)
            # optimizer = keras.optimizers.Adadelta(lr=0.00001, rho=0.95, epsilon=1e-08, decay=0.0)
            optimizer = keras.optimizers.Adamax(lr=0.001, beta_1=0.9, beta_2=0.999, epsilon=1e-08, decay=0.0)
            # optimizer = keras.optimizers.Nadam(lr=0.0002, beta_1=0.9, beta_2=0.999, epsilon=1e-08, schedule_decay=0.004)

            if classifier_rnn or classifier_rnn_on_grnd_truth:
                loss = 'categorical_crossentropy'
            else:
                loss = 'mean_squared_error'
                # loss = 'mean_absolute_error'

            pose_sequence_learner = \
                PoseSquenceLearnerLSTM(input_dropout_rate,
                                       layer_sizes,
                                       train_data.shape[2],
                                       burn_in_length,
                                       batch_size,
                                       stateful,
                                       mini_seq_length - piggy_back_train_order * look_ahead_step,
                                       look_ahead_step,
                                       callbacks,
                                       classifier_rnn,
                                       holden_dataset,
                                       network_name,
                                       optimizer,
                                       loss)

            # pose_sequence_learner.loadWeights('autoEncoder.h5', by_name=True)
            # pose_sequence_learner.loadWeights('model_weights_LSTM.h5', by_name=True)
            # pose_sequence_learner.fixAutoEncoderWeights()
            if not discard_prev_training:
                # pose_sequence_learner.loadWeights('model_weightsDropOutDenseLayersBest.h5', by_name=True)
                # pose_sequence_learner.loadWeights('model_weights_LSTM3LR.h5', by_name=True)
                pose_sequence_learner.loadWeights('model_weights.h5', by_name=load_weights_by_name)

                # pose_sequence_learner.loadWeights('/home/partha/ait-ra-ship/python_models/savedWeights/expMap/'
                #                                   'TrainedOnallbut5/AutoEncoderOnly/dropOutEncoder/6/model_weights.h5',
                #                                   by_name=True)
                # pose_sequence_learner.loadWeights('/home/partha/ait-ra-ship/python_models/savedWeights/expMap/'
                #                                   'TrainedOnallbut5/LSTM3LR/increasinNoiseTrainingLSTM3LR/model_weights.h5',
                #                                   by_name=True)
                # exit(0)
                # pose_sequence_learner.freezeFirstNLayers(9)
                # pose_sequence_learner.freezelastNLayers(3)
                # pose_sequence_learner.freezeByid([0, 0, 1, 1, 0, 0])
                # pose_sequence_learner.freezeByid([1, 0, 0, 0, 0, 1])
                # pose_sequence_learner.freezeByid([1, 1, 1, 0, 1, 1, 0, 1])
                print "<---------------------------Model Loaded------------------------------------------->"

            if skip_trainin_viz:
                pose_sequence_learner.visualizeModel("")
            else:
                pose_sequence_learner.visualizeModel("model.png")
            count = 0
            while True:
                count += 1
                if get_all_train_data_in_one_go:
                    shuffle = not stateful

                    if increasing_noise_training:
                        num_epochs_per_noise_sigma = int(num_epochs/len(input_noise_sigma))
                        count_1 = 0
                        for sigma in input_noise_sigma:
                            count_1 += 1
                            hist = pose_sequence_learner.train(train_data, target_labels, validation_split,
                                                               num_epochs_per_noise_sigma, shuffle, sigma, -1)
                            if count_1 == 1:
                                train_hist = hist
                            else:
                                train_hist = numpy.vstack((train_hist, numpy.array(hist)))

                            print "<--------------" + \
                                  str((float(count_1))/len(input_noise_sigma)) +\
                                  "% Done of increasing noise training ----------->"
                    else:
                        sigma = 0
                        train_hist = pose_sequence_learner.train(train_data, target_labels, validation_split,
                                                                 num_epochs, shuffle, sigma, -1)

                    if piggy_back_train_order > 0:
                        sigma = 0
                        print "<----------------------- Strating piggy back training ----------------------->"
                        hist = pose_sequence_learner.train(train_data, [], validation_split, num_epochs,
                                                           shuffle, sigma, piggy_back_train_order)
                        train_hist = numpy.vstack((train_hist, numpy.array(hist)))
                else:  # fix this case piggy back is not happening in this part!!!
                    sigma = input_noise_sigma[0] # Fix this, getInputData must return epoch number and then calculate noise index
                    # Can't shuffle in stateful
                    hist = pose_sequence_learner.train(train_data, [], validation_split, 1, False, sigma,
                                                       -1)
                    # Accumulating history data
                    if count == 1:
                        train_hist = hist
                    else:
                        train_hist = numpy.vstack((train_hist, numpy.array(hist)))

                if save_model_every_n_epoch > 0 and (count % save_model_every_n_epoch) == 0:
                    pose_sequence_learner.saveWeights('model_weights.h5')

                print "<--------One file passed through. " + str(input_provider.getPercentComplete()) + "% done.-------->"
                if not input_provider.isDataAvailable() or get_all_train_data_in_one_go:
                    break
                pose_sequence_learner.resetModel()
                train_data, current_file_config = input_provider.getTrainingSeq(mini_seq_length, False)

            numpy.savetxt('training_history.csv', train_hist, delimiter=",")
            if not skip_trainin_viz:
                from plotting import visualiTrainingHistory
                visualiTrainingHistory(train_hist)
            # Save the model weights
            if not dont_save_model:
                pose_sequence_learner.saveWeights('model_weights.h5')

        #----------------------TESTING----------------------#
        # Load the same model for prediction. Except now the sequence length is only 1 for real time prediction
        if not holden_dataset:
            input_provider.resetCurrentFileReadHead()

        statefulness_in_test_time = True

        if classifier_rnn_on_grnd_truth or not classifier_rnn:
            parent_dir_to_generated_smpls = folder_path_to_data_files
        else:
            file_type = '.csv'

        test_data, current_file_config = input_provider.getTestSeq(seed_seq_length,
                                                                   parent_dir_to_generated_smpls,
                                                                   file_type, num_files)

        pose_sequence_prediction = PoseSquenceLearnerLSTM(input_dropout_rate,
                                                          layer_sizes,
                                                          test_data.shape[2],
                                                          burn_in_length,
                                                          1,
                                                          statefulness_in_test_time,
                                                          2,
                                                          look_ahead_step,
                                                          [],
                                                          classifier_rnn,
                                                          holden_dataset,
                                                          network_name)
        pose_sequence_prediction.loadWeights('model_weights.h5', by_name=False)

        output_post_processor = input_provider.getOutputPostProcessor()

        out_file_counter = 0
        while True:
            out_file_counter += 1
            if classifier_rnn:
                out_file_name = "categorical_" + str(out_file_counter) + ".txt"
            else:
                out_file_name = ""

            if test_on_train_data:
                test_data, current_file_config = input_provider.getTrainingSeq(mini_seq_length, False)

            current_file_config.network_name = network_name

            output_post_processor.setSeedOutputConfiguration(current_file_config)
            if not test_data.size:  # if test_data is empty
                break
            pose_sequence_prediction.predict(test_data, seed_seq_length, num_predicted_time_steps, output_post_processor)
            pose_sequence_prediction.writePredictionsToFile(output_post_processor, output_folder, out_file_name)
            print "<-----------------Test time saved file %d ----------------------------->" % out_file_counter
            pose_sequence_prediction.resetModel()
            if not test_on_train_data:
                test_data, current_file_config = input_provider.getTestSeq(seed_seq_length,
                                                                           parent_dir_to_generated_smpls,
                                                                           file_type, num_files)

        if not skip_training:
            raw_input("All operations finished. Press ENTER to terminate.")

    except(KeyboardInterrupt, SystemExit):
        print 'Interrupted because: ' 
        print sys.exc_info()[0]
        if not skip_training:
            # Save the model weights
            if not dont_save_model:
                pose_sequence_learner.saveWeights('model_weights.h5')
                print "<<<<<<<<<<<<<<<<Saved Model>>>>>>>>>>>>>>>>>>>>>>>"

            numpy.savetxt('training_history.csv', train_hist, delimiter=",")
            if not skip_trainin_viz:
                from plotting import visualiTrainingHistory
                visualiTrainingHistory(train_hist)
            raw_input("All operations finished. Press ENTER to terminate.")
if __name__ == "__main__":
    main()


