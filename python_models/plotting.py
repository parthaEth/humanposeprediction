#!/usr/bin/env python

# /*
#  * Created by Partha on 3/12/2016.
#  */

import matplotlib.pyplot as plt


def visualiTrainingHistory(train_hist):
    """This function plots training accuracy, loss, validation loss and validation loss for visualization"""
    train_steps = range(train_hist.shape[0])

    plt.figure()
    [a, b] = plt.plot(train_steps, train_hist[:, 0], 'r', train_steps, train_hist[:, 1], 'g')
    plt.legend([a, b], ['Training loss', 'Validation loss'])
    plt.show(block=False)
