#!/usr/bin/env python

# /*
#  * Created by Partha on 07/02/2017.
#  */

class InputProviderBase:
    """This is the base class for input pre processing.

    It can down sample, normalize and re-shape by removing redundent dimensions"""