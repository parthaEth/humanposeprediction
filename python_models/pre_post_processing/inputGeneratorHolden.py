#!/usr/bin/env python

# /*
#  * Created by Partha on 07/02/2017.
#  */

import numpy
import logging as LOG
import gflags
from gflags import FLAGS
import math
from outputPostProcessor import *

# TODO: Perhaps switch to glogs? See if CHECKs are availabe here
LOG.basicConfig(level=LOG.DEBUG)
# gflags.DEFINE_integer('down_sample_in_time_by_n', 2, '--down_sample_in_time_by_n=10 means every 10th data is used')
gflags.DEFINE_string('input_data_base_dir', '../Data/motionsynth_data/processed/', 'Location of the input data')


class InputProviderHolden:
    """Imports and performs downsampling tasks for holden dataset"""
    # TODO:(Partha) Extend it from the InputProviderBase
    def __init__(self, batch_size, validation_split, down_sample_in_time_by_n, num_files):
        self._X_train = []
        self._X_test = []
        self._mini_seq_length = 240
        self._mean_and_std_recomputable = False
        self._num_files = 1
        self._has_been_done_on_test = False
        self._has_been_done_on_train = False
        self._down_sample_in_time_by_n = down_sample_in_time_by_n
        self._num_files = num_files
        self._batch_size = batch_size
        self._validation_split = validation_split
        self.count_test_seqs_accessed = 0

    def _loadTrainDataFromFile(self, num_files):
        self._num_files = num_files
        if num_files == 1 or num_files == -1:
            self._X_train = (numpy.load(FLAGS.input_data_base_dir + 'data_hdm05.npz')['clips'])

        if num_files == -1:
            Xcmu = numpy.load(FLAGS.input_data_base_dir + 'data_cmu.npz')['clips']
            LOG.info("Done readig CMU dataset...")

            Xedin_mix = numpy.load(FLAGS.input_data_base_dir + 'data_edin_misc.npz')['clips']
            LOG.info("Done readig hdm05 dataset...")

            Xmhad = numpy.load(FLAGS.input_data_base_dir + 'data_mhad.npz')['clips']
            LOG.info("Done readig mhad dataset...")

            #Xstyletransfer = numpy.load('../Data/motionsynth_data/processed/data_styletransfer.npz')['clips']
            Xedin_xsens = numpy.load(FLAGS.input_data_base_dir + 'data_edin_xsens.npz')['clips']
            LOG.info("Done readig data_edin_xsens dataset...")

            Xedin_punching = numpy.load(FLAGS.input_data_base_dir + 'data_edin_punching.npz')['clips']
            LOG.info("Done readig data_edin_punching dataset...")

            self._mean_and_std_recomputable = True

            self._X_train = numpy.concatenate([Xcmu, Xedin_mix, Xmhad, Xedin_xsens, self._X_train, Xedin_punching],
                                              axis=0)
            # self._X_train = Xedin_mix
        self._X_train = self._X_train[:, :, 3:]

    def _loadTestDataFromFile(self):
        self._X_test = numpy.load(FLAGS.input_data_base_dir + 'data_edin_locomotion.npz')['clips']
        self._X_test = self._X_test[:, :, 3:]

    def _readjustForMiniSeqLength(self, mini_seq_length):
        if mini_seq_length > self._mini_seq_length:
            LOG.critical("Can not make bigger minisequence length than 240. Requested for %d", mini_seq_length)
        if mini_seq_length != self._mini_seq_length:
            temp = self._X_train[:, 0:mini_seq_length, :]
            current_index = mini_seq_length
            remaining_length = self._mini_seq_length - current_index

            while remaining_length > mini_seq_length:
                temp = numpy.concatenate((temp, self._X_train[:, current_index:current_index + mini_seq_length, :]))
                remaining_length = self._mini_seq_length - mini_seq_length
                current_index += mini_seq_length

        self._X_train = temp
        self._mini_seq_length = mini_seq_length

    def _downSampleIntime(self, sampling_factor, on_trian_data):
        """Considers every sampling_factor^th element"""
        LOG.info("The mini sequence length will be altered becasue of this operation")
        if sampling_factor <= 0:
            LOG.critical("sampling_factor must be positive. Got %d", sampling_factor)
        mini_seq_end_elm = sampling_factor * (self._mini_seq_length//sampling_factor) - 1

        if on_trian_data:
            temp = self._X_train[:, 0:mini_seq_end_elm:sampling_factor, :]
            for i in range(sampling_factor - 1):
                temp = numpy.concatenate((temp, self._X_train[:, i + 1:i + 1 + mini_seq_end_elm:sampling_factor, :]), axis=0)

            self._X_train = temp
            self._mini_seq_length = self._X_train.shape[1]
        else:
            temp = self._X_test[:, 0:mini_seq_end_elm:sampling_factor, :]
            for i in range(sampling_factor - 1):
                temp = numpy.concatenate((temp, self._X_test[:, i + 1:i + 1 + mini_seq_end_elm:sampling_factor, :]),
                                         axis=0)

            self._X_test = temp
            self._mini_seq_length = self._X_test.shape[1]

    def _normalizeData(self, test_data_only):
        # feet = numpy.array([12, 13, 14, 15, 16, 17, 24, 25, 26, 27, 28, 29])
        if self._num_files == -1 and (not self._has_been_done_on_train) and not test_data_only:
            self._Xmin = numpy.reshape(self._X_train, [-1, self._X_train.shape[2]]).min(axis=0)

            self._Xmax = numpy.reshape(self._X_train, [-1, self._X_train.shape[2]]).max(axis=0)
            self._Xmax -= self._Xmin
            numpy.savez_compressed('mean_and_std.npz', Xmin=self._Xmin, Xmax=self._Xmax)
        else:
            min_max = numpy.load('mean_and_std.npz')
            self._Xmin, self._Xmax = min_max['Xmin'], min_max['Xmax']

        if not self._has_been_done_on_train and not test_data_only:
            self._X_train = (self._X_train - self._Xmin) / self._Xmax
            self._has_been_done_on_train = False

        if not self._has_been_done_on_test and (len(self._X_test) != 0):
            self._X_test = (self._X_test - self._Xmin) / self._Xmax
            self._has_been_done_on_test = False

    def checkTrainDatasizeConstraints(self):
        num_mini_seq = self._X_train.shape[0]
        validation_data_len = int(math.ceil(num_mini_seq * self._validation_split))
        train_data_len = int(round(num_mini_seq - validation_data_len))
        if train_data_len % self._batch_size > 0 or validation_data_len % self._batch_size > 0:
            print "Fatal error: Batch size must divide number of all sequences available. num_mini_seq = " + \
                  str(train_data_len) + " validation_data_len = " + str(validation_data_len) + " and batch_size = " \
                  + str(self._batch_size)
            exit(8)

    def getTrainDataMutable(self, mini_seq_length):
        if len(self._X_train) == 0:
            self._loadTrainDataFromFile(self._num_files)
            # self._downSampleIntime(FLAGS.down_sample_in_time_by_n, on_trian_data=True)
            self._downSampleIntime(self._down_sample_in_time_by_n, on_trian_data=True)
            self._normalizeData(False)
            self.checkTrainDatasizeConstraints()
            if mini_seq_length > self._X_train.shape[1]:
                LOG.critical("Required minisequence length is too long")
        return self._X_train
    
    def getOutputPostProcessor(self):
        output_post_processor = OutputPostProcessor(self._Xmin[:-4], self._Xmax[:-4], numpy.zeros(7),
                                                    numpy.array([0, 1, 2, 69, 70, 71, 72]))
        return output_post_processor

    def getTestDataMutable(self):
        if len(self._X_test) == 0:
            self._loadTestDataFromFile()
            self._downSampleIntime(self._down_sample_in_time_by_n, on_trian_data=False)
            self._normalizeData(True)
        return self._X_test

    def getTrainingSeq(self, mini_seq_length, get_all_train_data_in_one_go):
        current_file_config = OutputFileConfiguration()
        return self.getTrainDataMutable(mini_seq_length), [], current_file_config

    def getTestSeq(self, required_seq_length, base_folder_path, file_type, num_files_toread=-1):
        if len(self._X_test) == 0:
            self._loadTestDataFromFile()
            self._downSampleIntime(self._down_sample_in_time_by_n, on_trian_data=False)
            self._normalizeData(True)
        self.count_test_seqs_accessed += 1
        if required_seq_length > self._X_test.shape[1]:
            LOG.critical("Required minisequence length is too long")

        current_file_config = OutputFileConfiguration('../Data/motionsynth_data/processed/data_edin_locomotion.npz',
                                                      self.count_test_seqs_accessed - 1, required_seq_length, fps=30,
                                                      down_smapling_rate=self._down_sample_in_time_by_n)

        return self._X_test[self.count_test_seqs_accessed -1 : self.count_test_seqs_accessed, :, :], current_file_config

    def getPercentComplete(self):
        return 100

    def isDataAvailable(self):
        return False