#!/usr/bin/env python

# /*
#  * Created by Partha on 17/11/2016.
#  */

import numpy
import os.path


class OutputPostProcessor:
    def __init__(self, shift_vector, scale_vector, deleted_column_vals, deleted_column_idxes):
        """shift_vector -- is the mean vector by which the data has been shifted in order to achieve zero centric data
           scale_vector -- is the scale by which the feaures have been scaled in order to achieve 0-1 bound
           deleted_column_vals -- values of the columns that have been deleted. The deleted columns are constants
           deleted_column_idxes -- Indexes of the columns that have been deleted"""
        self._current_file_number = 1
        self._file_type = ".csv"
        self._all_data = numpy.array([[]])
        self._all_data_in_original_dim = numpy.array([[]])
        self._first_call_to_accum_func = True
        self._shift_vector = shift_vector
        self._scale_vector = scale_vector
        self._deleted_column_vals = deleted_column_vals
        self._deleted_column_idxes = deleted_column_idxes
        self._output_file_configuration = OutputFileConfiguration()

    def rescaleAndReinsertConstColsAllVals(self):
        self._all_data *= self._scale_vector
        self._all_data += self._shift_vector

        self._all_data_in_original_dim = numpy.zeros((self._all_data.shape[0],
                                                      len(self._deleted_column_idxes) + self._all_data.shape[1]))

        deleted_col_num = 0
        useful_data_col_num = 0
        for col in range(self._all_data_in_original_dim.shape[1]):
            if deleted_col_num < len(self._deleted_column_idxes) and \
               col == self._deleted_column_idxes[deleted_col_num]:
                self._all_data_in_original_dim[:, col] = self._deleted_column_vals[deleted_col_num]
                deleted_col_num += 1
            else:
                self._all_data_in_original_dim[:, col] = self._all_data[:, useful_data_col_num]
                useful_data_col_num += 1

    def convertReltivePositionToAbsolute(self):
        for row in range(len(self._all_data) - 1):
            self._all_data[row + 1, :2] += self._all_data[row, :2]

    def clearAccumulationState(self):
        self._all_data = []
        self._all_data_in_original_dim = []
        self._first_call_to_accum_func = True

    def dumpRawAccumulatedVals(self, filename="", dest_dir_path=""):
        self._all_data_in_original_dim = self._all_data
        self.writeAccumulatedDataToDisk(filename, dest_dir_path)
        self.clearAccumulationState()

    def dumpRescaledAndResizedVals(self, filename="", dest_dir_path=""):
        if not filename == "":
            if not (filename[-4:] == ".csv" or filename[-4:] == ".txt"):
                print "Fileformat not supported. File name given:" + filename
                exit(4)
        self.rescaleAndReinsertConstColsAllVals()
        # self.convertReltivePositionToAbsolute()
        self.writeAccumulatedDataToDisk(filename, dest_dir_path)
        self.clearAccumulationState()

    def setSeedOutputConfiguration(self, output_file_configuration):
        self._output_file_configuration = output_file_configuration

    def accumulateTimeseriesRow(self, feature_vector):
        """Receives and records a row of a time series and finally builds a time series matrix of the dimension
        (time_steps, feature_size)"""
        if self._first_call_to_accum_func:
            self._all_data = feature_vector[:]
            self._first_call_to_accum_func = False
        else:
            self._all_data = numpy.vstack((self._all_data, feature_vector))

    def appendHeaderToOutputFile(self, file_path_and_name):
        if os.path.exists(file_path_and_name):
            os.remove(file_path_and_name)

        file_handle = open(file_path_and_name, 'ab')
        file_handle.write("seed_seq_origination_file = " +
                          self._output_file_configuration.seed_seq_origination_file + "\n\r")
        file_handle.write("seed_seq_start_row = " + str(self._output_file_configuration.seed_seq_start_row) + "\n\r")
        file_handle.write("seed_seq_len = " + str(self._output_file_configuration.seed_seq_len) + "\n\r")
        file_handle.write("FPS = " + str(self._output_file_configuration.fps) + "\n\r")
        file_handle.write("Down_sampling_rate = " + str(self._output_file_configuration.down_sampling_rate) + "\n\r")
        file_handle.write("network_name = " + str(self._output_file_configuration.network_name) + "\n\r")
        return file_handle

    def writeAccumulatedDataToDisk(self, filename="", dest_dir_path=""):
        if filename == "":
            filename = str(self._current_file_number)+self._file_type

        if not dest_dir_path == "":
            if not dest_dir_path[:-1] == "/":
                dest_dir_path += "/"

        filename = dest_dir_path + filename

        file_handle = self.appendHeaderToOutputFile(filename)

        numpy.savetxt(file_handle, self._all_data_in_original_dim, delimiter=",")
        self._current_file_number += 1
        file_handle.close()

    def getRescaledAndResized(self):
        self.rescaleAndReinsertConstColsAllVals()
        temp = self._all_data_in_original_dim
        self.clearAccumulationState()
        return temp

class OutputFileConfiguration:

    def __init__(self, seed_seq_origination_file="Unknown",
                 seed_seq_start_row=0, seed_seq_len=0, fps=0, down_smapling_rate=1,
                 network_name="Unknown"):
        self.seed_seq_origination_file = seed_seq_origination_file
        self.seed_seq_start_row = seed_seq_start_row
        self.fps = fps
        self.seed_seq_len = seed_seq_len
        self.down_sampling_rate = down_smapling_rate
        self.network_name = network_name