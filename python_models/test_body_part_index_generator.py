from LSTM_fr_pose import *

spine_idx, l_arm_idx, r_arm_idx, l_leg_idx, r_leg_idx = getBodyPartIndices("Holden")

spine_idx_expect = [27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 0,  1,  2, 63, 64, 65]
l_arm_idx_expect = [39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50]
r_arm_idx_expect = [51, 52, 53, 54, 55, 56, 57, 58, 59, 60, 61, 62]
l_leg_idx_expect = [3,  4,  5,  6,  7,  8,  9, 10, 11, 12, 13, 14]
r_leg_idx_expect = [15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26]

if spine_idx_expect != spine_idx:
    print "spine indices don't match ! spine_idx_expect = " + str(spine_idx_expect) + " wehreas spine_idx = " \
          + str(spine_idx)

if l_arm_idx_expect != l_arm_idx:
    print "left arm indices don't match ! l_arm_idx_expect = " + str(l_arm_idx_expect) + " wehreas l_arm_idx = " \
          + str(l_arm_idx)

if r_arm_idx_expect != r_arm_idx:
    print "Right arm indices don't match ! r_arm_idx_expect = " + str(r_arm_idx_expect) + " wehreas r_arm_idx = " \
          + str(r_arm_idx)

if l_leg_idx_expect != l_leg_idx:
    print "Left leg indices don't match ! l_leg_idx_expect = " + str(l_leg_idx_expect) + " wehreas l_leg_idx = " \
          + str(l_leg_idx)

if r_leg_idx_expect != r_leg_idx:
    print "Right leg indices don't match ! r_leg_idx_expect = " + str(r_leg_idx_expect) + " wehreas r_leg_idx = " \
          + str(r_leg_idx)

print "Test Passed"


# Inverse map generation
forward_map = spine_idx_expect + l_arm_idx_expect + r_arm_idx_expect + l_leg_idx_expect + r_leg_idx_expect

reverse_map = []
for i in range(66):
    for j in range(len(forward_map)):
        if i == forward_map[j]:
            reverse_map.append(j)
            break

print reverse_map
print forward_map