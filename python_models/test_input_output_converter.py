#!/usr/bin/env python

# /*
#  * Created by Partha on 27/11/2016.
#  */

from inputGenerator import *

folder_path_to_data_files = "../Data"
file_type = ".csv"
num_files = 10
mini_seq_length = 200
seed_seq_length = 100

input_provider = InputProvider()
input_provider.loadTimeSeriesData(folder_path_to_data_files, file_type, num_files)

output_post_processor = input_provider.getOutputPostProcessor()
# train_data = input_provider.getTrainingSeq(mini_seq_length)

train_data = input_provider.getTestSeq(mini_seq_length, seed_seq_length)

for seq in range(train_data.shape[0]):
    for time_step in range(train_data.shape[1]):
        output_post_processor.accumulateTimeseriesRow(train_data[seq, time_step, :])

output_post_processor.dumpRescaledAndResizedVals()
