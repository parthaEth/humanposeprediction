#!/usr/bin/env python

# /*
#  * Created by Partha on 26/11/2016.
#  */

import numpy
from inputGenerator import *

data = numpy.array([[1, 2, 3, 4], [5, 6, 7, 8], [9, 9, 9, 9], [1, 2, 3, 4], [1, 2, 3, 4], [1, 2, 3, 4],\
                   [1, 2, 3, 4], [1, 2, 3, 4], [1, 2, 3, 4]]) * 1.0

file_attribs1 = FilesAndNumberOfPoses("file1", 1, 0, 3)
file_attribs2 = FilesAndNumberOfPoses("file2", 2, 3, 9)

input_provider = InputProvider(data, (file_attribs1, file_attribs2))
input_provider.makePositionFrameIndependent()
print input_provider._all_poses