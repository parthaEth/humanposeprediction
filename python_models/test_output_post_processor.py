#!/usr/bin/env python

# /*
#  * Created by Partha on 17/11/2016.
#  */

import numpy
from outputPostProcessor import *


shift_vector = numpy.array([1, 2, 3, 4])
scale_vector = numpy.array([1, 2, 3, 4])
deleted_column_vals = numpy.array([1, 2, 3, 4])
deleted_column_idxes = numpy.array([1, 2, 3, 4])

output_post_processor = OutputPostProcessor(shift_vector, scale_vector, deleted_column_vals, deleted_column_idxes)

for i in range(9):
    output_post_processor.accumulateTimeseriesRow(numpy.array([1, 2, 3, 4]))

output_post_processor.dumpRescaledAndResizedVals("dummy_data.csv")
