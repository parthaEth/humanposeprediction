#!/usr/bin/env python

# /*
#  * Created by Partha on 10/11/2016.
#  */

from LSTM_fr_pose import *
import numpy

pose_sequence_learner = PoseSquenceLearnerLSTM([10], 1)
pose_sequence_learner.train(numpy.array([[1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12]]).T, 3)
