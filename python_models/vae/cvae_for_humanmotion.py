#!/usr/bin/env python

# /*
#  * Created by Partha on 12/11/2016.
#  */
"""This script trains and tests a conditional variational auto encoder (CVAE) or a VAE for human motion modeling"""
# TODO(Partha) - Split the functionality of a VAE and a CVAE into two different classes
import numpy
import glog as log

from keras.layers import TimeDistributedDense, Lambda, LSTM, Input, merge
from keras.models import Model
from keras import backend as K
from keras import objectives
from keras.models import Sequential
from keras.regularizers import l2
import gflags


FLAGS = gflags.FLAGS
gflags.DEFINE_float('W_regularizer_val', 0.0000001, 'l2 regularization factor')
gflags.DEFINE_float('epsilon_std', 1.0, 'The standard deviation of the noise sample')
gflags.DEFINE_float('kl_loss_2_data_loss_ratio', 0.5, 'Ratio to combine the two losses. '
                    'loss  = kl_loss_2_data_loss_ratio * kl_loss + data_loss * (1-kl_loss)')

gflags.DEFINE_integer('latent_dim', 1000, 'Latent space dimension')


class CVAEForHumanPose:
    """Creates and lets one train a Conditional Variational Auto encoder with an RNN generating conditions"""
    def __init__(self, dense_layer_sizes, rnn_layer_sizes, original_dim, batch_size,
                 seq_length, cvae, stateful, training, optimizer):
        self._original_dim = original_dim
        self._batch_size = batch_size
        self._trainable_length = seq_length - 1
        self._cvae = cvae
        self._training = training
        self._cvae = cvae
        self._stateful = stateful
        self._last_noise_sample = 0

        log.check_ge(len(dense_layer_sizes), 0, 'The network needs to have atleast one layer')
        log.check(stateful or training, 'The model is always stateful in test time')
        log.check_ge(FLAGS.kl_loss_2_data_loss_ratio, 0, 'This ratio should not be negative')
        log.check_le(FLAGS.kl_loss_2_data_loss_ratio, 1, 'This parameter should\'nt be > 1, see the formula to see why')

        batch_input_shape = (batch_size, self._trainable_length, original_dim)
        noise_input_shape = (batch_size, self._trainable_length, FLAGS.latent_dim)

        current_pose_input = Input(batch_shape=batch_input_shape, dtype='float32', name='current_pose_input')

        if training:
            # Encoder
            # encoder = merge([current_pose_input, conditioner], mode='concat')
            encoder = current_pose_input
            count_encoder_layer = 1
            for neuron_this_layer in dense_layer_sizes:
                encoder = TimeDistributedDense(neuron_this_layer,
                                     batch_input_shape=batch_input_shape,
                                     activation='relu', name='encoder_' + str(count_encoder_layer))(encoder)
                count_encoder_layer += 1

            # Calculating the mean and log variance of the random variable
            self._q_z_mean = TimeDistributedDense(FLAGS.latent_dim, batch_input_shape=batch_input_shape,
                                                  activation='relu', name='q_mu')(encoder)
            self._q_z_log_var = TimeDistributedDense(FLAGS.latent_dim, batch_input_shape=batch_input_shape,
                                                     activation='relu', name='q_sigma')(encoder)

            # note that "output_shape" isn't necessary with the TensorFlow backend
            self._z = Lambda(self.sampling, batch_input_shape=batch_input_shape)([self._q_z_mean, self._q_z_log_var])

            if cvae:
                prev_pose_input = Input(batch_shape=batch_input_shape, dtype='float32', name='prev_pose_input')
                prev_pose_and_noise = merge([prev_pose_input, self._z], mode='concat')
        else:
            if cvae:
                prev_pose_input = Input(batch_shape=batch_input_shape, dtype='float32', name='prev_pose_input')
                noise_input = Input(batch_shape=noise_input_shape, dtype='float32', name='noise_input')
                prev_pose_and_noise = merge([prev_pose_input, noise_input], mode='concat')

        if cvae:
            # Conditioner
            conditioner = prev_pose_and_noise
            count = 1

            # conditioner = TimeDistributedDense(1000, batch_input_shape=batch_input_shape,
            #                                    activation='relu', name='conditionar_dense_0')(conditioner)
            # conditioner = TimeDistributedDense(1000, batch_input_shape=batch_input_shape,
            #                                    activation='relu', name='conditionar_dense_1')(conditioner)
            for neuron_this_layer in rnn_layer_sizes:
                conditioner = LSTM(neuron_this_layer, return_sequences=True, inner_init='orthogonal',
                                   batch_input_shape=batch_input_shape, stateful=stateful,
                                   W_regularizer=l2(FLAGS.W_regularizer_val),
                                   name='conditioner_' + str(count))(conditioner)
                count += 1

            self._p_z_mean = TimeDistributedDense(FLAGS.latent_dim, batch_input_shape=batch_input_shape,
                                                  activation='relu', name='p_mu')(conditioner)
            self._p_z_log_var = TimeDistributedDense(FLAGS.latent_dim, batch_input_shape=batch_input_shape,
                                                     activation='relu', name='p_sigma')(conditioner)

        if not training and cvae:
            self._z = Lambda(self.sampling, batch_input_shape=batch_input_shape)([self._p_z_mean, self._p_z_log_var])
        elif not training and not cvae:
            self._z = Input(batch_shape=batch_input_shape, dtype='float32', name='std_normal_noise')

        # Decoder
        count_decoder_layer = 1
        if cvae:
            decoder = merge([self._z, conditioner], mode='concat')
            # decoder = z
        else:
            decoder = self._z

        for neuron_this_layer in dense_layer_sizes:
            decoder = TimeDistributedDense(neuron_this_layer, activation='relu', batch_input_shape=batch_input_shape,
                                           name='Decoder_' + str(count_decoder_layer))(decoder)
            count_decoder_layer += 1

        predicted_output = TimeDistributedDense(original_dim, activation='sigmoid', batch_input_shape=batch_input_shape,
                                                name='predicted_output')(decoder)
        if cvae:
            if training:
                kl_loss = Lambda(self.latentTrajectorySmoothing,
                                 batch_input_shape=batch_input_shape,
                                 name='kl_loss')([self._p_z_mean, self._p_z_log_var, self._q_z_mean, self._q_z_log_var])
                self.vae = Model(input=[current_pose_input, prev_pose_input], output=[predicted_output, kl_loss])
                self.vae.compile(optimizer=optimizer, loss={'predicted_output': 'mse', 'kl_loss': self.klDivLoss},
                                 loss_weights=[1.0-FLAGS.kl_loss_2_data_loss_ratio, FLAGS.kl_loss_2_data_loss_ratio])
            else:
                self.vae = Model(input=[prev_pose_input, noise_input], output=[predicted_output, self._z])
                self.vae.compile(optimizer=optimizer, loss='mse')
        else:
            if training:
                self.vae = Model(input=current_pose_input, output=predicted_output)
                self.vae.compile(optimizer=optimizer, loss=self.vaeLoss)
            else:
                self.vae = Model(input=self._z, output=predicted_output)
                self.vae.compile(optimizer=optimizer, loss='mse')

        self.vae.summary()

    def sampling(self, args):
        z_mean, z_log_var = args
        epsilon = K.random_normal(shape=(self._batch_size, self._trainable_length, FLAGS.latent_dim), mean=0.,
                                  std=FLAGS.epsilon_std)
        self._current_noise_sample = z_mean + K.exp(z_log_var / 2) * epsilon
        return self._current_noise_sample

    def latentTrajectorySmoothing(self, args):
        kl_loss = self.multivariteKLDiv(args)
        smoothness_loss = K.sum(K.square(self._z - self._last_noise_sample), axis=-1) * 1e-5
        self._last_noise_sample = self._z
        return 0.5 * smoothness_loss + 0.5 * kl_loss

    def multivariteKLDiv(self, args):
        p_z_mean, p_z_log_var, q_z_mean, q_z_log_var = args
        p_z_var = K.exp(p_z_log_var)
        q_z_var = K.exp(q_z_log_var)
        return - 0.5 * (FLAGS.latent_dim + K.sum(-q_z_log_var + p_z_log_var - (p_z_var/ q_z_var)
                                                 - (K.square(q_z_mean - p_z_mean) / q_z_var), axis=-1))

    def klDivLoss(self, kldiv_desired, kldiv):
        return kldiv

    def vaeLoss(self, x, x_decoded_mean):
        xent_loss = self._original_dim * objectives.mean_squared_error(x, x_decoded_mean)
        kl_loss = - 0.5 * K.sum(1 + self._q_z_log_var - K.square(self._q_z_mean) - K.exp(self._q_z_log_var), axis=-1)
        return xent_loss(1 - FLAGS.kl_loss_2_data_loss_ratio) + kl_loss * FLAGS.kl_loss_2_data_loss_ratio

    def train(self, x_train, nb_epoch, validation_split, callbacks):
        """train the VAE on MNIST digits"""
        hist = numpy.zeros((nb_epoch, 2))
        if self._cvae:
            training_hist = self.vae.fit([x_train[:, 1:self._trainable_length + 1, :],
                                          x_train[:, 0:self._trainable_length, :]],
                                         [x_train[:, 1:self._trainable_length + 1, :],
                                          numpy.zeros((x_train.shape[0], self._trainable_length))],
                                         shuffle=True, nb_epoch=nb_epoch, batch_size=self._batch_size,
                                         validation_split=validation_split, callbacks=callbacks)
        else:
            training_hist = self.vae.fit(x_train, x_train, shuffle=True, nb_epoch=nb_epoch, callbacks=callbacks,
                                         batch_size=self._batch_size, validation_split=validation_split)

        hist[:, 0] = numpy.array(training_hist.history['loss']).T
        hist[:, 1] = numpy.array(training_hist.history['val_loss']).T

        return hist

    def predict(self, test_data, seed_seq_length, num_predicted_time_steps, output_post_processor):
        """test_data must have the shape (1, num_time_steps, features)"""
        log.check(not self._training, "Predict called on training model")
        log.check_ge(test_data.shape[1], seed_seq_length, "Requested seed is longer than data available test_data.shape"
                                                          " = " + str(test_data.shape) + " requested sseed length = " +
                                                          str(seed_seq_length))
        if self._cvae:
            for time_step in range(seed_seq_length):
                # seed here
                z = numpy.zeros((1, 1, FLAGS.latent_dim))
                predicted_seq, z = \
                    self.vae.predict([test_data[0:1, time_step:time_step + 1, :], z])
                # output_post_processor.accumulateTimeseriesRow(test_data[seq_ind, mini_seq_time_indx, :])
                output_post_processor.accumulateTimeseriesRow(predicted_seq[0, :, :])

            #  Prediction extrapolation
            for time_step in range(num_predicted_time_steps):
                predicted_seq, z = self.vae.predict([predicted_seq, z])
                output_post_processor.accumulateTimeseriesRow(predicted_seq[0, :, :])
        else:
            if test_data:
                for time_step in range(seed_seq_length):
                    # seed here
                    predicted_seq = \
                        self.vae.predict(test_data[0:1, time_step:time_step + 1, :])
                    # output_post_processor.accumulateTimeseriesRow(test_data[seq_ind, mini_seq_time_indx, :])
                    output_post_processor.accumulateTimeseriesRow(predicted_seq[0, :, :])
            else:
                for time_step in range(seed_seq_length):
                    # seed here
                    predicted_seq = \
                        self.vae.predict(numpy.random.normal(loc=0.0, scale=FLAGS.epsilon_std,
                                                                size=(1, 1, self._original_dim)))
                    # output_post_processor.accumulateTimeseriesRow(test_data[seq_ind, mini_seq_time_indx, :])
                    output_post_processor.accumulateTimeseriesRow(predicted_seq[0, :, :])

    def loadWeights(self, model_name, by_name):
        self.vae.load_weights(model_name, by_name)

    def saveWeights(self, model_name):
        self.vae.save_weights(model_name)

    def visualizeModel(self, output_file_name):
        """To view the model"""
        from keras.utils.visualize_util import plot
        plot(self.vae, to_file = output_file_name)

    def resetModel(self):
        if self._stateful:
            self.vae.reset_states()
        else:
            log.error("Reset state is called on a non stateful RNN")

    @staticmethod
    def writePredictionsToFile(output_post_processor, dest_dir="", filename=""):
        output_post_processor.dumpRescaledAndResizedVals(filename, dest_dir)