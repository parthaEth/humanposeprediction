from cvae_for_humanmotion import *
import numpy
from keras import backend as K

dense_layer_sizes = [100, 102]
rnn_layer_sizes = [150, 153]
original_dim = 52
latent_dim = 2
batch_size = 5
seq_length= 100
epsilon_std = 0.1
cvae = True
stateful = False

sample_inst = CVAEForHumanPose(dense_layer_sizes, rnn_layer_sizes, original_dim, latent_dim, batch_size,
                               seq_length, epsilon_std, cvae, stateful)

# sample_inst.visualizeModel('CVAE_train.png')

p_z_mean = K.variable(value=numpy.array([1.0, 2.0]))
p_z_log_var = K.variable(value=numpy.array([0.5, 0.6]))
q_z_mean = K.variable(value=numpy.array([1.0, 2.0]))
q_z_log_var = K.variable(value=numpy.array([0.5, 0.6]))

kl_div_calculated= sample_inst.multivariteKLDiv([p_z_mean, p_z_log_var, q_z_mean, q_z_log_var])

kl_div_expected = 0.0

assert K.eval((K.abs(kl_div_expected - kl_div_calculated) < 1e-6)), \
    'The expected valis ' + str(kl_div_expected) + ' but the calculated one is ' + str(K.eval(kl_div_calculated)) + \
    ' instead'


p_z_mean = K.variable(value=numpy.array([1.0, 2.0]))
p_z_log_var = K.variable(value=numpy.array([0.5, 0.6]))
q_z_mean = K.variable(value=numpy.array([2.0, 1.0]))
q_z_log_var = K.variable(value=numpy.array([0.6, 0.5]))

kl_div_calculated= sample_inst.multivariteKLDiv([p_z_mean, p_z_log_var, q_z_mean, q_z_log_var])

kl_div_expected = 0.582675315959133

assert K.eval((K.abs(kl_div_expected - kl_div_calculated) < 1e-6)), \
    'The expected valis ' + str(kl_div_expected) + ' but the calculated one is ' + str(K.eval(kl_div_calculated)) + \
    ' instead'

print "Test passed"

