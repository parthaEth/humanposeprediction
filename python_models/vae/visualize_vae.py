from cvae_for_humanmotion import *

dense_layer_sizes = [100, 102, 11, 23]
rnn_layer_sizes = [150, 153, 12, 13]
original_dim = 52
latent_dim = 1000
batch_size = 5
seq_length= 100
epsilon_std = 0.1
cvae = False
training = True

# sample_inst = CVAEForHumanPose(dense_layer_sizes, rnn_layer_sizes, original_dim, latent_dim, batch_size,
#                  seq_length, epsilon_std, cvae, False, training)
#
# sample_inst.visualizeModel('VAE_train.png')
#
# sample_inst = CVAEForHumanPose(dense_layer_sizes, rnn_layer_sizes, original_dim, latent_dim, batch_size,
#                  seq_length, epsilon_std, cvae, True, not training)
#
# sample_inst.visualizeModel('VAE_test.png')

cvae = True

sample_inst = CVAEForHumanPose(dense_layer_sizes, rnn_layer_sizes, original_dim, batch_size,
                               seq_length, cvae, False, training, 'rmsprop')

sample_inst.visualizeModel('CVAE_train.png')

sample_inst = CVAEForHumanPose(dense_layer_sizes, rnn_layer_sizes, original_dim, batch_size,
                               seq_length, cvae, True, not training, 'rmsprop')

sample_inst.visualizeModel('CVAE_test.png')