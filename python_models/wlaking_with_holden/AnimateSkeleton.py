#!/usr/bin/env python

# /*
#  * Created by Partha on 04/02/2017.
#  */

import sys
import os
import numpy
import parseHeader
import gflags
from gflags import FLAGS

sys.path.append('../../providedCode/holden/nn')
sys.path.append('../../providedCode/holden/motion')

from AnimationPlot import animation_plot

gflags.DEFINE_string('movie_file_name', None, 'Specify a file name if you would like the animation to be saved')
gflags.DEFINE_string('file_name_to_animate', '3.csv', 'Specify a file name to animate the file naes are usually'
                                                      ' are of the form n.csv, where n is an integer')

gflags.DEFINE_boolean('animate_alongside_original', False, 'If the animation would happen alongside original')
gflags.DEFINE_boolean('donot_change_color', False, 'If the character would be colored differently after seed sequence')
gflags.DEFINE_boolean('animate_groundtruth', False, 'Animate Groundtruth')


gflags.DEFINE_list('frames_to_save', ['0', '3', '9', '15', '30'], 'List of frame Indices to save')

def parseToListOfInts(list_indxs):
    indxs = []
    for idxs in list_indxs:
        indxs.append(int(idxs))

    return indxs

def animateSkeletonSideBySide(file_name_and_path, movei_file_name=None):
    # Load Generated data
    X_generated = numpy.genfromtxt(file_name_and_path, skip_header=11, delimiter=',')
    X_generated = X_generated[None, :]
    X_generated = numpy.swapaxes(X_generated, 1, 2)

    # Load Original data
    original_file_name, seq_star_row, seq_length, dwon_sampling_rate, FPS, _ = parseHeader.parseHeader(file_name_and_path)
    X_original = numpy.load("../" + original_file_name)['clips']
    seq_idx = seq_star_row // dwon_sampling_rate
    time_idx = seq_star_row % dwon_sampling_rate

    X_original = X_original[seq_idx:seq_idx + 1, time_idx:-1:dwon_sampling_rate, :]
    X_original = numpy.swapaxes(X_original, 1, 2)

    comparable_length = numpy.minimum(X_generated.shape[-1], X_original.shape[-1])

    if FLAGS.donot_change_color:
        seq_length = -1

    frames_to_save = parseToListOfInts(FLAGS.frames_to_save)

    if FLAGS.animate_alongside_original:
        animation_plot([X_generated[:, :, :comparable_length], X_original[:, :, :comparable_length]],
                       interval=(1000.0/FPS), change_color_after=seq_length, frames_to_save=frames_to_save)
    else:
        if FLAGS.animate_groundtruth:
            animation_plot([X_original], interval=(1000.0 / FPS), filename=movei_file_name,
                           change_color_after=seq_length, frames_to_save=frames_to_save)
        else:
            animation_plot([X_generated], interval=(1000.0 / FPS), filename=movei_file_name,
                           change_color_after=seq_length, frames_to_save=frames_to_save)


if __name__ == "__main__":
    FLAGS(sys.argv)  # parse flags
    print animateSkeletonSideBySide('../GeneratedSequences/' + FLAGS.file_name_to_animate, FLAGS.movie_file_name)

    if FLAGS.animate_groundtruth and FLAGS.animate_alongside_original:
        print "Fatal error: Confusing Input arguments"
        exit(0)

    for temp_file in os.listdir('./'):
        if temp_file[-4:] == '.png' and temp_file[:4] == '_tmp':
            os.remove("./" + temp_file)