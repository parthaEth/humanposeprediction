#!/usr/bin/env python

# /*
#  * Created by Partha on 15/02/2017.
#  */


def parseHeader(file_name_with_path):
    file_handle = open(file_name_with_path, 'r')

    line = file_handle.readline()
    parts = line.split(' ')
    original_file_name = parts[2][:-1]

    line = file_handle.readline()
    parts = line.split(' ')
    seq_star_row = int(parts[2])

    line = file_handle.readline()
    parts = line.split(' ')
    seq_length = int(parts[2])

    line = file_handle.readline()
    parts = line.split(' ')
    FPS = float(parts[2])

    line = file_handle.readline()
    parts = line.split(' ')
    dwon_sampling_rate = int(parts[2])

    line = file_handle.readline()
    parts = line.split(' ')
    network_name = parts[2][:-1]

    return original_file_name, seq_star_row, seq_length, dwon_sampling_rate, FPS, network_name

if __name__ == "__main__":
    print parseHeader('../GeneratedSequences/1.csv')