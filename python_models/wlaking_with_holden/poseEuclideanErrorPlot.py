#!/usr/bin/env python

# /*
#  * Created by Partha on 15/02/2017.
#  */

import sys
import matplotlib.pyplot as plt
import numpy
import parseHeader
import gflags
from gflags import FLAGS

gflags.DEFINE_string('file_name', '1.csv', 'Specify a generated file name for which this metric will be evaluated.'
                                           'File names are usually of the form n.csv, where n is an integer')

gflags.DEFINE_integer('avg_num_files', 1, 'Compute average error accross these many files starting from the one '
                                          'specified with "file_name" flag')

def psoseEucledianErrorPlot(file_name_and_path, verbose):
    current_scale_to_average_per_joint_in_cm = 0.33321  # Found by normalizing with body length
    # Load Generated data
    X_generated = numpy.genfromtxt(file_name_and_path, skip_header=11, delimiter=',')

    # Load Original data
    original_file_name, seq_star_row, seq_length, dwon_sampling_rate, FPS, network_name = \
        parseHeader.parseHeader(file_name_and_path)
    X_original = numpy.load("../" + original_file_name)['clips']
    seq_idx = seq_star_row % X_original.shape[0]
    time_idx = (seq_star_row // X_original.shape[0]) % dwon_sampling_rate

    if network_name == "AutoEncoderLSTM":
        X_original = X_original[seq_idx, time_idx:-1:dwon_sampling_rate, :]
    else:
        X_original = X_original[seq_idx, time_idx+dwon_sampling_rate:-1:dwon_sampling_rate, :]

    comparable_length = numpy.minimum(X_generated.shape[0], X_original.shape[0])

    eucledian_error = \
        (numpy.sum((X_original[:comparable_length, :-7] - X_generated[:comparable_length, :-7])**2, axis=-1))**0.5

    eucledian_error = eucledian_error * current_scale_to_average_per_joint_in_cm

    if verbose:
        indices_to_print = numpy.array([3, 5, 11, 19, 30]) + seq_length  # 130 as well but it is out of range
        print ["%.2f" % v for v in eucledian_error[indices_to_print]]

    return eucledian_error, seq_length

if __name__ == "__main__":
    FLAGS(sys.argv)  # parse flags
    eucledian_error, seq_length = psoseEucledianErrorPlot('../GeneratedSequences/' + FLAGS.file_name, verbose=False)
    for i in range(FLAGS.avg_num_files -1):
        next_file_name = str(int(FLAGS.file_name[:-4]) + i + 1) + FLAGS.file_name[-4:]
        eucledian_error += psoseEucledianErrorPlot('../GeneratedSequences/' + next_file_name, verbose=False)[0]

    eucledian_error = eucledian_error/float(FLAGS.avg_num_files)

    indices_to_print = numpy.array([3, 5, 11, 19, 30]) + seq_length  # 130 as welll but it is out of range
    print eucledian_error[indices_to_print]
    plt.figure()
    plt.plot(eucledian_error)
    plt.show()