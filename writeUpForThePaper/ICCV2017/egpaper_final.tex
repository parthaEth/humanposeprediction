\documentclass[10pt,twocolumn,letterpaper]{article}

\usepackage{iccv}
\usepackage{times}
\usepackage{epsfig}
\usepackage{graphicx}
\usepackage{amsmath}
\usepackage{amssymb}

\usepackage{booktabs}

% Include other packages here, before hyperref.

% If you comment hyperref and then uncomment it, you should delete
% egpaper.aux before re-running latex.  (Or just hit 'q' on the first latex
% run, let it finish, and you should be clear).
\usepackage[pagebackref=true,breaklinks=true,letterpaper=true,colorlinks,bookmarks=false]{hyperref}

% \iccvfinalcopy % *** Uncomment this line for the final submission

\def\iccvPaperID{332} % *** Enter the ICCV Paper ID here
\def\httilde{\mbox{\tt\raisebox{-.5ex}{\symbol{126}}}}

% Pages are numbered in submission mode, and unnumbered in camera-ready
\ificcvfinal\pagestyle{empty}\fi
\begin{document}

%%%%%%%%% TITLE
\title{Deep Statistical Learning for Human Motion Modeling}

\author{First Author\\
Institution1\\
Institution1 address\\
{\tt\small firstauthor@i1.org}
% For a paper whose authors are all at the same institution,
% omit the following lines up until the closing ``}''.
% Additional authors and addresses can be added with ``\and'',
% just like the second author.
% To save space, use either the email address or home page, not both
\and
Second Author\\
Institution2\\
First line of institution2 address\\
{\tt\small secondauthor@i2.org}
}

\maketitle
%\thispagestyle{empty}


%%%%%%%%% ABSTRACT
\begin{abstract}
   We propose the Drop-Out-EncoderLSTM model for open loop prediction of human body pose in 3D. The proposed model is comprised of two parts dedicated to exploit the two different features present in human body pose dynamics data. Specifically it is a predictive model for the task of motion capture (mocap) generation for full human body pose. While it is relatively easy to judge model-quality for short term prediction, it is not so for the long range prediction case because of the inherent stochasticity of human motion. In this work we also propose a metric which captures the fluidity of motion and hence can provide quantitative measurement of arbitrary length sequences that correlate with the human judgement. We evaluate our method in two of the largest mocap dataset available to date and our method show large improvement over the state of the art in extrapolating sequences of mocap vectors by exploiting temporal and structural correlations. Further more we decouple the global parameters (global position and orientation) from the prediction program and show that the model is capable of generating actions to the global parameters that may be provided by a human being. This makes animation of human character completely automated and trivial to generate.
\end{abstract}

%%%%%%%%% BODY TEXT
\section{Introduction}
Reliable and accurate prediction of immediate future is the key for making fast meaningful decisions. With ever increasing number of robots working alongside human beings it is thus essential for modern machines to be able to predict human behaviour. Although computer vision technology have take large strides over the last few years it is still difficult to track full human skeletal pose from RGB cameras in natural environment. One of the challenges in this task is lack of strong predictive model. State prediction of inanimate objects can relatively easily be formalized with the laws of physics but prediction of conscious movement of human being requires statistical methods. 

We address this issue with a Drop-Out-EncoderLSTM model, a augmented recurrent neural network model that extracts and benefits from both structural and temporal dependencies present in the data. We apply the developed method for mocap sequence prediction conditioned on a ground truth seed sequence. Specifically our model learns to assign a most likely pose for time $t + 1$ given the history of poses till $t$. At test time by supplying the predicted sample back to the input we can synthesize a novel sequence. Our input and output domain coincides. Specifically they are the same manifold in which 3D joint positions (or 3D joint angles) of human skeleton lives.

Time series is best modelled by RNNs and they have been successfully applied to language modelling\cite{}, handwriting generation\cite{}, image captioning\cite{}, machine translation\cite{} and to many more fields. One of the strengths of RNNs is that it can condition on variable length of history. Not having to choose a fixed time length in a sliding window fashion is a serious advantage since it eliminates the guess work of what might be the time horizon which might be important. Just like MLPs, RNNs can model any program given enough data and expressive power but many different inherent problems of deep learning including exploding and vanishing gradients make it difficult in practice to achieve the best performance promised theoretically. Further more given enough size and data the only reason why two different deep learning model may differ in their final performance is the loss function surface space imposed by the parameters. If a model imposes many saddle points and more suboptimal local minimums the optimization algorithm wil most likely fail to train the network to yield high quality results. Hence our motivation behind the design choice had always been to decouple sources of semantic correlations and sources of information and dedicate parts of network to learn them separately.

While feature extraction with convolutional layer is crucial for image based data because of the dimensionality reduction requirement, in case of mocap data we believe the raw representation (3D joint position or 3D joint angles) is of good enough quality for an LSTM network to be able to learn the time dependency. This belief is supported by the high scores achieved by a simple 3 layer LSTM network in human pose prediction \cite{}. However we do believe that here are two parts of the problem at hand, namely the inherent constraints to the data imposed by the size and shape of a human skeleton and temporal coherence of human motion. Attempts to capture spacial correlation has been made in te past and it mainly involves designing elaborate neural networks graphs. In contrast to  such efforts in our approach we try to learn this part from the data itself by specifically constraining a traditional fully connected network. Finally we learn the temporal dependency by a 3 layer LSTM network. This architecture although very simple captures the two component of the problem elegantly and improves significantly on the state of the art.

%-------------------------------------------------------------------------

\section{Related Work}
We provide a overview of literature that deals with human motion modelling with focus in computer graphics applications as well as action recognition. Our work mainly differentiates itself from the prior art in the following ways. Instead of using an auto encoder to learn an alternative representation of the data and hoping that the main training objective becomes easier in this space we simply train our auto encoder to refine our main network's output in a post processing sense. Further more instead of trying to capture both spatial and temporal correlation with just one network we divide the work between two different parts of the network which can be trained separately making it easier to train while maintaining end to end training capability.

Human pose prediction is one of the many problems in computer vision and robotics that require spatial and temporal reasoning. While examples of human activity recognition \cite{activity_concept_transitions, 6751553, Classifying_and_localizing, Lezama2011TrackTT} and human object interaction \cite{Anticipating_Human_Activities, Human_Object_Interactions} are abundant in the literature, pose prediction has received relatively less attention. However we do find a few works in the fied such as bilinear spatio-temporal basis models\cite{BilinearBasis}, Hidden Markov models \cite{4604474}, linear dynamical systems \cite{Learning_Switching} and Gaussian process latent variable models\cite{Gaussian_Process_Dynamical_Models}. A few more recent efforts based on deep learning techniques have been made in \cite{SRNN_paper, ERD_paper}.

Temporal dependency and state transitions are modelled in two different way, explicit parametric filtering such as Gaussian process, HMM or Kalman Filtering \cite{1640765, 1467294} and different flavors of end to end deep learning methods based on recurrent neural networks of which stacked layers of LSTMS are most favored ones\cite{LSTM}. Advantages of end to end deep learning framework over traditional methods have been highlighted over and again in the literature and since the best two competitive methods\cite{SRNN_paper, ERD_paper} are based on deep recurrent framework let us focus briefly on the methodology followed in these two work and also how our method compare against these. Katerina Fragkiadaki \etal in \cite{ERD_paper} takes an approach in which they jointly learns a representation of pose data and its time variance. The representation is learned through an auto-encoder while the time variance is learned through an RNN which is sandwiched between the encoder and the decoder. Since they use the same architecture to process video frames it is essential to have an encoder before the data can be feed to a fully connected layer but in case of human pose expressed as joint angles this representation transformation requirement is arguable. In fact we believe that a 3D joint position representation or 3D joint representation is of well enough quality to form an input for a fully connected RNN layer. This belief is supported intuitively by the observation that the representation is relatively low dimensional the sequences are smooth and there lies no discontinuity for small deviations from ground truth. This belief is further supported by \cite{SRNN_paper} which like our method learns directly on the raw representation. It is common to deploy elaborate deep learning model architecture to capture spatial structure \cite{SpatioTemporalLSTM, NTU_RGBD, SRNN_paper}. While we agree with these works about the importance of spatial structure of the data we believe a better way to capture this dependency is to explicitly train a network of this particular aspect rather than modifying it's internal connection manually. As weights in a fully connected network can modify itself to represent an arbitrary graph this way we maintain the high flexibility of the network while capturing relationship directly from the data, one of the core reason why deep learning outperforms hand crafted methods in wide variety of problems. Motivated by this reasoning we keep our design as simple as possible while forcing it to capture different data correlations. While our network looks very similar to the first half of the ERD network they differ very much on their intended purpose. While ERD perform temporal learning in the auto-encoder transformed space we do the same job in original data domain. Further more we dedicate the non recurrent layers to specifically capture spatial correlation unlike previously discussed methods.

\section{Dropout encoder-LSTM network for kinematic tracking and forecasting}
Figure \ref{fig:DropoutEncoderLSTM} shows the architecture of the network used for the purpose of human movement prediction. Formally, the task at hand is to predict $X_{t+1}$ representing the one step ahead prediction pose vector given $X_{t:0}$, the history of poses so far. The pose vector at any time instance $t$ is a sequence of real numbers $X_t = [x_1, x_2, ... x_n]$ which fully specifies the joint state of a human skeleton. There are a few kinds of conventional representations which are popular in the community and the one chosen for this work will be discussed in a later section. In order to predict human pose precisely knowledge of data correlation over both spacial and temporal dimensions is crucial i.e. in a well performing framework the trained network has to be able to take advantage of the facts that given all the states of joints of a human skeleton but one it is not hard to make a reasonable guess of the missing ones. Furthermore the joint states evolve in time rather smoothly following certain pattern. This observation is directly supported by the work by Jun Liu \etal \cite{SpatioTemporalLSTM}.

\begin{figure}[t]
\begin{center}
%\fbox{\rule{0pt}{5in} \rule{0.9\linewidth}{0pt}}
   \includegraphics[width=0.8\linewidth]{model.png}
\end{center}
   \caption{Final test itme model with dropout layers removed. Except for the input dropout layer other layers are not functionally critical and can in general be viewed as regularizers}
\label{fig:DropoutEncoderLSTM}
\label{fig:onecol}
\end{figure}

\subsection{Exploiting spacial structure} 
Disambiguating the spacial and temporal correlation can improve final results by leveraging multiple sources of uncorrelated information. Instead of trying to impose characteristics of a sequence on a data which naturally is represented as an $n-tuple$, we exploit the spacial correlation (correlation between different axes of the data) by an auto encoder. The chosen auto encoder consists of three dense layers of neurons with $relu$ activation functions. This network is trained in a denoising setting. We also found that using drop out in the network's hidden state improves its generalization and improves its convergence. In contrast to the work by Katerina Fragkiadaki \etal in \cite{ERD_paper} and by Ashesh Jain in \cite{SRNN_paper}, where a multivariate Gaussian noise is added to the input we randomly drop part of input, i.e. with a certain probability $p$ we make a component of $X_t$ zero. So the auto encoder always sees on an average $p*n$ zeros in its input. The advantage we believe this scheme enjoys over denoising of multivariate Gaussian noise is the following.  since the auto encoder is a feed forward memory less function approximator the only way it can replace a missing dimension in the input is by understanding the fact that the data comes from a physical structure and hence present input dimensions can be used to infer the missing. Clearly here we are able to force the network to learn the spacial correlation of human skeleton as opposed to the multivariate Gaussian noise removal case, where a network which can average over the noise components would perform just as good in training, but would fail if the corrupting noise components were not independent. Once the auto encoder is trained to remove dropout noise from input we save the weights and keep it aside for future use.

\subsection{Exploiting temporal structure}
In order to be able to predict the pose of a person at a time instance arbitrarily ahead in the future, we only have to be able to calculate $X_{t+1}$, the pose just one time step ahead given the history $[X_0, X_1, ... X_t]$ till now, as we could progressively build upon our prediction to produce a prediction many steps ahead. To model this time series problem we use a $3$ layer LSTM network (3LR-LSTM) with $1000$ units in each hidden layer. The training of this network is done with batches of sequences with $100$ time steps in each sequence with euclidean distance ($L_2$ norm) as the error metric. Although a probabilistic prediction formulation provides the option to sample its output distribution can generally show better resilience against convergence to the mean pose we chose not to use a trivial Gaussian mixture model formulation mainly prevalent in the literature because of the following reasons. For high dimensional data such as human pose it is often infeasible to use more than a few mixture models and more over we have to restrict the covariance of each mixture component to be diagonal. This limits the expressive power of the distribution so parametrized and hence the network is forced to learn a compromise between the mean expected pose and a few other modes around it, grossly killing the purpose of a probabilistic interpretation. Furthermore because of the nature of the mixture model we often end up sampling from the valley rejoins leading to generation of less crisp samples (equivalent to blurry image in case of image generators). Finally since we are interested in predicting precisely the next few poses and can afford to have a deterministic prediction rather than trying to generate novel sequences in each run, a deterministic formulation is well suited. None the less we do believe that for animation and computer graphics related applications a recursive Variational Auto Encoder might yield results that are human like and pleasing to the eye. But since this is not the major focus area of this work we keep aside as future work. The above discussion might convey the impression that a deterministic model is incapable of producing long novel sequences and is prone to converge to mean pose. While this is true to certain extent the effect is not severe. We shall show in the later sections that our model although deterministic can produce realistic locomotion sequence over many times longer time horizon as compared to the state of the art (11000ms as compared to 1000ms).

\subsection{The Dropout Encoder LSTM system and training}
We put the trained 3LR-LSTM network on top of the trained auto-encoder to build our final network as shown in figure \ref{fig:DropoutEncoderLSTM}. Notice that this network without any fine turning is capable of delivering high quality results. This indeed is the case because the output of the auto encoder is perfectly compatible with the input of the LSTM network. Further more since the two network focuses on two independent part of the problem, namely the spacial structure and the temporal structure they are capable of rejecting errors made by each other. How ever it was notices that a slight fine tuning (training for $~1$ epoch) with dropout actions disabled improved the over all performance. This improvement is obtained due to the fact that weight scaling after removal of dropout action does introduce slight variation in activity in the neuron inputs and outputs, which is adjusted during the fine tuning process.

\section{Long term prediction and quantitative evaluation}
The commonly followed quality measurement metric for human pose prediction is evaluated by the euclidean distance between the predicted and ground truth pose. Although this metric correctly corresponds to the quality for short prediction horizon, due to inherent stochasticity of human motion it performs poorly on longer horizons, i.e. the distance in the pose vector space might evaluate large although the generated sequence matches closely to a plausible human behaviour. Following this argument all the long term evaluation in the works so far provides only qualitative evaluation for long time horizon predictions. 

We propose a novel quantitative evaluation based on the following observation. Given the fact that there could be multiple continuation of a given seed sequence the evaluation can not rely on a comparison based method, i.e. any method which measures some form of dissimilarity between a true continuation and  the one generated is bound to fail. A similar problem is faced by the machine translation community where multiple translation is possible to a given native language sentence. The solution widely followed there is the BLEU evaluation metric \cite{BLEU}. Drawing inspiration from this work which in essence tries to evaluate the fluency and accuracy of the generated sequence we device our metric to capture the fluency of the generated sequence too. Further more since there is no preference over actions taken in long term in absence of a secondary high level objective, we do not incorporate any accuracy term in out metric, i.e. We treat all plausible continuations equally. We postulate that since human body consists of different parts connected in a certain way any action performed will impose a dependency graph on the joint trajectories. This postulate is also supported by \cite{SRNN_paper} where the dependency graph is refereed to as a spatio-temporal graph. Unlike this work which uses an expert generated dependency graph to determine the structure of the network we derive the dependency graph from the available data itself. This way we keep human involvement in the system as minimalistic as possible. Specifically given a time series of poses $[X_0, X_1, ... X_t]$ we fit a simple but separate polynomial (we shall call these as weak learners) to predict each joint's states from every other's. The degree of the polynomial is a hyper parameter which can influence over and under fitting. We choose the degree to be $3$. We also separate out major joints in order to limit the number of unique polynomials that need to be to fitted. The joints we chose are $\{'RightLeg',\ 'LeftLeg',\ 'Spine',\  'LeftArm', \\ 'RightArm'\}$. Although the polynomial changes for every pair they stay constant (coefficients of the polynomial) for the whole duration of the time series. Since the modelling capacity of a $3^{rd}$ order polynomial is limited there remains a residual error always. i.e. for instance, we can never predict the exact time evolution of the $Spine$ given that of $RightLeg$ even for the training data. But if two body parts go together in a related fashion the residual error in the training data is expected to be less. This gives us a way to use the residual errors as a measure of dependency/ relatedness of different body parts. The lesser the residual the higher the relatedness of two different body parts. \footnote{Because of this formulation it is important that we keep our weak learners in the under fitting paradigm. As otherwise in the over-fitting scenario the residuals do not capture modelling difficulty rather just become high frequency noise resulting from numerical impressions} Following this hypothesis we construct a $5{\times5}$ relatedness matrix representing the skeletal dependencies in the human body. Now this matrix by itself is not - activity, representation frame, and data format independent and can not be used to measure sequence quality with high precision. In order to gain all these desirable invariances we threshold the raw values in this matrix and automatically generate a dependency graph. The threshold value is chosen to be the median value of residuals for one joint against all the others (median across all rows in a given column). Now since this graph represents the dependency of the human skeleton we expect it be invariant across activity type, sequence length and even actors. Figure \ref{fig:TrueDepMap} shows the dependency graph obtained by running this evaluation on the ground truth sequences and was found to be invariant across different actors and activities present in human3.6m data set. Now we evaluate the quality of a generated sequence by comparing the dependency graph induced by it to that obtained from ground truth. We quantize similarity by computing $F1$ score of the presence of edges, which represent correlation. Further more in order to gain more fine tuned evaluation we can compute this score when different prediction horizons are considered for a given network, and the longer a network can maintain a high $F1$ score the better is its quality.

\begin{figure}[t]
\begin{center}
%\fbox{\rule{0pt}{5in} \rule{0.9\linewidth}{0pt}}
   \includegraphics[width=0.8\linewidth]{TrueDepMap.eps}
\end{center}
   \caption{Dependency map of human motion data. Each link (lines in blue) represents dependency between body parts(represented by red circles)}
\label{fig:TrueDepMap}
\end{figure}

\section{Experiment}
In order to ensure general applicability of our model we validate it on two largest motion capture dataset available at the time of writing this article. One of these dataset is the H3.6M \cite{h36m_pami, IonescuSminchisescu11} dataset which provides the single largest dataset with high quality 3D joint position of human skeleton. This dataset is further pre processed so the joint angles are considered. This pre processing technique is used to ensure direct comparison with \cite{SRNN_paper, ERD_paper}. Finally we also test our model on a dataset which is constructed by combining many large datasets freely available\cite{CMU_dataset, hdm05, mhad, Holden:2015:LMM, Holden:2016:DLF}. The preprocessing is taken from \cite{Holden:2015:LMM, Holden:2016:DLF} by Holden \etal. 

\subsection{Motion modelling and forecasting}
In this section we shall provide the results obtained on the prediction task of human pose for different time horizons in the future. We first present the results obtained in the H3.6M dataset with the conventionally used metric. A common observation made by all the works in this field is that quality of long term predictions by a model can not be judged quantitatively with currently available methods. In order to improve upon this draw back we propose an error metric which can quantitatively benchmark different methods. In order to verify our error metric's quality verify that the model quality induced by our metric matches well with the true model quality.

We compare DropoutAutoencoder model with the state of the art SRNN\cite{SRNN_paper} and ERD\cite{ERD_paper} architecture. We also compare with a 3 layer LSTM architecture (LSTM-3LR) which is used as first baseline by both the previous works. We find this simple network surprisingly competitive, especially when carefully tuned. Indeed in theory this network can model everything any other network can given correct set of weights and the only advantage that we receive from a more complicated architecture is ease of training. To forecast, we feed the models with first $(100) seed$ mocap frames and predict $(300)$ frames into the future. Following \cite{SRNN_paper}, we consider walking, eating, and smoking activity. 


\begin{table}
\begin{center}
\centering
\begin{tabular}{|l|c|c|c|c|c|c}
\hline
Methods &  \multicolumn{2}{l|}{Short-term} & \multicolumn{3}{l|}{Long-term forecast}\\
 & 80 & 160 & 320 & 560 & 1000\\
 \hline
\multicolumn{6}{|c|}{Walking activity}\\
\hline
LSTM-3LR & 1.19&1.21&1.44&1.57&1.81\\
ERD & 1.30 & 1.56 & 1.84 & 2.00 & 2.38 \\
S-RNN   &  1.08&1.34&1.60&1.90&2.13\\
DropOutEncoder & \textbf{0.89}&\textbf{1.05}&\textbf{1.15}&\textbf{1.27}&\textbf{1.38}\\
\hline
    \multicolumn{6}{|c|}{Eating activity}\\
\hline
    LSTM-3LR & 1.49&1.67&2.15&2.01&\textbf{2.08}\\
    ERD & 1.66 & 1.93 & 2.28 & 2.36 & 2.41 \\
    S-RNN   &  1.35&1.71&2.12&2.28&2.58\\
    DropOutEncoder & \textbf{1.24}&\textbf{1.53}&\textbf{1.80}&\textbf{1.99}&2.50\\
    \hline
    \multicolumn{6}{|c|}{Smoking activity}\\
\hline
    LSTM-3LR & 1.13&1.15&1.33&1.63&2.53\\
    ERD & 2.05 & 2.34 & 3.10 & 3.24 & 3.42 \\
    S-RNN   &  1.90&2.30&2.90&3.21&3.23\\
    DropOutEncoder & \textbf{0.89}&\textbf{0.99}&\textbf{1.20}&\textbf{1.48}&\textbf{1.77}\\
    \hline
    \multicolumn{6}{|c|}{Discussion activity}\\
\hline
    LSTM-3LR & 1.21&1.36&1.72&2.03&\textbf{2.31}\\
    ERD & 2.67 & 2.97 & 3.23 & 3.47 & 2.92 \\
    S-RNN   &  1.67  &  2.03   & 2.20  & 2.39 & 2.43\\
    DropOutEncoder & \textbf{1.17} & \textbf{1.28} & \textbf{1.51} & \textbf{1.85} & 5.24\\
\hline
\end{tabular}
\end{center}
\caption{Squared Loss for different seed sequences and time horizons. The loss is calculated as the euclidean norm (L2) of unnormalized ground truth and predicted MOCP vectors.}
\label{h36M_euclidean}
\end{table}

Table \ref{h36M_euclidean} shows the quantitative results for different activities over a time horizon of 1000 ms. This metric is taken from Fragkiadaki \etal, which simply calculates the euclidean error between the predicted mocap vector and the ground truth. Since euclidean distance in exponential map space dos not represent true dissimilarity this metric is a week measure of quality. However for small time horizons and small enough absolute value although not linearly the numbers produced by this metric do correspond to closeness of poses. Due to stochasticity of human motion a higher value in long term prediction is not a measure of model-quality our method tends to keep the error much lower. To summarize, all deterministic neural predictive models when feed back with its own output tend to drift in certain direction in the data manifold due to accumulation of correlated error. We can counteract this effect by de-noising the generated vector with a second network which preferably captures and exploits a different type of data correlation to maximize decorrelation.

Figure \ref{} shows a qualitative evaluation of the models in consideration. Qualitatively we see a very similar reflection of the first quantitative measure i.e. in short term all the models are very close to the seed sequence and all of them smoothly extends it. The subtle differences in predicted joint positions are a bit difficult to notice from this plot. However the purpose of this table is to examine the fluidity of movement produced by different models over longer time horizon, i.e. to compare how realistic they look.

\begin{table}
\begin{center}
\centering
\begin{tabular}{|l|c|c|c|c|c|c}
\hline
Methods &  \multicolumn{2}{l|}{Short-term} & \multicolumn{3}{l|}{Long-term forecast}\\
 & 80 & 160 & 320 & 560 & 1000\\
 & ms & ms & ms & ms & ms\\
\hline
LSTM-3LR & 2.76 & 3.41 & \textbf{4.23} & \textbf{3.89} & 4.12\\
ERD & 2.87 & 3.88 & 5.64 & 6.08 & 6.96 \\
S-RNN   &  ?? & ?? & ?? & ?? & ??\\
DropOutEncoder & \textbf{2.42}&\textbf{3.14}&4.37 & 4.09  & \textbf{4.03}\\
\hline
\end{tabular}
\end{center}
\caption{Average error in joint position for different time horizons represented in $cm$. The loss is calculated as the euclidean norm (L2) of unnormalized ground truth and predicted MOCP vectors. The height of the skeleton (1.7m) was used convert the errors into real scale}
\label{h36M_euclidean}
\end{table}

Table \ref{holden_euclidean} show the results obtained when the experiment was repeated on the collective dataset, described and constructed by Holden \etal \cite{Holden:2015:LMM, Holden:2016:DLF}. Since the processed data had no action label and also due to space constraints we present here the average prediction error across all test sequences. For this dataset the only pre-processing done apart from \cite{Holden:2015:LMM, Holden:2016:DLF} is to scale each feature to a range of $[0, 1]$. Also here we represent the error which has a unit of $cm-per-joint$ as opposed to unit less exponential domain distance presented in Table \ref{h36M_euclidean}.


\subsection{Validation of Autoencoder's design motivation}
As described in the previous section the main motivation behind the design of our model was to capture different kinds of data correlation in different part of the network so we may tune the training conditions to force the network to learn the specific details. Particularly we argue that in human full-body mocap data we have spatial as well as temporal dependency and that our de-noising auto-encoder helps remove errors made by the recurrent predictor. Table \ref{denoising_helps} show that this indeed is the case. Clearly the filtered output is much closer to the ground truth than the original recurrent output. 

\begin{table}
\begin{center}
\centering
\begin{tabular}{|l|c|c|c|c|c|c}
\hline
Methods &  \multicolumn{2}{l|}{Short-term} & \multicolumn{3}{l|}{Long-term forecast}\\
 & 80 & 160 & 320 & 560 & 1000\\
 & ms & ms & ms & ms & ms\\
\hline
DropOutEncoder & 2.72 & 3.39 & 4.44 & 3.96 & 4.02 \\
No Filtering & & & & & \\
\hline
DropOutEncoder & 2.42 & 3.14 & 4.37 & 4.09  & 4.03\\
\hline
\end{tabular}
\end{center}
\caption{Average error in joint position for different time horizons represented in $cm$. The loss is calculated as the euclidean norm (L2) of unnormalized ground truth and predicted MOCP vectors. The height of the skeleton (1.7m) was used convert the errors into real scale}
\label{h36M_euclidean}
\end{table}

%-------------------------------------------------------------------------
%\subsection{References}

%List and number all bibliographical references in 9-point Times,
%single-spaced, at the end of your paper. When referenced in the text,
%enclose the citation number in square brackets, for
%example~\cite{Authors14}.  Where appropriate, include the name(s) of
%editors of referenced books.
%
%\begin{table}
%\begin{center}
%\begin{tabular}{|l|c|}
%\hline
%Method & Frobnability \\
%\hline\hline
%Theirs & Frumpy \\
%Yours & Frobbly \\
%Ours & Makes one's heart Frob\\
%\hline
%\end{tabular}
%\end{center}
%\caption{Results.   Ours is better.}
%\end{table}
%
%%-------------------------------------------------------------------------
%\subsection{Illustrations, graphs, and photographs}
%
%When placing figures in \LaTeX, it's almost always best to use
%\verb+\includegraphics+, and to specify the  figure width as a multiple of
%the line width as in the example below
%{\small\begin{verbatim}
%   \usepackage[dvips]{graphicx} ...
%   \includegraphics[width=0.8\linewidth]
%                   {myfile.eps}
%\end{verbatim}
%}
%
%
%%------------------------------------------------------------------------
%\section{Final copy}
%
%You must include your signed IEEE copyright release form when you submit
%your finished paper. We MUST have this form before your paper can be
%published in the proceedings.


{\small
\bibliographystyle{ieee}
\bibliography{egbib}
}

\end{document}
